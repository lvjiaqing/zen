// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "../zen.h"

namespace zen {

class TopCommand : public ZenCmdBase
{
public:
	TopCommand();
	~TopCommand();

	virtual int				  Run(const ZenCliOptions& GlobalOptions, int argc, char** argv) override;
	virtual cxxopts::Options& Options() override { return m_Options; }

private:
	cxxopts::Options m_Options{"top", "Show dev UI"};
};

class PsCommand : public ZenCmdBase
{
public:
	PsCommand();
	~PsCommand();

	virtual int				  Run(const ZenCliOptions& GlobalOptions, int argc, char** argv) override;
	virtual cxxopts::Options& Options() override { return m_Options; }

private:
	cxxopts::Options m_Options{"ps", "Enumerate running Zen server instances"};
};

}  // namespace zen
