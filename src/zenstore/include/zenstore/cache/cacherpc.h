// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include <zencore/iobuffer.h>
#include <zencore/logging.h>
#include <zenutil/cache/cache.h>

#include <atomic>
#include <string_view>
#include <vector>

namespace zen {

namespace cache { namespace detail {
	struct RecordBody;
	struct ChunkRequest;
}}	// namespace cache::detail

struct CacheChunkRequest;
struct CacheKeyRequest;
struct PutRequestData;

class CbPackage;
class CbObjectView;
class CidStore;
class DiskWriteBlocker;
class HttpStructuredCacheService;
class UpstreamCacheClient;
class ZenCacheStore;

enum class CachePolicy : uint32_t;
enum class RpcAcceptOptions : uint16_t;

struct AttachmentCount
{
	uint32_t New	 = 0;
	uint32_t Valid	 = 0;
	uint32_t Invalid = 0;
	uint32_t Total	 = 0;
};

struct CacheStats
{
	std::atomic_uint64_t HitCount{};
	std::atomic_uint64_t UpstreamHitCount{};
	std::atomic_uint64_t MissCount{};
	std::atomic_uint64_t WriteCount{};
	std::atomic_uint64_t BadRequestCount{};
	std::atomic_uint64_t RpcRequests{};
	std::atomic_uint64_t RpcRecordRequests{};
	std::atomic_uint64_t RpcRecordBatchRequests{};
	std::atomic_uint64_t RpcValueRequests{};
	std::atomic_uint64_t RpcValueBatchRequests{};
	std::atomic_uint64_t RpcChunkRequests{};
	std::atomic_uint64_t RpcChunkBatchRequests{};
};

enum class PutResult
{
	Success,
	Fail,
	Invalid,
};

/** Recognize both kBinary and kCompressedBinary as kCompressedBinary for structured cache value keys.

	We need this until the content type is preserved for kCompressedBinary when passing to and from upstream servers.
  */

constexpr bool
IsCompressedBinary(ZenContentType Type)
{
	return Type == ZenContentType::kBinary || Type == ZenContentType::kCompressedBinary;
}

struct CacheRpcHandler
{
	CacheRpcHandler(LoggerRef				InLog,
					CacheStats&				InCacheStats,
					UpstreamCacheClient&	InUpstreamCache,
					ZenCacheStore&			InCacheStore,
					CidStore&				InCidStore,
					const DiskWriteBlocker* InDiskWriteBlocker);
	~CacheRpcHandler();

	enum class RpcResponseCode
	{
		InsufficientStorage = 507,
		BadRequest			= 400,
		OK					= 200
	};

	RpcResponseCode HandleRpcRequest(const CacheRequestContext& Context,
									 const ZenContentType		ContentType,
									 IoBuffer&&					Body,
									 uint32_t&					OutAcceptMagic,
									 RpcAcceptOptions&			OutAcceptFlags,
									 int&						OutTargetProcessId,
									 CbPackage&					OutPackage);

private:
	CbPackage HandleRpcPutCacheRecords(const CacheRequestContext& Context, const CbPackage& BatchRequest);
	CbPackage HandleRpcGetCacheRecords(const CacheRequestContext& Context, CbObjectView BatchRequest);
	CbPackage HandleRpcPutCacheValues(const CacheRequestContext& Context, const CbPackage& BatchRequest);
	CbPackage HandleRpcGetCacheValues(const CacheRequestContext& Context, CbObjectView BatchRequest);
	CbPackage HandleRpcGetCacheChunks(const CacheRequestContext& Context, RpcAcceptOptions AcceptOptions, CbObjectView BatchRequest);

	PutResult PutCacheRecord(PutRequestData& Request, const CbPackage* Package);

	/** HandleRpcGetCacheChunks Helper: Parse the Body object into RecordValue Requests and Value Requests. */
	bool ParseGetCacheChunksRequest(std::string&							   Namespace,
									std::vector<CacheKeyRequest>&			   RecordKeys,
									std::vector<cache::detail::RecordBody>&	   Records,
									std::vector<CacheChunkRequest>&			   RequestKeys,
									std::vector<cache::detail::ChunkRequest>&  Requests,
									std::vector<cache::detail::ChunkRequest*>& RecordRequests,
									std::vector<cache::detail::ChunkRequest*>& ValueRequests,
									CbObjectView							   RpcRequest);
	/** HandleRpcGetCacheChunks Helper: Load records to get ContentId for RecordRequests, and load their payloads if they exist locally.
	 */
	void GetLocalCacheRecords(const CacheRequestContext&				 Context,
							  std::string_view							 Namespace,
							  std::vector<CacheKeyRequest>&				 RecordKeys,
							  std::vector<cache::detail::RecordBody>&	 Records,
							  std::vector<cache::detail::ChunkRequest*>& RecordRequests,
							  std::vector<CacheChunkRequest*>&			 OutUpstreamChunks);
	/** HandleRpcGetCacheChunks Helper: For ValueRequests, load their payloads if they exist locally. */
	void GetLocalCacheValues(const CacheRequestContext&					Context,
							 std::string_view							Namespace,
							 std::vector<cache::detail::ChunkRequest*>& ValueRequests,
							 std::vector<CacheChunkRequest*>&			OutUpstreamChunks);
	/** HandleRpcGetCacheChunks Helper: Load payloads from upstream that did not exist locally. */
	void GetUpstreamCacheChunks(const CacheRequestContext&				  Context,
								std::string_view						  Namespace,
								std::vector<CacheChunkRequest*>&		  UpstreamChunks,
								std::vector<CacheChunkRequest>&			  RequestKeys,
								std::vector<cache::detail::ChunkRequest>& Requests);
	/** HandleRpcGetCacheChunks Helper: Send response message containing all chunk results. */
	CbPackage WriteGetCacheChunksResponse(const CacheRequestContext&				Context,
										  std::string_view							Namespace,
										  RpcAcceptOptions							AcceptOptions,
										  std::vector<cache::detail::ChunkRequest>& Requests);

	LoggerRef				Log() { return m_Log; }
	LoggerRef				m_Log;
	CacheStats&				m_CacheStats;
	UpstreamCacheClient&	m_UpstreamCache;
	ZenCacheStore&			m_CacheStore;
	CidStore&				m_CidStore;
	const DiskWriteBlocker* m_DiskWriteBlocker = nullptr;

	bool AreDiskWritesAllowed() const;
};

}  // namespace zen
