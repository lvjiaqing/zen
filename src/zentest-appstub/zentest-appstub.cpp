// Copyright Epic Games, Inc. All Rights Reserved.

#include <stdio.h>
#include <cstdlib>
#include <cstring>
#include <thread>

using namespace std::chrono_literals;

int
main(int argc, char* argv[])
{
	int ExitCode = 0;

	for (int i = 0; i < argc; ++i)
	{
		if (std::strncmp(argv[i], "-t=", 3) == 0)
		{
			const int SleepTime = std::atoi(argv[i] + 3);

			printf("[zentest] sleeping for %ds...\n", SleepTime);

			std::this_thread::sleep_for(SleepTime * 1s);
		}
		else if (std::strncmp(argv[i], "-f=", 3) == 0)
		{
			ExitCode = std::atoi(argv[i] + 3);
		}
	}

	printf("[zentest] exiting with exit code: %d\n", ExitCode);

	return ExitCode;
}
