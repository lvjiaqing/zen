// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include <zencore/zencore.h>

#define ZENSTORE_API

namespace zen {

class IoBuffer;
struct IoHash;

class ChunkResolver
{
public:
	virtual IoBuffer FindChunkByCid(const IoHash& DecompressedId) = 0;
};

ZENSTORE_API void zenstore_forcelinktests();

}  // namespace zen
