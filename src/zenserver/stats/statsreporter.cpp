// Copyright Epic Games, Inc. All Rights Reserved.

#include "statsreporter.h"

#include <zencore/logging.h>
#include <zennet/statsdclient.h>

namespace zen {

StatsReporter::StatsReporter()
{
}

StatsReporter::~StatsReporter()
{
}

void
StatsReporter::Initialize(const ZenStatsConfig& Config)
{
	RwLock::ExclusiveLockScope _(m_Lock);

	if (Config.Enabled)
	{
		ZEN_INFO("initializing stats reporter: {}:{}", Config.StatsdHost, Config.StatsdPort)
		m_Statsd = CreateStatsDaemonClient(Config.StatsdHost, gsl::narrow<uint16_t>(Config.StatsdPort));
		m_Statsd->SetMessageSize(1500, false);
	}
}

void
StatsReporter::Shutdown()
{
	RwLock::ExclusiveLockScope _(m_Lock);
	m_Statsd.reset();
}

void
StatsReporter::AddProvider(StatsProvider* Provider)
{
	RwLock::ExclusiveLockScope _(m_Lock);
	m_Providers.push_back(Provider);
}

void
StatsReporter::ReportStats()
{
	RwLock::ExclusiveLockScope _(m_Lock);
	if (m_Statsd)
	{
		for (StatsProvider* Provider : m_Providers)
		{
			Provider->ReportMetrics(*m_Statsd);
		}

		m_Statsd->Flush();
	}
}

}  // namespace zen
