// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include <zencore/zencore.h>
#include <zenutil/statsreporter.h>

#include <memory>
#include <string_view>

namespace zen {

class StatsTransportBase
{
public:
	virtual ~StatsTransportBase()							= 0;
	virtual void SendMessage(const void* Data, size_t Size) = 0;
};

class StatsDaemonClient : public StatsMetrics
{
public:
	virtual ~StatsDaemonClient() = 0;

	virtual void SetMessageSize(size_t MessageSize, bool UseThreads) = 0;
	virtual void Flush()											 = 0;

	// Not (yet) implemented: Set,Timing
};

std::unique_ptr<StatsDaemonClient> CreateStatsDaemonClient(std::string_view TargetHost = "localhost", uint16_t TargetPort = 8125);

void statsd_forcelink();

}  // namespace zen
