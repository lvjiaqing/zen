-- Copyright Epic Games, Inc. All Rights Reserved.

target("zencore-test")
    set_kind("binary")
    set_group("tests")
    add_headerfiles("**.h")
    add_files("*.cpp")
    add_deps("zencore")
    add_packages("vcpkg::doctest")
