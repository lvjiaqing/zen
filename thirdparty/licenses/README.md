# Third party licenses

Third party library license approvals for all libraries used by Zen.  All referenced libraries are required to have
approval for use by the legal team.  If adding a new referenced library, please request legal approval first.
