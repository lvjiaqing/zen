// Copyright Epic Games, Inc. All Rights Reserved.

#include <zenvfs/zenvfs.h>

namespace zen {

// temporary dummy function to appease Mac linker
void
ZenVfs()
{
}

}  // namespace zen
