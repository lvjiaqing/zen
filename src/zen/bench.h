// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include <stdexcept>

namespace zen::bench::util {

void EmptyStandByList();

struct elevation_required_exception : public std::runtime_error
{
	explicit elevation_required_exception(const std::string& What) : std::runtime_error{What} {}
};

}  // namespace zen::bench::util
