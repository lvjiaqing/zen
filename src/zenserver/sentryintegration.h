// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include <zencore/intmath.h>
#include <zencore/zencore.h>

#if !defined(ZEN_USE_SENTRY)
#	if ZEN_PLATFORM_MAC && ZEN_ARCH_ARM64
// vcpkg's sentry-native port does not support Arm on Mac.
#		define ZEN_USE_SENTRY 0
#	else
#		define ZEN_USE_SENTRY 1
#	endif
#endif

#if ZEN_USE_SENTRY

#	include <memory>

ZEN_THIRD_PARTY_INCLUDES_START
#	include <spdlog/logger.h>
ZEN_THIRD_PARTY_INCLUDES_END

namespace sentry {

struct SentryAssertImpl;

}  // namespace sentry

namespace zen {

class SentryIntegration
{
public:
	SentryIntegration();
	~SentryIntegration();

	void		Initialize(std::string SentryDatabasePath, std::string SentryAttachmentsPath, bool AllowPII);
	void		LogStartupInformation();
	static void ClearCaches();

private:
	int										  m_SentryErrorCode = 0;
	bool									  m_IsInitialized	= false;
	bool									  m_AllowPII		= false;
	std::unique_ptr<sentry::SentryAssertImpl> m_SentryAssert;
	std::string								  m_SentryUserName;
	std::string								  m_SentryHostName;
	std::string								  m_SentryId;
	std::shared_ptr<spdlog::logger>			  m_SentryLogger;
};

}  // namespace zen
#endif
