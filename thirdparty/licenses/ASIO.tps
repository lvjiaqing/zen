<?xml version="1.0" encoding="utf-8"?>
<TpsData xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <Name>ASIO</Name>
  <Location>https://github.com/EpicGames/zen/tree/main/thirdparty/</Location>
  <Function>Asio is a cross-platform C++ library for network and low-level I/O programming that provides developers with a consistent asynchronous model using a modern C++ approach.</Function>
  <Eula>https://github.com/chriskohlhoff/asio/blob/master/asio/LICENSE_1_0.txt</Eula>
  <RedistributeTo>
    <EndUserGroup>Licensees</EndUserGroup>
    <EndUserGroup>Git</EndUserGroup>
    <EndUserGroup>P4</EndUserGroup>
  </RedistributeTo>
  <LicenseFolder>https://github.com/EpicGames/zen/tree/main/thirdparty/licenses</LicenseFolder>
</TpsData>