// Copyright Epic Games, Inc. All Rights Reserved.

#include <zenhttp/httpserver.h>

#pragma once

namespace zen {

/** Helper class for HTTP server implementations

	Provides some common functionality which can be used across all server
	implementations. These could be in the root class but I think it's nicer
	to hide the implementation details from client code
 */
class HttpServerTracer
{
public:
	void Initialize(std::filesystem::path DataDir);
	void WriteDebugPayload(std::string_view Filename, const std::span<const IoBuffer> Payload);

private:
	std::filesystem::path m_DataDir;	 // Application data directory
	std::filesystem::path m_PayloadDir;	 // Request debugging payload directory
};

}  // namespace zen
