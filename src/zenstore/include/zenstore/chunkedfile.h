// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include <zencore/iobuffer.h>
#include <zencore/iohash.h>
#include <zencore/zencore.h>

#include <functional>
#include <vector>

namespace zen {

class BasicFile;

struct ChunkedInfo
{
	uint64_t			  RawSize = 0;
	IoHash				  RawHash;
	std::vector<uint32_t> ChunkSequence;
	std::vector<IoHash>	  ChunkHashes;
};

struct ChunkSource
{
	uint64_t Offset;  // 8
	uint32_t Size;	  // 4
};

struct ChunkedInfoWithSource
{
	ChunkedInfo				 Info;
	std::vector<ChunkSource> ChunkSources;
};

struct ChunkedParams
{
	bool   UseThreshold = true;
	size_t MinSize		= (2u * 1024u) - 128u;
	size_t MaxSize		= (16u * 1024u);
	size_t AvgSize		= (3u * 1024u);
};

static const ChunkedParams UShaderByteCodeParams = {.UseThreshold = true, .MinSize = 17280, .MaxSize = 139264, .AvgSize = 36340};

ChunkedInfoWithSource ChunkData(BasicFile& RawData, uint64_t Offset, uint64_t Size, ChunkedParams Params = {});
void				  Reconstruct(const ChunkedInfo&							   Info,
								  const std::filesystem::path&					   TargetPath,
								  std::function<IoBuffer(const IoHash& ChunkHash)> GetChunk);
IoBuffer			  SerializeChunkedInfo(const ChunkedInfo& Info);
ChunkedInfo			  DeserializeChunkedInfo(IoBuffer& Buffer);

void chunkedfile_forcelink();
}  // namespace zen
