winget install Microsoft.VisualStudio.2022.Professional --silent --override "--wait --quiet --add ProductLang En-us --add Microsoft.VisualStudio.Workload.NativeDesktop --includeRecommended"
winget install git.git --silent --accept-package-agreements
winget install github.cli --silent --accept-package-agreements
winget install xmake --silent --accept-package-agreements
winget install vscode --silent --accept-package-agreements
