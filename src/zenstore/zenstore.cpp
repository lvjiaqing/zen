// Copyright Epic Games, Inc. All Rights Reserved.

#include "zenstore/zenstore.h"

#if ZEN_WITH_TESTS

#	include <zenstore/blockstore.h>
#	include <zenstore/cache/structuredcachestore.h>
#	include <zenstore/workspaces.h>
#	include <zenstore/gc.h>
#	include <zenstore/hashkeyset.h>

#	include "cas.h"
#	include "compactcas.h"
#	include "filecas.h"

namespace zen {

void
zenstore_forcelinktests()
{
	CAS_forcelink();
	filecas_forcelink();
	blockstore_forcelink();
	compactcas_forcelink();
	workspaces_forcelink();
	gc_forcelink();
	hashkeyset_forcelink();
	structured_cachestore_forcelink();
}

}  // namespace zen

#endif
