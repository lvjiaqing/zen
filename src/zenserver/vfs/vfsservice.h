// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include <zenbase/refcount.h>
#include <zenhttp/httpserver.h>
#include <zenvfs/vfs.h>

#include <memory>

namespace zen {

class ProjectStore;
class ZenCacheStore;

/** Virtual File System service

	Implements support for exposing data via a virtual file system interface. Currently
	this is primarily used to surface various data stored in the local storage service
	to users for debugging and exploration purposes.

	Currently, it surfaces information from the structured cache service and from the
	project store.

 */

class VfsService : public HttpService
{
public:
	VfsService();
	~VfsService();

	void Mount(std::string_view MountPoint);
	void Unmount();

	void AddService(Ref<ProjectStore>&&);
	void AddService(Ref<ZenCacheStore>&&);

protected:
	virtual const char* BaseUri() const override;
	virtual void		HandleRequest(HttpServerRequest& HttpServiceRequest) override;

private:
	struct Impl;
	Impl* m_Impl = nullptr;

	HttpRequestRouter m_Router;

	friend struct VfsServiceDataSource;
};

}  // namespace zen
