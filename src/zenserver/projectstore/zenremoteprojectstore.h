// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "remoteprojectstore.h"

namespace zen {

struct ZenRemoteStoreOptions : RemoteStoreOptions
{
	std::string Url;
	std::string ProjectId;
	std::string OplogId;
};

std::shared_ptr<RemoteProjectStore> CreateZenRemoteStore(const ZenRemoteStoreOptions& Options, const std::filesystem::path& TempFilePath);

}  // namespace zen
