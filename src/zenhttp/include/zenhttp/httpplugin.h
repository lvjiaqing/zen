// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include <zenbase/refcount.h>

#if !defined(ZEN_WITH_PLUGINS)
#	if ZEN_PLATFORM_WINDOWS
#		define ZEN_WITH_PLUGINS 1
#	else
#		define ZEN_WITH_PLUGINS 0
#	endif
#endif

#if ZEN_WITH_PLUGINS
#	include "transportplugin.h"
#	include <zenhttp/httpserver.h>

namespace zen {

class HttpPluginServer : public HttpServer
{
public:
	virtual void AddPlugin(Ref<TransportPlugin> Plugin)	   = 0;
	virtual void RemovePlugin(Ref<TransportPlugin> Plugin) = 0;
};

Ref<HttpPluginServer> CreateHttpPluginServer();

}  // namespace zen

#endif
