// Copyright Epic Games, Inc. All Rights Reserved.

#include "projfsproviderinterface.h"

#if ZEN_WITH_VFS

namespace zen {

HRESULT
ProjFsProviderInterface::Notify(const PRJ_CALLBACK_DATA*	 CallbackData,
								BOOLEAN						 IsDirectory,
								PRJ_NOTIFICATION			 NotificationType,
								PCWSTR						 DestinationFileName,
								PRJ_NOTIFICATION_PARAMETERS* NotificationParameters)
{
	ZEN_UNUSED(CallbackData, IsDirectory, NotificationType, DestinationFileName, NotificationParameters);
	return S_OK;
}

HRESULT
ProjFsProviderInterface::QueryFileName(const PRJ_CALLBACK_DATA* CallbackData)
{
	ZEN_UNUSED(CallbackData);
	return S_OK;
}

void
ProjFsProviderInterface::CancelCommand(const PRJ_CALLBACK_DATA* CallbackData)
{
	ZEN_UNUSED(CallbackData);
}

//////////////////////////////////////////////////////////////////////////
//
// Forwarding functions for all ProjFS callbacks - these simply call the
// corresponding member function on the active provider
//

HRESULT
ProjFsProviderInterface::StartDirEnumCallback_C(_In_ const PRJ_CALLBACK_DATA* CallbackData, _In_ const GUID* EnumerationId)
{
	return reinterpret_cast<ProjFsProviderInterface*>(CallbackData->InstanceContext)->StartDirEnum(CallbackData, EnumerationId);
}

HRESULT
ProjFsProviderInterface::EndDirEnumCallback_C(_In_ const PRJ_CALLBACK_DATA* CallbackData, _In_ const GUID* EnumerationId)
{
	return reinterpret_cast<ProjFsProviderInterface*>(CallbackData->InstanceContext)->EndDirEnum(CallbackData, EnumerationId);
}

HRESULT
ProjFsProviderInterface::GetDirEnumCallback_C(_In_ const PRJ_CALLBACK_DATA*	   CallbackData,
											  _In_ const GUID*				   EnumerationId,
											  _In_opt_ PCWSTR				   SearchExpression,
											  _In_ PRJ_DIR_ENTRY_BUFFER_HANDLE DirEntryBufferHandle)
{
	return reinterpret_cast<ProjFsProviderInterface*>(CallbackData->InstanceContext)
		->GetDirEnum(CallbackData, EnumerationId, SearchExpression, DirEntryBufferHandle);
}

HRESULT
ProjFsProviderInterface::GetPlaceholderInfoCallback_C(_In_ const PRJ_CALLBACK_DATA* CallbackData)
{
	return reinterpret_cast<ProjFsProviderInterface*>(CallbackData->InstanceContext)->GetPlaceholderInfo(CallbackData);
}

HRESULT
ProjFsProviderInterface::GetFileDataCallback_C(_In_ const PRJ_CALLBACK_DATA* CallbackData, _In_ UINT64 ByteOffset, _In_ UINT32 Length)
{
	return reinterpret_cast<ProjFsProviderInterface*>(CallbackData->InstanceContext)->GetFileData(CallbackData, ByteOffset, Length);
}

HRESULT
ProjFsProviderInterface::NotificationCallback_C(_In_ const PRJ_CALLBACK_DATA*		 CallbackData,
												_In_ BOOLEAN						 IsDirectory,
												_In_ PRJ_NOTIFICATION				 NotificationType,
												_In_opt_ PCWSTR						 DestinationFileName,
												_Inout_ PRJ_NOTIFICATION_PARAMETERS* NotificationParameters)
{
	return reinterpret_cast<ProjFsProviderInterface*>(CallbackData->InstanceContext)
		->Notify(CallbackData, IsDirectory, NotificationType, DestinationFileName, NotificationParameters);
}

HRESULT
ProjFsProviderInterface::QueryFileName_C(_In_ const PRJ_CALLBACK_DATA* CallbackData)
{
	return reinterpret_cast<ProjFsProviderInterface*>(CallbackData->InstanceContext)->QueryFileName(CallbackData);
}

void
ProjFsProviderInterface::CancelCommand_C(_In_ const PRJ_CALLBACK_DATA* CallbackData)
{
	return reinterpret_cast<ProjFsProviderInterface*>(CallbackData->InstanceContext)->CancelCommand(CallbackData);
}

}  // namespace zen

#endif
