// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include <zencore/thread.h>
#include <zencore/uid.h>

#include <thread>
#include <unordered_map>

namespace zen {

class OpenProcessCache
{
public:
	OpenProcessCache();
	~OpenProcessCache();

	void* GetProcessHandle(Oid SessionId, int ProcessPid);

private:
#if ZEN_PLATFORM_WINDOWS
	struct Process
	{
		int	  ProcessPid;
		void* ProcessHandle;
	};

	void GCHandles();
	void GcWorker();

	RwLock										  m_SessionsLock;
	std::unordered_map<Oid, Process, Oid::Hasher> m_Sessions;
	Event										  m_GcExitEvent;
	std::thread									  m_GcThread;
#endif	// ZEN_PLATFORM_WINDOWS
};

}  // namespace zen
