// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "../zen.h"

namespace zen {

/** Copy files, possibly using block cloning
 */
class CopyCommand : public ZenCmdBase
{
public:
	CopyCommand();
	~CopyCommand();

	virtual cxxopts::Options& Options() override { return m_Options; }
	virtual int				  Run(const ZenCliOptions& GlobalOptions, int argc, char** argv) override;
	virtual ZenCmdCategory&	  CommandCategory() const override { return g_UtilitiesCategory; }

private:
	cxxopts::Options m_Options{"copy", "Copy files efficiently"};
	std::string		 m_CopySource;
	std::string		 m_CopyTarget;
	bool			 m_NoClone	 = false;
	bool			 m_MustClone = false;
};

}  // namespace zen
