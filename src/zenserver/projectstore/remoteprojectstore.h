// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include <zencore/jobqueue.h>
#include "projectstore.h"

#include <unordered_set>

namespace zen {

class CidStore;
class WorkerThreadPool;
struct ChunkedInfo;

class RemoteProjectStore
{
public:
	struct Result
	{
		int32_t		ErrorCode{};
		double		ElapsedSeconds{};
		std::string Reason;
		std::string Text;
	};

	struct SaveResult : public Result
	{
		std::unordered_set<IoHash, IoHash::Hasher> Needs;
		IoHash									   RawHash;
	};

	struct FinalizeResult : public Result
	{
		std::unordered_set<IoHash, IoHash::Hasher> Needs;
	};

	struct SaveAttachmentResult : public Result
	{
	};

	struct SaveAttachmentsResult : public Result
	{
	};

	struct HasAttachmentsResult : public Result
	{
		std::unordered_set<IoHash, IoHash::Hasher> Needs;
	};

	struct LoadAttachmentResult : public Result
	{
		IoBuffer Bytes;
	};

	struct LoadContainerResult : public Result
	{
		CbObject ContainerObject;
	};

	struct LoadAttachmentsResult : public Result
	{
		std::vector<std::pair<IoHash, CompressedBuffer>> Chunks;
	};

	struct RemoteStoreInfo
	{
		bool		CreateBlocks;
		bool		UseTempBlockFiles;
		bool		AllowChunking;
		std::string ContainerName;
		std::string BaseContainerName;
		std::string Description;
	};

	struct Stats
	{
		std::uint64_t m_SentBytes;
		std::uint64_t m_ReceivedBytes;
		std::uint64_t m_RequestTimeNS;
		std::uint64_t m_RequestCount;
		std::uint64_t m_PeakSentBytes;
		std::uint64_t m_PeakReceivedBytes;
		std::uint64_t m_PeakBytesPerSec;
	};

	RemoteProjectStore();
	virtual ~RemoteProjectStore();

	virtual RemoteStoreInfo GetInfo() const	 = 0;
	virtual Stats			GetStats() const = 0;

	virtual SaveResult			  SaveContainer(const IoBuffer& Payload)								= 0;
	virtual SaveAttachmentResult  SaveAttachment(const CompositeBuffer& Payload, const IoHash& RawHash) = 0;
	virtual FinalizeResult		  FinalizeContainer(const IoHash& RawHash)								= 0;
	virtual SaveAttachmentsResult SaveAttachments(const std::vector<SharedBuffer>& Payloads)			= 0;

	virtual LoadContainerResult	  LoadContainer()										= 0;
	virtual LoadContainerResult	  LoadBaseContainer()									= 0;
	virtual LoadAttachmentResult  LoadAttachment(const IoHash& RawHash)					= 0;
	virtual HasAttachmentsResult  HasAttachments(const std::span<IoHash> RawHashes)		= 0;
	virtual LoadAttachmentsResult LoadAttachments(const std::vector<IoHash>& RawHashes) = 0;
};

struct RemoteStoreOptions
{
	static const size_t DefaultMaxBlockSize		  = 64u * 1024u * 1024u;
	static const size_t DefaultMaxChunkEmbedSize  = 3u * 512u * 1024u;
	static const size_t DefaultChunkFileSizeLimit = 256u * 1024u * 1024u;

	size_t MaxBlockSize		  = DefaultMaxBlockSize;
	size_t MaxChunkEmbedSize  = DefaultMaxChunkEmbedSize;
	size_t ChunkFileSizeLimit = DefaultChunkFileSizeLimit;
};

typedef std::function<IoBuffer(const IoHash& AttachmentHash)> TGetAttachmentBufferFunc;
typedef std::function<CompositeBuffer(const IoHash& RawHash)> FetchChunkFunc;

RemoteProjectStore::LoadContainerResult BuildContainer(
	CidStore&																	 ChunkStore,
	ProjectStore::Project&														 Project,
	ProjectStore::Oplog&														 Oplog,
	size_t																		 MaxBlockSize,
	size_t																		 MaxChunkEmbedSize,
	size_t																		 ChunkFileSizeLimit,
	bool																		 BuildBlocks,
	bool																		 IgnoreMissingAttachments,
	bool																		 AllowChunking,
	const std::function<void(CompressedBuffer&&, const IoHash&)>&				 AsyncOnBlock,
	const std::function<void(const IoHash&, TGetAttachmentBufferFunc&&)>&		 OnLargeAttachment,
	const std::function<void(std::vector<std::pair<IoHash, FetchChunkFunc>>&&)>& OnBlockChunks,
	bool																		 EmbedLooseFiles);

class JobContext;

RemoteProjectStore::Result SaveOplogContainer(ProjectStore::Oplog&									  Oplog,
											  const CbObject&										  ContainerObject,
											  const std::function<void(std::span<IoHash> RawHashes)>& OnReferencedAttachments,
											  const std::function<bool(const IoHash& RawHash)>&		  HasAttachment,
											  const std::function<void(const IoHash& BlockHash, std::vector<IoHash>&& Chunks)>& OnNeedBlock,
											  const std::function<void(const IoHash& RawHash)>&		 OnNeedAttachment,
											  const std::function<void(const ChunkedInfo& Chunked)>& OnChunkedAttachment,
											  JobContext*											 OptionalContext);

RemoteProjectStore::Result SaveOplog(CidStore&				ChunkStore,
									 RemoteProjectStore&	RemoteStore,
									 ProjectStore::Project& Project,
									 ProjectStore::Oplog&	Oplog,
									 size_t					MaxBlockSize,
									 size_t					MaxChunkEmbedSize,
									 size_t					ChunkFileSizeLimit,
									 bool					EmbedLooseFiles,
									 bool					ForceUpload,
									 bool					IgnoreMissingAttachments,
									 JobContext*			OptionalContext);

RemoteProjectStore::Result LoadOplog(CidStore&			  ChunkStore,
									 RemoteProjectStore&  RemoteStore,
									 ProjectStore::Oplog& Oplog,
									 bool				  ForceDownload,
									 bool				  IgnoreMissingAttachments,
									 bool				  CleanOplog,
									 JobContext*		  OptionalContext);

CompressedBuffer GenerateBlock(std::vector<std::pair<IoHash, FetchChunkFunc>>&& FetchChunks);
bool IterateBlock(const SharedBuffer& BlockPayload, std::function<void(CompressedBuffer&& Chunk, const IoHash& AttachmentHash)> Visitor);

}  // namespace zen
