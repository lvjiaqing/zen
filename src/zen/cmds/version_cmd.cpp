// Copyright Epic Games, Inc. All Rights Reserved.

#include "version_cmd.h"

#include <zencore/config.h>
#include <zencore/filesystem.h>
#include <zencore/fmtutils.h>
#include <zencore/logging.h>
#include <zenhttp/httpcommon.h>
#include <zenutil/zenserverprocess.h>

#include <memory>

ZEN_THIRD_PARTY_INCLUDES_START
#include <cpr/cpr.h>
ZEN_THIRD_PARTY_INCLUDES_END

namespace zen {

VersionCommand::VersionCommand()
{
	m_Options.add_options()("h,help", "Print help");
	m_Options.add_option("", "u", "hosturl", "Host URL", cxxopts::value(m_HostName), "[hosturl]");
	m_Options.add_option("", "d", "detailed", "Detailed Version", cxxopts::value(m_DetailedVersion), "[detailedversion]");
	m_Options.parse_positional({"hosturl"});
}

VersionCommand::~VersionCommand() = default;

int
VersionCommand::Run(const ZenCliOptions& GlobalOptions, int argc, char** argv)
{
	ZEN_UNUSED(GlobalOptions);
	if (!ParseOptions(argc, argv))
	{
		return 0;
	}

	std::string Version;

	if (m_HostName.empty())
	{
		if (m_DetailedVersion)
		{
			Version = ZEN_CFG_VERSION_BUILD_STRING_FULL;
		}
		else
		{
			Version = ZEN_CFG_VERSION;
		}
	}
	else
	{
		const std::string UrlBase = fmt::format("{}/health", m_HostName);
		cpr::Session	  Session;
		std::string		  VersionRequest = fmt::format("{}/version{}", UrlBase, m_DetailedVersion ? "?detailed=true" : "");
		Session.SetUrl(VersionRequest);
		cpr::Response Response = Session.Get();
		if (!zen::IsHttpSuccessCode(Response.status_code))
		{
			if (Response.status_code)
			{
				ZEN_ERROR("{} failed: {}: {} ({})", VersionRequest, Response.status_code, Response.reason, Response.text);
			}
			else
			{
				ZEN_ERROR("{} failed: {}", VersionRequest, Response.error.message);
			}

			return 1;
		}
		Version = Response.text;
	}

	ZEN_CONSOLE("{}", Version);

	return 0;
}
}  // namespace zen
