// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include <zencore/zencore.h>

#include <string_view>

namespace zen {

class StatsProvider;

class StatsMetrics
{
public:
	virtual void Increment(std::string_view Metric)						 = 0;
	virtual void Decrement(std::string_view Metric)						 = 0;
	virtual void Count(std::string_view Metric, int64_t CountDelta)		 = 0;
	virtual void Gauge(std::string_view Metric, uint64_t CurrentValue)	 = 0;
	virtual void Meter(std::string_view Metric, uint64_t IncrementValue) = 0;

	// Not (yet) implemented: Set,Timing
protected:
	virtual ~StatsMetrics() = default;
};

class StatsProvider
{
public:
	virtual void ReportMetrics(StatsMetrics& Statsd) = 0;

protected:
	virtual ~StatsProvider() = default;
};

}  // namespace zen
