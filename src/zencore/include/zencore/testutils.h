// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#if ZEN_WITH_TESTS

#	include <zencore/iobuffer.h>
#	include <filesystem>

namespace zen {

std::filesystem::path CreateTemporaryDirectory();

class ScopedTemporaryDirectory
{
public:
	explicit ScopedTemporaryDirectory(std::filesystem::path Directory);
	ScopedTemporaryDirectory();
	~ScopedTemporaryDirectory();

	std::filesystem::path& Path() { return m_RootPath; }

private:
	std::filesystem::path m_RootPath;
};

struct ScopedCurrentDirectoryChange
{
	std::filesystem::path OldPath{std::filesystem::current_path()};

	ScopedCurrentDirectoryChange() { std::filesystem::current_path(CreateTemporaryDirectory()); }
	~ScopedCurrentDirectoryChange() { std::filesystem::current_path(OldPath); }
};

IoBuffer CreateRandomBlob(uint64_t Size);

struct FalseType
{
	static const bool Enabled = false;
};
struct TrueType
{
	static const bool Enabled = true;
};

}  // namespace zen

#endif	// ZEN_WITH_TESTS
