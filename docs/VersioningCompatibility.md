# Zen Storage Server Versioning

Any API changes made in the Zen Storage Server must be backwards compatible. That is to say, a newer 
version of the server must be compatible with older versions of client code (typically, the Unreal Engine
editor or runtime). This means you can use any newer version of the server with some version of the engine.
For example, using Zen Server 5.5.0 with an engine version of 5.4.3 would be perfectly valid and in some
cases this may be desirable to get improvements in performance or functionality from newer versions of
the server without upgrading the engine.

Newer clients may not be backwards compatible however, which means that you will generally not be able to 
use an older server version with more recent UE versions.

The server is versioned such that you can easily correlate the server version with the engine version. Thus
in the Unreal Engine v5.4.0 release the server will be tagged with 5.4.0 which indicates that it is 
compatible with the 5.4.0 (and earlier) runtime. As updates are made, new versions may be released and these
will have a higher minor version number (5.4.1, 5.4.2 etc) but these will not necessarily line up with point
releases of the engine since there may be more or fewer updates to the server than to the engine.
