// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include <zencore/zencore.h>

namespace zen {

uint32_t MemCrc32(const void* InData, size_t Length, uint32_t Crc = 0);
uint32_t MemCrc32_Deprecated(const void* InData, size_t Length, uint32_t Crc = 0);
uint32_t StrCrc_Deprecated(const char* Data);

}  // namespace zen
