// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include <zencore/thread.h>
#include <zenhttp/httpserver.h>

namespace zen {

/**
 * @brief Null implementation of "http" server. Does nothing
 */

class HttpNullServer : public HttpServer
{
public:
	HttpNullServer();
	~HttpNullServer();

	virtual void RegisterService(HttpService& Service) override;
	virtual int	 Initialize(int BasePort, std::filesystem::path DataDir) override;
	virtual void Run(bool IsInteractiveSession) override;
	virtual void RequestExit() override;
	virtual void Close() override;

private:
	Event m_ShutdownEvent;
};

}  // namespace zen
