<?xml version="1.0" encoding="utf-8"?>
<TpsData xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <Name>rapidyaml</Name>
  <!-- Software Name and Version  -->
<!-- Software Name: rapidyaml
    Version: 0.5.0 -->
  <Location>https://github.com/EpicGames/zen</Location>
  <Function>The library parses text files in the YAML format, it can also generate said text file format</Function>
  <Eula>https://github.com/biojppm/rapidyaml/blob/master/LICENSE.txt</Eula>
  <RedistributeTo>
    <EndUserGroup>Licencees</EndUserGroup>
    <EndUserGroup>P4</EndUserGroup>
    <EndUserGroup>Git</EndUserGroup>
  </RedistributeTo>
  <LicenseFolder>/Engine/Source/ThirdParty/Licenses</LicenseFolder>
</TpsData>
 


 