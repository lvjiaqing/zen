// Copyright Epic Games, Inc. All Rights Reserved.

#include "zenvfs/projfs.h"

#if ZEN_WITH_VFS
#	if ZEN_PLATFORM_WINDOWS
#		include <zencore/windows.h>

namespace zen {

bool
IsProjFsAvailable()
{
	HMODULE hMod = LoadLibraryA("projectedfslib.dll");

	if (hMod)
	{
		FreeLibrary(hMod);

		return true;
	}

	return false;
}

}  // namespace zen
#	else
namespace zen {

bool
IsProjFsAvailable()
{
	return false;
}

}  // namespace zen
#	endif
#endif
