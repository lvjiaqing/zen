-- Copyright Epic Games, Inc. All Rights Reserved.

target("zenstore-test")
    set_kind("binary")
    set_group("tests")
    add_headerfiles("**.h")
    add_files("*.cpp")
    add_deps("zenstore", "zencore")
    add_packages("vcpkg::doctest")
