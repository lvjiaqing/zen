<?xml version="1.0" encoding="utf-8"?>
<TpsData xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <Name>sol2</Name>
  <Location>https://github.com/EpicGames/zen/tree/main/thirdparty/</Location>
  <Function>Sol v2.0 - a C++ <-> Lua API wrapper with advanced features and top notch performance - is here, and it's great</Function>
  <Eula>https://github.com/ThePhD/sol2/blob/develop/LICENSE.txt</Eula>
  <RedistributeTo>
    <EndUserGroup>Licensees</EndUserGroup>
    <EndUserGroup>Git</EndUserGroup>
    <EndUserGroup>P4</EndUserGroup>
  </RedistributeTo>
  <LicenseFolder>https://github.com/EpicGames/zen/tree/main/thirdparty/licenses</LicenseFolder>
</TpsData>