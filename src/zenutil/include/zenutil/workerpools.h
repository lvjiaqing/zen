// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include <zencore/workthreadpool.h>

namespace zen {

// Worker pool with std::thread::hardware_concurrency() worker threads, but at least one thread
WorkerThreadPool& GetLargeWorkerPool();

// Worker pool with std::thread::hardware_concurrency() / 4 worker threads, but at least one thread
WorkerThreadPool& GetMediumWorkerPool();

// Worker pool with std::thread::hardware_concurrency() / 8 worker threads, but at least one thread
WorkerThreadPool& GetSmallWorkerPool();

// Special worker pool that does not use worker thread but issues all scheduled work on the calling thread
// This is useful for debugging when multiple async thread can make stepping in debugger complicated
WorkerThreadPool& GetSyncWorkerPool();

void ShutdownWorkerPools();

}  // namespace zen
