// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "../zen.h"

#include <zencore/filesystem.h>

namespace zen {

class StatusCommand : public ZenCmdBase
{
public:
	StatusCommand();
	~StatusCommand();

	virtual int				  Run(const ZenCliOptions& GlobalOptions, int argc, char** argv) override;
	virtual cxxopts::Options& Options() override { return m_Options; }

private:
	int GetLockFileEffectivePort() const;

	cxxopts::Options	  m_Options{"status", "Show zen status"};
	uint16_t			  m_Port;
	std::filesystem::path m_DataDir;
};

}  // namespace zen
