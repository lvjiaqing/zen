// Copyright Epic Games, Inc. All Rights Reserved.

#include "httptracer.h"

#include <zencore/fmtutils.h>
#include <zencore/logging.h>
#include <zencore/session.h>

namespace zen {

void
HttpServerTracer::Initialize(std::filesystem::path DataDir)
{
	m_DataDir	 = DataDir;
	m_PayloadDir = DataDir / "debug" / GetSessionIdString();

	ZEN_INFO("any debug payloads will be written to '{}'", m_PayloadDir);
}

void
HttpServerTracer::WriteDebugPayload(std::string_view Filename, const std::span<const IoBuffer> Payload)
{
	uint64_t					 PayloadSize = 0;
	std::vector<const IoBuffer*> Buffers;
	for (auto& Io : Payload)
	{
		Buffers.push_back(&Io);
		PayloadSize += Io.GetSize();
	}

	if (PayloadSize)
	{
		WriteFile(m_PayloadDir / Filename, Buffers.data(), Buffers.size());
	}
}

}  // namespace zen
