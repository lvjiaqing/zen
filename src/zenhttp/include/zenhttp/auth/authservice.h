// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include <zenhttp/httpserver.h>

namespace zen {

class AuthMgr;

class HttpAuthService final : public zen::HttpService
{
public:
	HttpAuthService(AuthMgr& AuthMgr);
	virtual ~HttpAuthService();

	virtual const char* BaseUri() const override;
	virtual void		HandleRequest(zen::HttpServerRequest& Request) override;

private:
	AuthMgr&		  m_AuthMgr;
	HttpRequestRouter m_Router;
};

}  // namespace zen
