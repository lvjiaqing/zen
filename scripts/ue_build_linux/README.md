# Build Zen with the UE Linux toolchain

This folder contains scripts to build Zen using the UE Linux toolchain. This
can be used to output binaries that meet the VFX Reference Platform versions.
It works by using the `--sysroot=` option to redirect compilers and linkers to
find headers and libraries. There are a few components involved;

1) get_ue_toolchain.sh <toolchain_dir>

```
$ scripts/ue_build_linux/get_ue_toolchain.sh ./.tmp-ue-toolchain
```

This will download the required components from cdn.unrealengine.com and
structure them in such a way that they can be used by both vcpkg and xmake
when building Zen.

2) ue_build.sh <toolchain_dir> <prog> [args...] 

Given the toolchain location downloaded in step (1) and the `VCPKG_ROOT`
environment variable is properly configured, this script sets up a suitable
environment and execs the "prog [args...]".

It is expected that this is used to invoke xmake to build Zen;

```
$ scripts/ue_build_linux/ue_build.sh .tmp-ue-toolchain xmake config --mode=debug
$ scripts/ue_build_linux/ue_build.sh .tmp-ue-toolchain xmake build
```

It is possible that `--toolchain=clang` may be required as a configuration
option. The `ue_build.sh` script can also be sourced into the current shell,
although it is worth noting that this has never been tried.

3) `scripts/ue_build_linux/clang` / `scripts/ue_build_linux/clang++`

These acts as shims to the binaries in `toolchain_dir`, adding in the required
command line arguments to use the correct headers and libraries.
The `ue_build.sh` script adjusts `$PATH` appropriately.
