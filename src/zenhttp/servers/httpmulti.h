// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include <zencore/thread.h>
#include <zenhttp/httpserver.h>

#include <memory>

namespace zen {

class HttpMultiServer : public HttpServer
{
public:
	HttpMultiServer();
	~HttpMultiServer();

	virtual void RegisterService(HttpService& Service) override;
	virtual int	 Initialize(int BasePort, std::filesystem::path DataDir) override;
	virtual void Run(bool IsInteractiveSession) override;
	virtual void RequestExit() override;
	virtual void Close() override;

	void AddServer(Ref<HttpServer> Server);

private:
	bool						 m_IsInitialized = false;
	Event						 m_ShutdownEvent;
	std::vector<Ref<HttpServer>> m_Servers;
};

}  // namespace zen
