name: Create Release
env:
  VCPKG_VERSION: 2024.03.25
  XMAKE_VERSION: 2.9.1
  WINDOWS_SDK_VERSION: 22621

on:
  push:
    paths:
      - 'VERSION.txt'

jobs:
  bundle-windows:
    runs-on: [windows, x64, zen]
    timeout-minutes: 25

    steps:
    - uses: actions/checkout@v3

    - name: Setup xmake
      uses: ue-foundation/github-action-setup-xmake@v1.1.1
      with:
        xmake-version: ${{env.XMAKE_VERSION}}

    - name: Installing vcpkg
      run: |
        git clone -b ${{env.VCPKG_VERSION}} --single-branch https://github.com/Microsoft/vcpkg.git .vcpkg
        cd .vcpkg
        .\bootstrap-vcpkg.bat
        cd ..

#    - name: Cache vcpkg
#      uses: actions/cache@v2
#      with:
#        path: |
#          ${{ github.workspace }}\.vcpkg\installed
#        key: ${{ runner.os }}-bundle-${{env.VCPKG_VERSION}}-${{ hashFiles('xmake.lua') }}-${{ matrix.arch }}-v1

    - name: Config
      run: |
        xmake config -v -y -m release
      env:
        VCPKG_ROOT: ${{ github.workspace }}/.vcpkg

    - name: Bundle
      run: |
        xmake bundle -v -y --codesignidentity="Epic Games"
      env:
        VCPKG_ROOT: ${{ github.workspace }}/.vcpkg

    - name: Upload To Sentry
      run: |
        scripts\sentry-cli --auth-token ${{ secrets.SENTRY_API_KEY }} upload-dif --org to --project zen-server --include-sources build/windows/x64/release/zenserver.pdb build/windows/x64/release/zenserver.exe build/windows/x64/release/zen.pdb build/windows/x64/release/zen.exe

    - name: Upload zenserver-win64
      uses: actions/upload-artifact@v3
      with:
        name: zenserver-win64
        path: build/zenserver-win64.zip

  bundle-linux:
    runs-on: [linux, x64, zen]
    timeout-minutes: 25

    steps:
    - uses: actions/checkout@v3

    - name: Install UE Toolchain
      run: |
        rm -rf ./.tmp-ue-toolchain
        ./scripts/ue_build_linux/get_ue_toolchain.sh ./.tmp-ue-toolchain

    - name: Setup xmake
      uses: ue-foundation/github-action-setup-xmake@v1.1.1
      with:
        xmake-version: ${{env.XMAKE_VERSION}}

    - name: Installing vcpkg
      run: |
        git clone -b ${{env.VCPKG_VERSION}} --single-branch https://github.com/Microsoft/vcpkg.git .vcpkg
        cd .vcpkg
        ./bootstrap-vcpkg.sh
        cd ..

#    - name: Cache vcpkg
#      uses: actions/cache@v2
#      with:
#        path: |
#          ${{ github.workspace }}/.vcpkg/installed
#        key: ${{ runner.os }}-bundle-${{env.VCPKG_VERSION}}-${{ hashFiles('xmake.lua') }}-${{ matrix.arch }}-v1

    - name: Config
      run: |
        ./scripts/ue_build_linux/ue_build.sh ./.tmp-ue-toolchain xmake config -v -y -m release
      env:
        VCPKG_ROOT: ${{ github.workspace }}/.vcpkg

    - name: Bundle
      run: |
        ./scripts/ue_build_linux/ue_build.sh ./.tmp-ue-toolchain xmake bundle -v -y
      env:
        VCPKG_ROOT: ${{ github.workspace }}/.vcpkg

    - name: Get Sentry CLI
      run: |
        curl -sL https://sentry.io/get-cli/ | bash
        ls -la ./scripts
      env:
        INSTALL_DIR: ./scripts

    - name: Upload To Sentry
      run: |
        scripts/sentry-cli --auth-token ${{ secrets.SENTRY_API_KEY }} upload-dif --org to --project zen-server --include-sources build/linux/x86_64/release/zenserver.sym build/linux/x86_64/release/zen.sym

    - name: Upload zenserver-linux
      uses: actions/upload-artifact@v3
      with:
        name: zenserver-linux
        path: build/zenserver-linux.zip
 
  bundle-macos:
    runs-on: [macos, x64, zen]
    timeout-minutes: 25

    steps:
    - uses: actions/checkout@v3

    - name: Setup xmake
      uses: ue-foundation/github-action-setup-xmake@v1.1.1
      with:
        xmake-version: ${{env.XMAKE_VERSION}}

    - name: Installing vcpkg
      run: |
        git clone -b ${{env.VCPKG_VERSION}} --single-branch https://github.com/Microsoft/vcpkg.git .vcpkg
        cd .vcpkg
        ./bootstrap-vcpkg.sh
        cd ..

#    - name: Cache vcpkg
#      uses: actions/cache@v2
#      with:
#        path: |
#          ${{ github.workspace }}/.vcpkg/installed
#        key: ${{ runner.os }}-bundle-${{env.VCPKG_VERSION}}-${{ hashFiles('xmake.lua') }}-${{ matrix.arch }}-v1

    - name: Config
      run: |
        xmake config -v -y -m release
      env:
        VCPKG_ROOT: ${{ github.workspace }}/.vcpkg

    - name: Bundle
      run: |
        xmake bundle -v -y --codesignidentity="Developer ID Application"
      env:
        VCPKG_ROOT: ${{ github.workspace }}/.vcpkg

    - name: Get Sentry CLI
      run: |
        curl -sL https://sentry.io/get-cli/ | bash
        ls -la ./scripts
      env:
        INSTALL_DIR: ./scripts

#    - name: Upload To Sentry arm64
#      run: |
#        scripts/sentry-cli --auth-token ${{ secrets.SENTRY_API_KEY }} upload-dif --org to --project zen-server --include-sources build/macosx/arm_64/release/zenserver.dSYM

    - name: Upload To Sentry x86_64
      run: |
        scripts/sentry-cli --auth-token ${{ secrets.SENTRY_API_KEY }} upload-dif --org to --project zen-server --include-sources build/macosx/x86_64/release/zenserver.dSYM build/macosx/x86_64/release/zen.dSYM

    - name: Upload zenserver-macos
      uses: actions/upload-artifact@v3
      with:
        name: zenserver-macos
        path: build/zenserver-macos.zip

  create-release:
    runs-on: [linux, x64, zen]
    timeout-minutes: 5
    needs: [bundle-linux, bundle-macos, bundle-windows]

    steps:
    - uses: actions/checkout@v3

    - name: Read VERSION.txt
      id: read_version
      uses: ue-foundation/read-file-action@v1.1.6
      with:
        path: "./VERSION.txt"

    - name: Get Sentry CLI
      run: |
        curl -sL https://sentry.io/get-cli/ | bash
        ls -la ./scripts
      env:
        INSTALL_DIR: ./scripts

    - name: Create Release in Sentry
      run: |
        scripts/sentry-cli --auth-token ${{ secrets.SENTRY_API_KEY }} releases new --org to --project zen-server ${{steps.read_version.outputs.content}}

    - name: Download Linux artifacts
      uses: actions/download-artifact@v1
      with:
        name: zenserver-linux
        path: linux

    - name: Download MacOS artifacts
      uses: actions/download-artifact@v1
      with:
        name: zenserver-macos
        path: macos

    - name: Download Windows artifacts
      uses: actions/download-artifact@v1
      with:
        name: zenserver-win64
        path: win64

    - name: Extract Version Changes
      run: |
        sed '1,/^##/!d;/##/d' CHANGELOG.md > CHANGELOG.tmp

    - name: Read CHANGELOG.tmp
      id: read_changelog
      uses: ue-foundation/read-file-action@v1.1.6
      with:
        path: "./CHANGELOG.tmp"

    - name: Check prerelease
      id: get-prerelease
      uses: ue-foundation/action-cond@v1.1.1
      with:
        cond: ${{contains(steps.read_version.outputs.content, '-pre')}}
        if_true: "true"
        if_false: "false"

    - name: Create Release
      id: create_release
      uses: ue-foundation/action-gh-release@v1
      env:
        GITHUB_TOKEN: ${{ secrets.GITHUB_TOKEN }}
      with:
        tag_name: v${{steps.read_version.outputs.content}}
        body: |
          ${{steps.read_changelog.outputs.content}}
        draft: false
        prerelease: ${{steps.get-prerelease.outputs.value}}
        files: |
          linux/zenserver-linux.zip
          win64/zenserver-win64.zip
          macos/zenserver-macos.zip

    - name: Finalize Release in Sentry
      run: |
        scripts/sentry-cli --auth-token ${{ secrets.SENTRY_API_KEY }} releases finalize --org to --project zen-server ${{steps.read_version.outputs.content}}
