-- Copyright Epic Games, Inc. All Rights Reserved.

--------------------------------------------------------------------------------
local function _exec(cmd, ...)
    local args = {}
    for _, arg in pairs({...}) do
        if arg then
            table.insert(args, arg)
        end
    end

    print("--", cmd, table.unpack(args))
    local ret = os.execv(cmd, args)
    print()
    return ret
end

--------------------------------------------------------------------------------
local function _zip(store_only, zip_path, ...)
    -- Here's the rules; if len(...) is 1 and it is a dir then create a zip with
    -- archive paths like this;
    --
    --   glob(foo/bar/**) -> foo/bar/abc, foo/bar/dir/123 -> zip(abc, dir/123)
    --
    -- Otherwise assume ... is file paths and add without leading directories;
    --
    --   foo/abc, bar/123 -> zip(abc, 123)

    zip_path = path.absolute(zip_path)
    os.tryrm(zip_path)

    local inputs = {...}

    local source_dir = nil
    if #inputs == 1 and os.isdir(inputs[1]) then
        source_dir = inputs[1]
    end

    import("detect.tools.find_7z")
    local cmd_7z = find_7z()
    if cmd_7z then
        input_paths = {}
        if source_dir then
            -- Suffixing a directory path with a "/." will have 7z set the path
            -- for archived files relative to that directory.
            input_paths = { path.join(source_dir, ".") }
        else
            for _, input_path in pairs(inputs) do
                -- If there is a "/./" anywhere in file paths then 7z drops all
                -- directory information and just archives the file by name
                input_path = path.relative(input_path, ".")
                if input_path:sub(2,2) ~= ":" then
                    input_path = "./"..input_path
                end
                table.insert(input_paths, input_path)
            end
        end

        compression_level = "-mx1"
        if store_only then
            compression_level = "-mx0"
        end

        local ret = _exec(cmd_7z, "a", compression_level, zip_path, table.unpack(input_paths))
        if ret > 0 then
            raise("Received error from 7z")
        end
        return
    end

    print("7z not found, falling back to zip")

    import("detect.tools.find_zip")
    zip_cmd = find_zip()
    if zip_cmd then
        local input_paths = inputs
        local cwd = os.curdir()
        if source_dir then
            os.cd(source_dir)
            input_paths = { "." }
        end

        compression_level = "-1"
        if store_only then
            compression_level = "-0"
        end

        local strip_leading_path = nil
        if not source_dir then
            strip_leading_path = "--junk-paths"
        end

        local ret = _exec(zip_cmd, "-r", compression_level, strip_leading_path, zip_path, table.unpack(input_paths))
        if ret > 0 then
            raise("Received error from zip")
        end

        os.cd(cwd)
        return
    end
    print("zip not found")

    raise("Unable to find a suitable zip tool")
end

--------------------------------------------------------------------------------
function main()
    local zip_path = "src/zenserver/frontend/html.zip"
    local content_dir = "src/zenserver/frontend/html/"
    _zip(true, zip_path, content_dir)
end
