// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "../zen.h"

namespace zen {

class RunCommand : public ZenCmdBase
{
public:
	RunCommand();
	~RunCommand();

	virtual int				  Run(const ZenCliOptions& GlobalOptions, int argc, char** argv) override;
	virtual cxxopts::Options& Options() override { return m_Options; }
	virtual ZenCmdCategory&	  CommandCategory() const override { return g_UtilitiesCategory; }

private:
	cxxopts::Options m_Options{"run", "Run executable"};
	int				 m_RunCount = 0;
	int				 m_RunTime	= -1;
	std::string		 m_BaseDirectory;
	int				 m_MaxBaseDirectoryCount = 10;
};

}  // namespace zen
