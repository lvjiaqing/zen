// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include <zencore/iobuffer.h>
#include <zencore/iohash.h>
#include <zenstore/gc.h>

#include <gsl/gsl-lite.hpp>
#include <unordered_map>

namespace zen {

namespace access_tracking {

	struct KeyAccessTime
	{
		IoHash		  Key;
		GcClock::Tick LastAccess{};
	};

	struct AccessTimes
	{
		std::unordered_map<std::string, std::vector<KeyAccessTime>> Buckets;
	};
};	// namespace access_tracking

struct ZenCacheValue
{
	IoBuffer Value;
	uint64_t RawSize = 0;
	IoHash	 RawHash = IoHash::Zero;
};

struct CacheValueDetails
{
	struct ValueDetails
	{
		uint64_t			Size;
		uint64_t			RawSize;
		IoHash				RawHash;
		GcClock::Tick		LastAccess{};
		std::vector<IoHash> Attachments;
		ZenContentType		ContentType;
	};

	struct BucketDetails
	{
		std::unordered_map<IoHash, ValueDetails, IoHash::Hasher> Values;
	};

	struct NamespaceDetails
	{
		std::unordered_map<std::string, BucketDetails> Buckets;
	};

	std::unordered_map<std::string, NamespaceDetails> Namespaces;
};

bool IsKnownBadBucketName(std::string_view BucketName);
bool ValidateIoBuffer(ZenContentType ContentType, IoBuffer Buffer);

//////////////////////////////////////////////////////////////////////////

// This store the access time as seconds since epoch internally in a 32-bit value giving is a range of 136 years since epoch
struct AccessTime
{
	explicit AccessTime(GcClock::Tick Tick) noexcept : SecondsSinceEpoch(ToSeconds(Tick)) {}
	AccessTime& operator=(GcClock::Tick Tick) noexcept
	{
		SecondsSinceEpoch.store(ToSeconds(Tick), std::memory_order_relaxed);
		return *this;
	}
	operator GcClock::Tick() const noexcept
	{
		return std::chrono::duration_cast<GcClock::Duration>(std::chrono::seconds(SecondsSinceEpoch.load(std::memory_order_relaxed)))
			.count();
	}

	AccessTime(AccessTime&& Rhs) noexcept : SecondsSinceEpoch(Rhs.SecondsSinceEpoch.load(std::memory_order_relaxed)) {}
	AccessTime(const AccessTime& Rhs) noexcept : SecondsSinceEpoch(Rhs.SecondsSinceEpoch.load(std::memory_order_relaxed)) {}
	AccessTime& operator=(AccessTime&& Rhs) noexcept
	{
		SecondsSinceEpoch.store(Rhs.SecondsSinceEpoch.load(std::memory_order_relaxed), std::memory_order_relaxed);
		return *this;
	}
	AccessTime& operator=(const AccessTime& Rhs) noexcept
	{
		SecondsSinceEpoch.store(Rhs.SecondsSinceEpoch.load(std::memory_order_relaxed), std::memory_order_relaxed);
		return *this;
	}

private:
	static uint32_t ToSeconds(GcClock::Tick Tick)
	{
		return gsl::narrow<uint32_t>(std::chrono::duration_cast<std::chrono::seconds>(GcClock::Duration(Tick)).count());
	}
	std::atomic_uint32_t SecondsSinceEpoch;
};

}  // namespace zen
