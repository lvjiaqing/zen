// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include <zencore/compactbinary.h>
#include <zencore/compress.h>
#include <zencore/iobuffer.h>
#include <zencore/iohash.h>
#include <zencore/stats.h>
#include <zencore/zencore.h>
#include <zenutil/cache/cache.h>

#include <functional>
#include <memory>
#include <string>
#include <vector>

namespace zen {

class CbObjectView;
class CbPackage;

struct UpstreamCacheRecord
{
	ZenContentType		Type = ZenContentType::kBinary;
	std::string			Namespace;
	CacheKey			Key;
	std::vector<IoHash> ValueContentIds;
	CacheRequestContext Context;
};

struct UpstreamError
{
	int32_t		ErrorCode{};
	std::string Reason{};

	explicit operator bool() const { return ErrorCode != 0; }
};

struct UpstreamEndpointInfo
{
	std::string Name;
	std::string Url;
};

struct GetUpstreamCacheResult
{
	UpstreamError Error{};
	int64_t		  Bytes{};
	double		  ElapsedSeconds{};
	bool		  Success = false;
};

struct PutUpstreamCacheResult
{
	std::string Reason;
	int64_t		Bytes{};
	double		ElapsedSeconds{};
	bool		Success = false;
};

struct CacheRecordGetCompleteParams
{
	CacheKeyRequest&			Request;
	const CbObjectView&			Record;
	const CbPackage&			Package;
	double						ElapsedSeconds{};
	const UpstreamEndpointInfo* Source = nullptr;
};

using OnCacheRecordGetComplete = std::function<void(CacheRecordGetCompleteParams&&)>;

struct CacheValueGetCompleteParams
{
	CacheValueRequest&			Request;
	IoHash						RawHash;
	uint64_t					RawSize;
	IoBuffer					Value;
	double						ElapsedSeconds{};
	const UpstreamEndpointInfo* Source = nullptr;
};

using OnCacheValueGetComplete = std::function<void(CacheValueGetCompleteParams&&)>;

struct CacheChunkGetCompleteParams
{
	CacheChunkRequest&			Request;
	IoHash						RawHash;
	uint64_t					RawSize;
	IoBuffer					Value;
	double						ElapsedSeconds{};
	const UpstreamEndpointInfo* Source = nullptr;
};

using OnCacheChunksGetComplete = std::function<void(CacheChunkGetCompleteParams&&)>;

class UpstreamCacheClient
{
public:
	virtual ~UpstreamCacheClient() = default;

	virtual bool IsActive() = 0;

	virtual void GetCacheValues(std::string_view			  Namespace,
								std::span<CacheValueRequest*> CacheValueRequests,
								OnCacheValueGetComplete&&	  OnComplete) = 0;

	virtual void GetCacheRecords(std::string_view			 Namespace,
								 std::span<CacheKeyRequest*> Requests,
								 OnCacheRecordGetComplete&&	 OnComplete) = 0;

	virtual void GetCacheChunks(std::string_view			  Namespace,
								std::span<CacheChunkRequest*> CacheChunkRequests,
								OnCacheChunksGetComplete&&	  OnComplete) = 0;

	virtual void EnqueueUpstream(UpstreamCacheRecord CacheRecord) = 0;
};

}  // namespace zen
