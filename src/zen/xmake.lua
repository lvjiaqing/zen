-- Copyright Epic Games, Inc. All Rights Reserved.

target("zen")
    set_kind("binary")
    add_headerfiles("**.h")
    add_files("**.cpp")
    add_files("zen.cpp", {unity_ignored  = true })
    add_deps("zencore", "zenhttp", "zenstore", "zenutil")
    add_includedirs(".")
    set_symbols("debug")

    if is_mode("release") then
        set_optimize("fastest")
    end

    if is_plat("windows") then
        add_files("zen.rc")
        add_ldflags("/subsystem:console,5.02")
        add_ldflags("/LTCG")
        add_links("crypt32", "wldap32", "Ws2_32")
    end

    if is_plat("macosx") then
        add_ldflags("-framework CoreFoundation")
        add_ldflags("-framework Security")
        add_ldflags("-framework SystemConfiguration")
        add_syslinks("bsm")
    end

    add_packages("vcpkg::cpr", "vcpkg::cxxopts", "vcpkg::mimalloc", "vcpkg::fmt")
