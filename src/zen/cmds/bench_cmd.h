// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "../zen.h"

namespace zen {

class BenchCommand : public ZenCmdBase
{
public:
	BenchCommand();
	~BenchCommand();

	virtual int				  Run(const ZenCliOptions& GlobalOptions, int argc, char** argv) override;
	virtual cxxopts::Options& Options() override { return m_Options; }
	virtual ZenCmdCategory&	  CommandCategory() const override { return g_UtilitiesCategory; }

private:
	cxxopts::Options m_Options{"bench", "Benchmarking utility command"};
	bool			 m_PurgeStandbyLists = false;
	bool			 m_SingleProcess	 = false;
};

}  // namespace zen
