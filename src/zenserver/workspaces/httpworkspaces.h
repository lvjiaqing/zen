// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include <zencore/stats.h>
#include <zenhttp/httpserver.h>
#include <zenhttp/httpstats.h>

namespace zen {

class Workspaces;

struct FileServeConfig
{
	std::filesystem::path SystemRootDir;
};

class HttpWorkspacesService final : public HttpService, public IHttpStatsProvider
{
public:
	HttpWorkspacesService(HttpStatsService& StatsService, const FileServeConfig& Cfg, Workspaces& Workspaces);
	virtual ~HttpWorkspacesService();

	virtual const char* BaseUri() const override;
	virtual void		HandleRequest(HttpServerRequest& Request) override;

	virtual void HandleStatsRequest(HttpServerRequest& Request) override;

private:
	struct WorkspacesStats
	{
		std::atomic_uint64_t WorkspaceReadCount{};
		std::atomic_uint64_t WorkspaceWriteCount{};
		std::atomic_uint64_t WorkspaceDeleteCount{};
		std::atomic_uint64_t WorkspaceShareReadCount{};
		std::atomic_uint64_t WorkspaceShareWriteCount{};
		std::atomic_uint64_t WorkspaceShareDeleteCount{};
		std::atomic_uint64_t WorkspaceShareFilesReadCount{};
		std::atomic_uint64_t WorkspaceShareEntriesReadCount{};
		std::atomic_uint64_t WorkspaceShareBatchReadCount{};
		std::atomic_uint64_t WorkspaceShareChunkHitCount{};
		std::atomic_uint64_t WorkspaceShareChunkMissCount{};
		std::atomic_uint64_t RequestCount{};
		std::atomic_uint64_t BadRequestCount{};
	};

	inline LoggerRef Log() { return m_Log; }

	LoggerRef m_Log;

	void				  Initialize();
	std::filesystem::path GetStatePath() const;
	void				  ReadState();
	void				  WriteState();

	void FilesRequest(HttpRouterRequest& Req);
	void ChunkInfoRequest(HttpRouterRequest& Req);
	void BatchRequest(HttpRouterRequest& Req);
	void EntriesRequest(HttpRouterRequest& Req);
	void ChunkRequest(HttpRouterRequest& Req);
	void ShareRequest(HttpRouterRequest& Req);
	void WorkspaceRequest(HttpRouterRequest& Req);

	void ShareAliasFilesRequest(HttpRouterRequest& Req);
	void ShareAliasChunkInfoRequest(HttpRouterRequest& Req);
	void ShareAliasBatchRequest(HttpRouterRequest& Req);
	void ShareAliasEntriesRequest(HttpRouterRequest& Req);
	void ShareAliasChunkRequest(HttpRouterRequest& Req);
	void ShareAliasRequest(HttpRouterRequest& Req);

	void FilesRequest(HttpRouterRequest& Req, const Oid& WorkspaceId, const Oid& ShareId);
	void ChunkInfoRequest(HttpRouterRequest& Req, const Oid& WorkspaceId, const Oid& ShareId, const Oid& ChunkId);
	void BatchRequest(HttpRouterRequest& Req, const Oid& WorkspaceId, const Oid& ShareId);
	void EntriesRequest(HttpRouterRequest& Req, const Oid& WorkspaceId, const Oid& ShareId);
	void ChunkRequest(HttpRouterRequest& Req, const Oid& WorkspaceId, const Oid& ShareId, const Oid& ChunkId);
	void ShareRequest(HttpRouterRequest& Req, const Oid& WorkspaceId, const Oid& InShareId);

	HttpStatsService&		 m_StatsService;
	const FileServeConfig	 m_Config;
	HttpRequestRouter		 m_Router;
	Workspaces&				 m_Workspaces;
	WorkspacesStats			 m_WorkspacesStats;
	metrics::OperationTiming m_HttpRequests;
};

}  // namespace zen
