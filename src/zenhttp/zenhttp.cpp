// Copyright Epic Games, Inc. All Rights Reserved.

#include <zenhttp/zenhttp.h>

#if ZEN_WITH_TESTS

#	include <zenhttp/httpclient.h>
#	include <zenhttp/httpserver.h>
#	include <zenutil/packageformat.h>

namespace zen {

void
zenhttp_forcelinktests()
{
	http_forcelink();
	httpclient_forcelink();
}

}  // namespace zen

#endif
