// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include <zencore/logging.h>
#include <zenhttp/httpserver.h>

#include <map>

namespace zen {

class HttpStatsService : public HttpService, public IHttpStatsService
{
public:
	HttpStatsService();
	~HttpStatsService();

	virtual const char* BaseUri() const override;
	virtual void		HandleRequest(HttpServerRequest& Request) override;
	virtual void		RegisterHandler(std::string_view Id, IHttpStatsProvider& Provider) override;
	virtual void		UnregisterHandler(std::string_view Id, IHttpStatsProvider& Provider) override;

private:
	LoggerRef		  m_Log;
	HttpRequestRouter m_Router;

	inline LoggerRef Log() { return m_Log; }

	RwLock									   m_Lock;
	std::map<std::string, IHttpStatsProvider*> m_Providers;
};

}  // namespace zen
