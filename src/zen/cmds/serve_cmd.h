// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "../zen.h"

namespace zen {

/** File serving
 */
class ServeCommand : public ZenCmdBase
{
public:
	ServeCommand();
	~ServeCommand();

	virtual int				  Run(const ZenCliOptions& GlobalOptions, int argc, char** argv) override;
	virtual cxxopts::Options& Options() override { return m_Options; }

private:
	cxxopts::Options m_Options{"serve", "Serve files from a tree"};
	std::string		 m_HostName;
	std::string		 m_ProjectName;
	std::string		 m_OplogName;
	std::string		 m_RootPath;
};

}  // namespace zen
