// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include <zenhttp/httpserver.h>

#ifndef ZEN_WITH_HTTPSYS
#	if ZEN_PLATFORM_WINDOWS
#		define ZEN_WITH_HTTPSYS 1
#	else
#		define ZEN_WITH_HTTPSYS 0
#	endif
#endif

namespace zen {

struct HttpSysConfig
{
	unsigned int ThreadCount			 = 0;
	unsigned int AsyncWorkThreadCount	 = 0;
	bool		 IsAsyncResponseEnabled	 = true;
	bool		 IsRequestLoggingEnabled = false;
	bool		 IsDedicatedServer		 = false;
	bool		 ForceLoopback			 = false;
};

Ref<HttpServer> CreateHttpSysServer(HttpSysConfig Config);

}  // namespace zen
