// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include <zencore/zencore.h>
#include <string_view>

namespace zen {

struct Oid;

ZENCORE_API [[nodiscard]] Oid			   GetSessionId();
ZENCORE_API [[nodiscard]] std::string_view GetSessionIdString();

}  // namespace zen
