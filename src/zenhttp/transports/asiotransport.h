// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include <zenhttp/httpplugin.h>

#if ZEN_WITH_PLUGINS

namespace zen {

TransportPlugin* CreateAsioTransportPlugin();

}  // namespace zen

#endif
