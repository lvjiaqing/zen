// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include <zencore/iobuffer.h>

#include <unordered_map>

namespace zen {

//////////////////////////////////////////////////////////////////////////
class ZipFs
{
public:
	ZipFs() = default;
	ZipFs(IoBuffer&& Buffer);
	IoBuffer GetFile(const std::string_view& FileName) const;
	inline	 operator bool() const { return !m_Files.empty(); }

private:
	using FileItem = MemoryView;
	using FileMap  = std::unordered_map<std::string_view, FileItem>;
	FileMap mutable m_Files;
	IoBuffer m_Buffer;
};

}  // namespace zen
