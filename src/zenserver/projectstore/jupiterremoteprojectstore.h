// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "remoteprojectstore.h"

namespace zen {

class AuthMgr;

struct JupiterRemoteStoreOptions : RemoteStoreOptions
{
	std::string Url;
	std::string Namespace;
	std::string Bucket;
	IoHash		Key;
	IoHash		OptionalBaseKey;
	std::string OpenIdProvider;
	std::string AccessToken;
	AuthMgr&	AuthManager;
	bool		ForceDisableBlocks	   = false;
	bool		ForceDisableTempBlocks = false;
	bool		AssumeHttp2			   = false;
};

std::shared_ptr<RemoteProjectStore> CreateJupiterRemoteStore(const JupiterRemoteStoreOptions& Options,
															 const std::filesystem::path&	  TempFilePath);

}  // namespace zen
