// Copyright Epic Games, Inc. All Rights Reserved.

#include <zencore/endian.h>
#include <zencore/intmath.h>

#include <zencore/testing.h>

namespace zen {

//////////////////////////////////////////////////////////////////////////
//
// Testing related code follows...
//

#if ZEN_WITH_TESTS

void
intmath_forcelink()
{
}

TEST_CASE("intmath")
{
	CHECK(FloorLog2(0x00) == 0);
	CHECK(FloorLog2(0x01) == 0);
	CHECK(FloorLog2(0x0f) == 3);
	CHECK(FloorLog2(0x10) == 4);
	CHECK(FloorLog2(0x11) == 4);
	CHECK(FloorLog2(0x12) == 4);
	CHECK(FloorLog2(0x22) == 5);
	CHECK(FloorLog2(0x0001'0000) == 16);
	CHECK(FloorLog2(0x0001'000f) == 16);
	CHECK(FloorLog2(0x8000'0000) == 31);

	CHECK(FloorLog2_64(0x00ull) == 0);
	CHECK(FloorLog2_64(0x01ull) == 0);
	CHECK(FloorLog2_64(0x0full) == 3);
	CHECK(FloorLog2_64(0x10ull) == 4);
	CHECK(FloorLog2_64(0x11ull) == 4);
	CHECK(FloorLog2_64(0x0001'0000ull) == 16);
	CHECK(FloorLog2_64(0x0001'000full) == 16);
	CHECK(FloorLog2_64(0x8000'0000ull) == 31);
	CHECK(FloorLog2_64(0x0000'0001'0000'0000ull) == 32);
	CHECK(FloorLog2_64(0x8000'0000'0000'0000ull) == 63);

	CHECK(CountLeadingZeros64(0x8000'0000'0000'0000ull) == 0);
	CHECK(CountLeadingZeros64(0x0000'0000'0000'0000ull) == 64);
	CHECK(CountLeadingZeros64(0x0000'0000'0000'0001ull) == 63);
	CHECK(CountLeadingZeros64(0x0000'0000'8000'0000ull) == 32);
	CHECK(CountLeadingZeros64(0x0000'0001'0000'0000ull) == 31);

	CHECK(CountTrailingZeros64(0x8000'0000'0000'0000ull) == 63);
	CHECK(CountTrailingZeros64(0x0000'0000'0000'0000ull) == 64);
	CHECK(CountTrailingZeros64(0x0000'0000'0000'0001ull) == 0);
	CHECK(CountTrailingZeros64(0x0000'0000'8000'0000ull) == 31);
	CHECK(CountTrailingZeros64(0x0000'0001'0000'0000ull) == 32);

	CHECK(ByteSwap(uint16_t(0x6d72)) == 0x726d);
	CHECK(ByteSwap(uint32_t(0x2741'3965)) == 0x6539'4127);
	CHECK(ByteSwap(uint64_t(0x214d'6172'7469'6e21ull)) == 0x216e'6974'7261'4d21ull);
}

#endif

}  // namespace zen
