// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include <zencore/compactbinary.h>
#include <zenhttp/httpserver.h>

namespace zen {

class GcScheduler;
class JobQueue;
class ZenCacheStore;
class CidStore;
class ProjectStore;
struct ZenServerOptions;

class HttpAdminService : public zen::HttpService
{
public:
	struct LogPaths
	{
		std::filesystem::path AbsLogPath;
		std::filesystem::path HttpLogPath;
		std::filesystem::path CacheLogPath;
	};
	HttpAdminService(GcScheduler&			 Scheduler,
					 JobQueue&				 BackgroundJobQueue,
					 ZenCacheStore*			 CacheStore,
					 CidStore*				 CidStore,
					 ProjectStore*			 ProjectStore,
					 const LogPaths&		 LogPaths,
					 const ZenServerOptions& ServerOptions);
	~HttpAdminService();

	virtual const char* BaseUri() const override;
	virtual void		HandleRequest(zen::HttpServerRequest& Request) override;

private:
	HttpRequestRouter		m_Router;
	GcScheduler&			m_GcScheduler;
	JobQueue&				m_BackgroundJobQueue;
	ZenCacheStore*			m_CacheStore;
	CidStore*				m_CidStore;
	ProjectStore*			m_ProjectStore;
	LogPaths				m_LogPaths;
	const ZenServerOptions& m_ServerOptions;
};

}  // namespace zen
