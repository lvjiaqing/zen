<?xml version="1.0" encoding="utf-8"?>
<TpsData xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <Name>rocksdb</Name>
  <Location>https://github.com/EpicGames/zen/tree/main/thirdparty/</Location>
  <Function>A library that provides an embeddable, persistent key-value store for fast storage</Function>
  <Eula>https://github.com/facebook/rocksdb/blob/main/LICENSE.Apache</Eula>
  <RedistributeTo>
    <EndUserGroup>Licensees</EndUserGroup>
    <EndUserGroup>Git</EndUserGroup>
    <EndUserGroup>P4</EndUserGroup>
  </RedistributeTo>
  <LicenseFolder>https://github.com/EpicGames/zen/tree/main/thirdparty/licenses</LicenseFolder>
</TpsData>