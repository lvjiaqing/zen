// Copyright Epic Games, Inc. All Rights Reserved.

#include "httpnull.h"

#include <zencore/logging.h>

#if ZEN_PLATFORM_WINDOWS
#	include <conio.h>
#endif

namespace zen {

HttpNullServer::HttpNullServer()
{
}

HttpNullServer::~HttpNullServer()
{
}

void
HttpNullServer::RegisterService(HttpService& Service)
{
	ZEN_UNUSED(Service);
}

int
HttpNullServer::Initialize(int BasePort, std::filesystem::path DataDir)
{
	ZEN_UNUSED(DataDir);
	return BasePort;
}

void
HttpNullServer::Run(bool IsInteractiveSession)
{
	const bool TestMode = !IsInteractiveSession;

	int WaitTimeout = -1;
	if (!TestMode)
	{
		WaitTimeout = 1000;
	}

#if ZEN_PLATFORM_WINDOWS
	if (TestMode == false)
	{
		ZEN_CONSOLE("Zen Server running (null HTTP). Press ESC or Q to quit");
	}

	do
	{
		if (!TestMode && _kbhit() != 0)
		{
			char c = (char)_getch();

			if (c == 27 || c == 'Q' || c == 'q')
			{
				RequestApplicationExit(0);
			}
		}

		m_ShutdownEvent.Wait(WaitTimeout);
	} while (!IsApplicationExitRequested());
#else
	if (TestMode == false)
	{
		ZEN_CONSOLE("Zen Server running (null HTTP). Ctrl-C to quit");
	}

	do
	{
		m_ShutdownEvent.Wait(WaitTimeout);
	} while (!IsApplicationExitRequested());
#endif
}

void
HttpNullServer::RequestExit()
{
	m_ShutdownEvent.Set();
}

void
HttpNullServer::Close()
{
}

}  // namespace zen
