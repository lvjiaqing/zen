-- Copyright Epic Games, Inc. All Rights Reserved.

target("zenhttp-test")
    set_kind("binary")
    set_group("tests")
    add_headerfiles("**.h")
    add_files("*.cpp")
    add_deps("zenhttp", "zencore")
    add_packages("vcpkg::doctest")

    if is_plat("macosx") then
        add_ldflags("-framework CoreFoundation")
        add_ldflags("-framework Security")
        add_ldflags("-framework SystemConfiguration")
    end
