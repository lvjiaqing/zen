-- Copyright Epic Games, Inc. All Rights Reserved.

target("zenserver")
    set_kind("binary")
    add_deps("zencore",
             "zenhttp",
             "zennet",
             "zenstore",
             "zenutil",
             "zenvfs")
    add_headerfiles("**.h")
    add_rules("utils.bin2c", {extensions = {".zip"}})
    add_files("**.cpp")
    add_files("frontend/*.zip")
    add_files("zenserver.cpp", {unity_ignored  = true })
    add_includedirs(".")
    set_symbols("debug")

    if is_mode("release") then
        set_optimize("fastest")
    end

    if is_plat("windows") then
        add_ldflags("/subsystem:console,5.02")
        add_ldflags("/MANIFEST:EMBED")
        add_ldflags("/LTCG")
        add_files("zenserver.rc")
        add_cxxflags("/bigobj")
        add_links("delayimp", "projectedfslib")
        add_ldflags("/delayload:ProjectedFSLib.dll")

        add_links("dbghelp", "winhttp", "version")    -- for Sentry
    else
        remove_files("windows/**")
    end

    if is_plat("macosx") then
        add_ldflags("-framework CoreFoundation")
        add_ldflags("-framework CoreGraphics")
        add_ldflags("-framework CoreText")
        add_ldflags("-framework Foundation")
        add_ldflags("-framework Security")
        add_ldflags("-framework SystemConfiguration")
        add_syslinks("bsm")
    end

    add_options("compute")
    add_options("exec")
    
    add_packages(
        "vcpkg::asio",
        "vcpkg::cxxopts",
        "vcpkg::http-parser",
        "vcpkg::json11",
        "vcpkg::lua",
        "vcpkg::mimalloc",
        "vcpkg::sol2"
    )

    if has_config("zensentry") then
        add_packages("vcpkg::sentry-native")
    end

    if is_plat("linux") then
        -- As sentry_native uses symbols from breakpad_client, the latter must
        -- be specified after the former with GCC-like toolchains. xmake however
        -- is unaware of this and simply globs files from vcpkg's output. The
        -- line below forces breakpad_client to be to the right of sentry_native
        add_syslinks("breakpad_client")
    end
