// Copyright Epic Games, Inc. All Rights Reserved.

#include "info_cmd.h"

#include <zencore/fmtutils.h>
#include <zencore/logging.h>
#include <zencore/string.h>
#include <zenhttp/httpclient.h>

using namespace std::literals;

namespace zen {

InfoCommand::InfoCommand()
{
	m_Options.add_options()("h,help", "Print help");
	m_Options.add_option("", "u", "hosturl", "Host URL", cxxopts::value(m_HostName)->default_value(""), "<hosturl>");
}

InfoCommand::~InfoCommand()
{
}

int
InfoCommand::Run(const ZenCliOptions& GlobalOptions, int argc, char** argv)
{
	ZEN_UNUSED(GlobalOptions);

	if (!ParseOptions(argc, argv))
	{
		return 0;
	}

	m_HostName = ResolveTargetHostSpec(m_HostName);

	if (m_HostName.empty())
	{
		throw OptionParseException("unable to resolve server specification");
	}

	HttpClient Http(m_HostName);

	if (HttpClient::Response Result = Http.Get("/admin/info", HttpClient::Accept(ZenContentType::kJSON)))
	{
		ZEN_CONSOLE("{}", Result.ToText());
	}

	return 0;
}

}  // namespace zen
