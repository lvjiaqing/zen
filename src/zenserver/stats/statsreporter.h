// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "config.h"

#include <zencore/thread.h>
#include <zenutil/statsreporter.h>

namespace zen {

class StatsDaemonClient;

class StatsReporter
{
public:
	StatsReporter();
	~StatsReporter();

	StatsReporter& operator=(const StatsReporter&) = delete;
	StatsReporter(const StatsReporter&)			   = delete;

	void Initialize(const ZenStatsConfig& Config);
	void Shutdown();
	void ReportStats();
	void AddProvider(StatsProvider* Provider);

private:
	RwLock							   m_Lock;
	std::unique_ptr<StatsDaemonClient> m_Statsd;
	std::vector<StatsProvider*>		   m_Providers;
};

}  // namespace zen
