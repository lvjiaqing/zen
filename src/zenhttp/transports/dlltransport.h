// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include <zenhttp/httpplugin.h>

#if ZEN_WITH_PLUGINS

namespace zen {

/** Transport plugin which supports dynamic loading of external transport
 * provider modules
 */

class DllTransportPlugin : public TransportPlugin
{
public:
	virtual void LoadDll(std::string_view Name)														 = 0;
	virtual void ConfigureDll(std::string_view Name, const char* OptionTag, const char* OptionValue) = 0;
};

DllTransportPlugin* CreateDllTransportPlugin();

}  // namespace zen

#endif
