// Copyright Epic Games, Inc. All Rights Reserved.

#include <zenstore/hashkeyset.h>

//////////////////////////////////////////////////////////////////////////

namespace zen {

void
HashKeySet::AddHashToSet(const IoHash& HashToAdd)
{
	m_HashSet.insert(HashToAdd);
}

void
HashKeySet::AddHashesToSet(std::span<const IoHash> HashesToAdd)
{
	m_HashSet.insert(HashesToAdd.begin(), HashesToAdd.end());
}

void
HashKeySet::RemoveHashesIf(std::function<bool(const IoHash& CandidateHash)>&& Predicate)
{
	for (auto It = begin(m_HashSet), ItEnd = end(m_HashSet); It != ItEnd;)
	{
		if (Predicate(*It))
		{
			It = m_HashSet.erase(It);
		}
		else
		{
			++It;
		}
	}
}

void
HashKeySet::IterateHashes(std::function<void(const IoHash& Hash)>&& Callback) const
{
	for (auto It = begin(m_HashSet), ItEnd = end(m_HashSet); It != ItEnd; ++It)
	{
		Callback(*It);
	}
}

//////////////////////////////////////////////////////////////////////////
//
// Testing related code follows...
//

#if ZEN_WITH_TESTS

void
hashkeyset_forcelink()
{
}

#endif

}  // namespace zen
