// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

namespace zen {

template<typename T>
void
_CombineHashes(size_t& Seed, const T& Val)
{
	Seed ^= std::hash<T>()(Val) + 0x9e3779b97f4a7c15ull + (Seed << 12) + (Seed >> 4);
}

/** Utility function to generate a high quality hash from multiple
	contributing items

	Example usage:

		const Guid A = GetA();
		const std::string B = GetB();

		const size_t AbHash = CombineHashes(A, B);

 */
template<typename... Types>
size_t
CombineHashes(const Types&... Args)
{
	size_t Seed = 0;
	(_CombineHashes(Seed, Args), ...);
	return Seed;
}

}  // namespace zen
