// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include <zencore/uid.h>

#include <optional>
#include <span>
#include <vector>

namespace zen {
class IoBuffer;

struct RequestChunkEntry
{
	Oid		 ChunkId;
	uint32_t CorrelationId;
	uint64_t Offset;
	uint64_t RequestBytes;
};

std::vector<IoBuffer>						  ParseChunkBatchResponse(const IoBuffer& Buffer);
IoBuffer									  BuildChunkBatchRequest(const std::vector<RequestChunkEntry>& Entries);
std::optional<std::vector<RequestChunkEntry>> ParseChunkBatchRequest(const IoBuffer& Payload);
std::vector<IoBuffer> BuildChunkBatchResponse(const std::vector<RequestChunkEntry>& Requests, std::span<IoBuffer> Chunks);

}  // namespace zen
