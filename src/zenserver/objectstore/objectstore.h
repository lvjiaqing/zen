// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include <zenhttp/httpserver.h>
#include <atomic>
#include <filesystem>
#include <mutex>

namespace zen {

class HttpRouterRequest;

struct ObjectStoreConfig
{
	struct BucketConfig
	{
		std::string			  Name;
		std::filesystem::path Directory;
	};

	std::filesystem::path	  RootDirectory;
	std::vector<BucketConfig> Buckets;
};

class HttpObjectStoreService final : public zen::HttpService
{
public:
	HttpObjectStoreService(ObjectStoreConfig Cfg);
	virtual ~HttpObjectStoreService();

	virtual const char* BaseUri() const override;
	virtual void		HandleRequest(zen::HttpServerRequest& Request) override;

private:
	void				  Inititalize();
	std::filesystem::path GetBucketDirectory(std::string_view BucketName);
	void				  CreateBucket(zen::HttpRouterRequest& Request);
	void				  ListBucket(zen::HttpRouterRequest& Request, const std::string& Path);
	void				  DeleteBucket(zen::HttpRouterRequest& Request);
	void				  GetObject(zen::HttpRouterRequest& Request, const std::string& Path);
	void				  PutObject(zen::HttpRouterRequest& Request);

	ObjectStoreConfig	 m_Cfg;
	std::mutex			 BucketsMutex;
	HttpRequestRouter	 m_Router;
	std::atomic_uint64_t TotalBytesServed{0};
};

}  // namespace zen
