// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "../zen.h"

namespace zen {

class DropCommand : public CacheStoreCommand
{
public:
	DropCommand();
	~DropCommand();

	virtual int				  Run(const ZenCliOptions& GlobalOptions, int argc, char** argv) override;
	virtual cxxopts::Options& Options() override { return m_Options; }

private:
	cxxopts::Options m_Options{"drop", "Drop cache namespace or bucket"};
	std::string		 m_HostName;
	std::string		 m_NamespaceName;
	std::string		 m_BucketName;
};

class CacheInfoCommand : public CacheStoreCommand
{
public:
	CacheInfoCommand();
	~CacheInfoCommand();
	virtual int				  Run(const ZenCliOptions& GlobalOptions, int argc, char** argv) override;
	virtual cxxopts::Options& Options() override { return m_Options; }

private:
	cxxopts::Options m_Options{"cache-info", "Info on cache, namespace or bucket"};
	std::string		 m_HostName;
	std::string		 m_NamespaceName;
	std::string		 m_BucketName;
};

class CacheStatsCommand : public CacheStoreCommand
{
public:
	CacheStatsCommand();
	~CacheStatsCommand();
	virtual int				  Run(const ZenCliOptions& GlobalOptions, int argc, char** argv) override;
	virtual cxxopts::Options& Options() override { return m_Options; }

private:
	cxxopts::Options m_Options{"cache-stats", "Stats info on cache"};
	std::string		 m_HostName;
};

class CacheDetailsCommand : public CacheStoreCommand
{
public:
	CacheDetailsCommand();
	~CacheDetailsCommand();
	virtual int				  Run(const ZenCliOptions& GlobalOptions, int argc, char** argv) override;
	virtual cxxopts::Options& Options() override { return m_Options; }

private:
	cxxopts::Options m_Options{"cache-details", "Detailed info on cache"};
	std::string		 m_HostName;
	bool			 m_CSV;
	bool			 m_Details;
	bool			 m_AttachmentDetails;
	std::string		 m_Namespace;
	std::string		 m_Bucket;
	std::string		 m_ValueKey;
};

}  // namespace zen
