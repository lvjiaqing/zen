// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include <zencore/string.h>

#include <vector>

namespace zen {

class OidcClient
{
public:
	struct Options
	{
		std::string_view BaseUrl;
		std::string_view ClientId;
	};

	OidcClient(const Options& Options);
	~OidcClient() = default;

	OidcClient(const OidcClient&) = delete;
	OidcClient& operator=(const OidcClient&) = delete;

	struct Result
	{
		std::string Reason;
		bool		Ok = false;
	};

	using InitResult = Result;

	InitResult Initialize();

	struct RefreshTokenResult
	{
		std::string TokenType;
		std::string AccessToken;
		std::string RefreshToken;
		std::string IdentityToken;
		std::string Scope;
		std::string Reason;
		int64_t		ExpiresInSeconds{};
		bool		Ok = false;
	};

	RefreshTokenResult RefreshToken(std::string_view RefreshToken);

private:
	using StringArray = std::vector<std::string>;

	struct OpenIdConfiguration
	{
		std::string Issuer;
		std::string AuthorizationEndpoint;
		std::string TokenEndpoint;
		std::string UserInfoEndpoint;
		std::string RegistrationEndpoint;
		std::string EndSessionEndpoint;
		std::string DeviceAuthorizationEndpoint;
		std::string JwksUri;
		StringArray SupportedResponseTypes;
		StringArray SupportedResponseModes;
		StringArray SupportedGrantTypes;
		StringArray SupportedScopes;
		StringArray SupportedTokenEndpointAuthMethods;
		StringArray SupportedClaims;
	};

	std::string			m_BaseUrl;
	std::string			m_ClientId;
	OpenIdConfiguration m_Config;
};

}  // namespace zen
