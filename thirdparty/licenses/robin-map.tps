<?xml version="1.0" encoding="utf-8"?>
<TpsData xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <Name>robin-map</Name>
  <Location>https://github.com/EpicGames/zen/tree/main/thirdparty/</Location>
  <Function>A C++ implementation of a fast hash map and hash set using robin hood hashing</Function>
  <Eula>https://github.com/Tessil/robin-map/blob/master/LICENSE</Eula>
  <RedistributeTo>
    <EndUserGroup>Licensees</EndUserGroup>
    <EndUserGroup>Git</EndUserGroup>
    <EndUserGroup>P4</EndUserGroup>
  </RedistributeTo>
  <LicenseFolder>https://github.com/EpicGames/zen/tree/main/thirdparty/licenses</LicenseFolder>
</TpsData>