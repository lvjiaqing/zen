// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "../zen.h"

namespace zen {

/** Print Compact Binary
 */
class PrintCommand : public ZenCmdBase
{
public:
	PrintCommand();
	~PrintCommand();

	virtual int				  Run(const ZenCliOptions& GlobalOptions, int argc, char** argv) override;
	virtual cxxopts::Options& Options() override { return m_Options; }
	virtual ZenCmdCategory&	  CommandCategory() const override { return g_UtilitiesCategory; }

private:
	cxxopts::Options m_Options{"print", "Print compact binary object"};
	std::string		 m_Filename;
};

/** Print Compact Binary Package
 */
class PrintPackageCommand : public ZenCmdBase
{
public:
	PrintPackageCommand();
	~PrintPackageCommand();

	virtual int				  Run(const ZenCliOptions& GlobalOptions, int argc, char** argv) override;
	virtual cxxopts::Options& Options() override { return m_Options; }
	virtual ZenCmdCategory&	  CommandCategory() const override { return g_UtilitiesCategory; }

private:
	cxxopts::Options m_Options{"printpkg", "Print compact binary package"};
	std::string		 m_Filename;
};

}  // namespace zen
