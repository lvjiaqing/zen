// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "../zen.h"

namespace zen {

class VersionCommand : public ZenCmdBase
{
public:
	VersionCommand();
	~VersionCommand();

	virtual int				  Run(const ZenCliOptions& GlobalOptions, int argc, char** argv) override;
	virtual cxxopts::Options& Options() override { return m_Options; }

private:
	cxxopts::Options m_Options{"version", "Get zen service version"};
	std::string		 m_HostName;
	bool			 m_DetailedVersion;
};

}  // namespace zen
