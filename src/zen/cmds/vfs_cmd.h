// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "../zen.h"

namespace zen {

class VfsCommand : public StorageCommand
{
public:
	VfsCommand();
	~VfsCommand();

	virtual int				  Run(const ZenCliOptions& GlobalOptions, int argc, char** argv) override;
	virtual cxxopts::Options& Options() override { return m_Options; }

private:
	cxxopts::Options m_Options{"vfs", "Manage virtual file system"};

	std::string m_Verb;
	std::string m_HostName;
	std::string m_MountPath;
};

}  // namespace zen
