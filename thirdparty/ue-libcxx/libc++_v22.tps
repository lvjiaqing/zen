<?xml version="1.0" encoding="utf-8"?>
<TpsData xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <Name>libc++ </Name>
  <!-- Software Name and Version  -->
<!-- Software Name: libc++
    Version:  v22_clang-16.0.6-centos7-->
<!-- Notes: v22_clang-16.0.6-centos7
 -->
  <Location>https://github.com/EpicGames/zen/thirdparty/ue-libcxx</Location>
  <Function>libc++ is an implementation of the C++ standard library, targeting C++11 and above.</Function>
  <Eula>https://github.com/llvm/llvm-project/blob/main/libcxx/LICENSE.TXT</Eula>
  <RedistributeTo>
    <EndUserGroup>Licencees</EndUserGroup>
    <EndUserGroup>P4</EndUserGroup>
     <EndUserGroup>Git</EndUserGroup>
  </RedistributeTo>
  <LicenseFolder>/Engine/Source/ThirdParty/Licenses</LicenseFolder>
</TpsData>
