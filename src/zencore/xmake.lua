-- Copyright Epic Games, Inc. All Rights Reserved.

target('zencore')
    set_kind("static")
    set_group("libs")
    add_headerfiles("**.h")
    add_configfiles("include/zencore/config.h.in")
    on_load(function (target)
        local version = io.readfile("VERSION.txt")
        version = string.gsub(version,"%-pre.*", "")
        target:set("version", version:trim(), {build = "%Y%m%d%H%M"})
    end)
    set_configdir("include/zencore")
    add_files("**.cpp")
    add_includedirs("include", {public=true})
    add_includedirs("$(projectdir)/thirdparty/utfcpp/source")
    add_includedirs("$(projectdir)/thirdparty/Oodle/include")
    add_includedirs("$(projectdir)/thirdparty/trace", {public=true})
    if is_os("windows") then
        add_linkdirs("$(projectdir)/thirdparty/Oodle/lib/Win64")
        add_links("oo2core_win64")
    elseif is_os("linux") then
        add_linkdirs("$(projectdir)/thirdparty/Oodle/lib/Linux_x64")
        add_links("oo2corelinux64")
        add_syslinks("pthread")
    elseif is_os("macosx") then
        add_linkdirs("$(projectdir)/thirdparty/Oodle/lib/Mac_x64")
        add_links("oo2coremac64")
    end
    add_options("zentrace")
    add_deps("zenbase")
    add_packages(
        "vcpkg::blake3", 
        "vcpkg::json11",
        "vcpkg::ryml",
        "vcpkg::c4core",
        "vcpkg::mimalloc",
        "vcpkg::openssl", -- required for crypto
        "vcpkg::spdlog")

    add_packages(
        "vcpkg::doctest", 
        "vcpkg::fmt",
        "vcpkg::gsl-lite", 
        "vcpkg::lz4",
        "vcpkg::xxhash",
        {public=true}
    )

    if is_plat("linux") then
        add_syslinks("rt")
    end

    if is_plat("windows") then
        add_syslinks("Advapi32")
        add_syslinks("Dbghelp")
        add_syslinks("Shell32")
        add_syslinks("User32")
        add_syslinks("crypt32")
        add_syslinks("bcrypt")
        add_syslinks("ws2_32")
    end
