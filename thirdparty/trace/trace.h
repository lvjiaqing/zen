// Copyright Epic Games, Inc. All Rights Reserved.
#pragma once
/* {{{1 standalone_prologue.h */

// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

// HEADER_UNIT_SKIP - Not included directly

#if !defined(TRACE_UE_COMPAT_LAYER)
#	if defined(__UNREAL__) && __UNREAL__
#		define TRACE_UE_COMPAT_LAYER	0
#	else
#		define TRACE_UE_COMPAT_LAYER	1
#	endif
#endif

#if TRACE_UE_COMPAT_LAYER

// platform defines
#define PLATFORM_ANDROID	0
#define PLATFORM_APPLE		0
#define PLATFORM_HOLOLENS	0
#define PLATFORM_MAC		0
#define PLATFORM_UNIX		0
#define PLATFORM_WINDOWS	0

#ifdef _WIN32
#	undef PLATFORM_WINDOWS
#	define PLATFORM_WINDOWS			1
#elif defined(__linux__)
#	undef  PLATFORM_UNIX
#	define PLATFORM_UNIX			1
#elif defined(__APPLE__)
#	undef  PLATFORM_MAC
#	define PLATFORM_MAC				1
#	undef  PLATFORM_APPLE
#	define PLATFORM_APPLE			1
#endif

// arch defines
#if defined(__amd64__) || defined(_M_X64)
#	define PLATFORM_CPU_X86_FAMILY	1
#	define PLATFORM_CPU_ARM_FAMILY	0
#	define PLATFORM_64BITS			1
#	define PLATFORM_CACHE_LINE_SIZE	64
#elif defined(__arm64__) || defined(_M_ARM64) || defined(_M_ARM64EC)
#	define PLATFORM_CPU_X86_FAMILY	0
#	define PLATFORM_CPU_ARM_FAMILY	1
#	define PLATFORM_64BITS			1
#	define PLATFORM_CACHE_LINE_SIZE	64
#else
#	error Unknown architecture
#endif

// external includes
#if defined(_MSC_VER)
#	define _ENABLE_EXTENDED_ALIGNED_STORAGE
#endif

#include <algorithm>
#include <cstddef>
#include <cstdint>
#include <cstring>
#include <string>
#include <string_view>
#include <utility>
#include <vector>

#if PLATFORM_WINDOWS
#	if !defined(WIN32_LEAN_AND_MEAN)
#		define WIN32_LEAN_AND_MEAN
#	endif
#	if !defined(NOGDI)
#		define NOGDI
#	endif
#	if !defined(NOMINMAX)
#		define NOMINMAX
#	endif
#	include <Windows.h>
#endif

// types
using uint8  = uint8_t;
using uint16 = uint16_t;
using uint32 = uint32_t;
using uint64 = uint64_t;

using int8	= int8_t;
using int16 = int16_t;
using int32 = int32_t;
using int64 = int64_t;

using UPTRINT = uintptr_t;
using PTRINT  = intptr_t;

using SIZE_T = size_t;

#if PLATFORM_WINDOWS
#	undef TEXT
#endif
#define TEXT(x)	x
#define TCHAR	ANSICHAR
using ANSICHAR	= char;
using WIDECHAR	= char16_t;

// keywords
#if defined(_MSC_VER)
#	define FORCENOINLINE	__declspec(noinline)
#	define FORCEINLINE		__forceinline
#else
#	define FORCENOINLINE	inline __attribute__((noinline))
#	define FORCEINLINE		inline __attribute__((always_inline))
#endif

#if defined(_MSC_VER)
#	define LIKELY(x)		x
#	define UNLIKELY(x)		x
#else
#	define LIKELY(x)		__builtin_expect(!!(x), 1)
#	define UNLIKELY(x)		__builtin_expect(!!(x), 0)
#endif

#define UE_ARRAY_COUNT(x)	(sizeof(x) / sizeof(x[0]))

// so/dll
#if defined(TRACE_DLL_EXPORT)
#	if PLATFORM_WINDOWS && defined(TRACE_DLL_EXPORT)
#		if TRACE_IMPLEMENT
#			define TRACELOG_API __declspec(dllexport)
#		else
#			define TRACELOG_API __declspec(dllimport)
#		endif
#	else
#		define TRACELOG_API		__attribute__ ((visibility ("default")))
#	endif
#else
#	define TRACELOG_API
#endif
#define TRACEANALYSIS_API		TRACELOG_API

#if !defined(IS_MONOLITHIC)
#	define IS_MONOLITHIC				0
#endif

// misc defines
#define TSAN_SAFE
#define THIRD_PARTY_INCLUDES_END
#define THIRD_PARTY_INCLUDES_START
#define TRACE_ENABLED					1
#define TRACE_PRIVATE_CONTROL_ENABLED	0
#define TRACE_PRIVATE_EXTERNAL_LZ4		1
#define UE_LAUNDER(x)					(x)
#define UE_DEPRECATED(x, ...)
#define UE_TRACE_ENABLED				TRACE_ENABLED
#define PREPROCESSOR_JOIN(a,b)			PREPROCESSOR_JOIN_IMPL(a,b)
#define PREPROCESSOR_JOIN_IMPL(a,b)		a##b
#define DEFINE_LOG_CATEGORY_STATIC(...)
#define UE_LOG(...)
#define check(x)						do { if (!(x)) __debugbreak(); } while (0)
#define checkf(x, ...)					check(x)

#if !defined(UE_BUILD_SHIPPING)
#	define UE_BUILD_SHIPPING			1
#endif
#define UE_BUILD_TEST					0

#if defined(__has_builtin)
#	if __has_builtin(__builtin_trap)
#		define PLATFORM_BREAK()			do { __builtin_trap(); } while (0)
#	endif
#elif defined(_MSC_VER)
#	define PLATFORM_BREAK()				do { __debugbreak(); } while (0)
#endif

#if !defined(PLATFORM_BREAK)
#	define PLATFORM_BREAK()				do { *(int*)0x493 = 0x493; } while (0)
#endif

// api
template <typename T> inline auto Forward(T t)		{ return std::forward<T>(t); }
template <typename T> inline auto MoveTemp(T&& t)	{ return std::move<T>(t); }
template <typename T> inline void Swap(T& x, T& y)	{ return std::swap(x, y); }



template <typename T, typename ALLOCATOR=void>
struct TArray
	: public std::vector<T>
{
	using	 Super = std::vector<T>;
	using	 Super::vector;
	using	 Super::back;
	using	 Super::begin;
	using	 Super::clear;
	using	 Super::data;
	using	 Super::emplace_back;
	using	 Super::empty;
	using	 Super::end;
	using	 Super::erase;
	using	 Super::front;
	using	 Super::insert;
	using	 Super::pop_back;
	using	 Super::push_back;
	using	 Super::reserve;
	using	 Super::resize;
	using	 Super::size;
	void	 Add(const T& t)					{ push_back(t); }
	T&		 Add_GetRef(const T& t)				{ push_back(t); return back(); }
	size_t	 AddUninitialized(size_t n)			{ resize(size() + n); return size() - n; }
	void	 Emplace()							{ emplace_back(); }
	T&		 Emplace_GetRef()					{ emplace_back(); return back(); }
	void	 Insert(T const& t, size_t i)		{ insert(begin() + i, t); }
	void	 Push(const T& t)					{ push_back(t); }
	T		 Pop(bool Eh=true)					{ T t = back(); pop_back(); return t; }
	size_t	 Num() const						{ return size(); }
	void	 SetNum(size_t num)					{ resize(num); }
	void	 SetNumZeroed(size_t num)			{ resize(num, 0); }
	void	 SetNumUninitialized(size_t num)	{ resize(num); }
	void	 Reserve(size_t num)				{ reserve(num); }
	T&		 Last()								{ return back(); }
	T*		 GetData()							{ return data(); }
	T const* GetData() const					{ return data(); }
	bool	 IsEmpty() const					{ return empty(); }
	void	 Empty()							{ clear(); }
	void	 Reset()							{ clear(); }
	void	 RemoveAll(bool (*p)(T const&))		{ erase(remove_if(begin(), end(), p), end()); }
	void	 RemoveAllSwap(bool (*p)(T const&))	{ RemoveAll(p); }

	struct NotLess {
		bool operator () (T const& Lhs, T const& Rhs) const {
			return bool(Lhs < Rhs) == false;
		}
	};
	void	 Heapify()				{ std::make_heap(begin(), end(), NotLess()); }
	void	 HeapPush(T const& t)	{ Add(t); std::push_heap(begin(), end(), NotLess()); }
	void	 HeapPopDiscard()		{ std::pop_heap(begin(), end(), NotLess()); pop_back(); }
	T const& HeapTop() const		{ return front(); }
};

template <size_t S> struct TInlineAllocator {};

template <typename T>
struct TArrayView
{
				TArrayView() = default;
				TArrayView(T* data, size_t size) : _data(data), _size(size) {}
	T*			GetData()		{ return _data; }
	T const*	GetData() const	{ return _data; }
	size_t		Num() const		{ return _size; }
	T*			_data = nullptr;
	size_t		_size = 0;
	using		value_type = T;
};


template <typename T>
struct TStringViewAdapter
	: public std::basic_string_view<T>
{
	using		Super = std::basic_string_view<T>;
	using		Super::basic_string_view;
	using		Super::size;
	using		Super::data;
	size_t		Len() const		{ return size(); }
	const char*	GetData() const	{ return data(); }
};

struct FStringProxy
{
							FStringProxy() = default;
	template <typename T>	FStringProxy(size_t len, const T* ptr);
	template <typename T>	FStringProxy(const T& r) : FStringProxy(r.size(), r.data()) {}
	std::string				_str;
	const void*				_ptr;
	uint32_t				_len;
	uint32_t				_char_size;
	std::string_view		ToView();
};

template <typename T>
inline FStringProxy::FStringProxy(size_t len, const T* ptr)
: _ptr(ptr)
, _len(uint32_t(len))
, _char_size(sizeof(T))
{
}

inline std::string_view FStringProxy::ToView()
{
	if (_char_size != 1)
	{
		_char_size = 1;
		_str.reserve(_len);
		char* __restrict cursor = _str.data();
		for (uint32_t i = 0, n = _len; i < n; ++i)
			cursor[i] = char(((const char16_t*)_ptr)[i]);
		_ptr = cursor;
	}

	return std::string_view((const char*)_ptr, _len);
}

using FAnsiStringView	= TStringViewAdapter<char>;
using FWideStringView	= TStringViewAdapter<char16_t>;
using FString			= struct FStringProxy;
using ANSITEXTVIEW		= FAnsiStringView;



namespace UECompat
{

template <typename T>
inline T		Max(T const& a, T const& b)				{ return std::max(a, b); }
inline size_t	Strlen(const char* str)					{ return ::strlen(str); }
inline void*	Malloc(size_t size)						{ return ::malloc(size); }
inline void*	Realloc(void* ptr, size_t size)			{ return ::realloc(ptr, size); }
inline void		Free(void* ptr)							{ return ::free(ptr); }
inline void		Memcpy(void* d, const void* s, size_t n){ ::memcpy(d, s, n); }

template <typename T, typename COMPARE=std::less<typename T::value_type>>
inline void StableSort(T& container, COMPARE&& compare=COMPARE())
{
	std::stable_sort(container.GetData(), container.GetData() + container.Num(),
		std::forward<COMPARE>(compare));
}

template <typename T, typename COMPARE=std::less<typename T::value_type>>
inline void Sort(T& container, COMPARE&& compare=COMPARE())
{
	std::sort(container.GetData(), container.GetData() + container.Num(),
		std::forward<COMPARE>(compare));
}

template <typename T, typename P>
inline void SortBy(T& container, P const& p)
{
	Sort(container, [&p] (typename T::value_type& lhs, typename T::value_type& rhs) {
		return p(lhs) < p(rhs);
	});
}

template <typename T, typename V, typename P>
inline int LowerBoundBy(T const& container, V const& v, P const& p)
{
	auto adapter = [&p] (typename T::value_type const& lhs, V const& v) {
		return p(lhs) < v;
	};
	auto* first = container.GetData();
	auto* last = first + container.Num();
	auto iter = std::lower_bound(first, last, v, adapter);
	return int(ptrdiff_t(iter - first));
}

} // namespace UeCompat

namespace Algo			= UECompat;
namespace FMath			= UECompat;
namespace FCStringAnsi	= UECompat;
namespace FMemory		= UECompat;

#endif // TRACE_UE_COMPAT_LAYER

#include <cstring>
#include "lz4.h"

#if PLATFORM_WINDOWS
#	pragma warning(push)
#	pragma warning(disable : 4200) // zero-sized arrays
#	pragma warning(disable : 4201) // anonymous structs
#	pragma warning(disable : 4127) // conditional expr. is constant
#endif
/* {{{1 Config.h */

#if !defined(UE_TRACE_ENABLED)
#	if !UE_BUILD_SHIPPING && !IS_PROGRAM
#		if PLATFORM_WINDOWS || PLATFORM_UNIX || PLATFORM_APPLE || PLATFORM_ANDROID
#			define UE_TRACE_ENABLED	1
#		endif
#	endif
#endif
#if !defined(UE_TRACE_ENABLED)
#	define UE_TRACE_ENABLED 0
#endif
#if UE_TRACE_ENABLED
#	define TRACE_PRIVATE_PROTOCOL_6
#endif
/* {{{1 Trace.h */

#if UE_TRACE_ENABLED
#include <type_traits>
namespace UE {
namespace Trace {
class FChannel;
} // namespace Trace
} // namespace UE
#define TRACE_PRIVATE_STATISTICS (!UE_BUILD_SHIPPING)
#define TRACE_PRIVATE_CHANNEL_DEFAULT_ARGS false, "None"
#define TRACE_PRIVATE_CHANNEL_DECLARE(LinkageType, ChannelName) \
	static UE::Trace::FChannel ChannelName##Object; \
	LinkageType UE::Trace::FChannel& ChannelName = ChannelName##Object;
#define TRACE_PRIVATE_CHANNEL_IMPL(ChannelName, ...) \
	struct F##ChannelName##Registrator \
	{ \
		F##ChannelName##Registrator() \
		{ \
			ChannelName##Object.Setup(#ChannelName, { __VA_ARGS__ } ); \
		} \
	}; \
	static F##ChannelName##Registrator ChannelName##Reg = F##ChannelName##Registrator();
#define TRACE_PRIVATE_CHANNEL(ChannelName, ...) \
	TRACE_PRIVATE_CHANNEL_DECLARE(static, ChannelName) \
	TRACE_PRIVATE_CHANNEL_IMPL(ChannelName, ##__VA_ARGS__)
#define TRACE_PRIVATE_CHANNEL_DEFINE(ChannelName, ...) \
	TRACE_PRIVATE_CHANNEL_DECLARE(, ChannelName) \
	TRACE_PRIVATE_CHANNEL_IMPL(ChannelName, ##__VA_ARGS__)
#define TRACE_PRIVATE_CHANNEL_EXTERN(ChannelName, ...) \
	__VA_ARGS__ extern UE::Trace::FChannel& ChannelName;
#define TRACE_PRIVATE_CHANNELEXPR_IS_ENABLED(ChannelsExpr) \
	bool(ChannelsExpr)
#define TRACE_PRIVATE_EVENT_DEFINE(LoggerName, EventName) \
	static UE::Trace::Private::FEventNode LoggerName##EventName##Event##Impl;\
	UE::Trace::Private::FEventNode& LoggerName##EventName##Event = LoggerName##EventName##Event##Impl;
#define TRACE_PRIVATE_EVENT_BEGIN(LoggerName, EventName, ...) \
	TRACE_PRIVATE_EVENT_DEFINE(LoggerName, EventName); \
	TRACE_PRIVATE_EVENT_BEGIN_IMPL(LoggerName, EventName, ##__VA_ARGS__)
#define TRACE_PRIVATE_EVENT_BEGIN_EXTERN(LoggerName, EventName, ...) \
	extern UE::Trace::Private::FEventNode& LoggerName##EventName##Event; \
	TRACE_PRIVATE_EVENT_BEGIN_IMPL(LoggerName, EventName, ##__VA_ARGS__)
#define TRACE_PRIVATE_EVENT_BEGIN_IMPL(LoggerName, EventName, ...) \
	struct F##LoggerName##EventName##Fields \
	{ \
		enum \
		{ \
			Important			= UE::Trace::Private::FEventInfo::Flag_Important, \
			NoSync				= UE::Trace::Private::FEventInfo::Flag_NoSync, \
			Definition8bit		= UE::Trace::Private::FEventInfo::Flag_Definition8, \
			Definition16bit		= UE::Trace::Private::FEventInfo::Flag_Definition16, \
			Definition32bit		= UE::Trace::Private::FEventInfo::Flag_Definition32, \
			Definition64bit		= UE::Trace::Private::FEventInfo::Flag_Definition64, \
			DefinitionBits		= UE::Trace::Private::FEventInfo::DefinitionBits, \
			PartialEventFlags	= (0, ##__VA_ARGS__), \
		}; \
		enum : bool { bIsImportant = ((0, ##__VA_ARGS__) & Important) != 0, bIsDefinition = ((0, ##__VA_ARGS__) & DefinitionBits) != 0,\
		bIsDefinition8 = ((0, ##__VA_ARGS__) & Definition8bit) != 0, \
		bIsDefinition16 = ((0, ##__VA_ARGS__) & Definition16bit) != 0,\
		bIsDefinition32 = ((0, ##__VA_ARGS__) & Definition32bit) != 0, \
		bIsDefinition64 = ((0, ##__VA_ARGS__) & Definition64bit) != 0,}; \
		typedef std::conditional_t<bIsDefinition8, UE::Trace::FEventRef8, std::conditional_t<bIsDefinition16, UE::Trace::FEventRef16 , std::conditional_t<bIsDefinition64, UE::Trace::FEventRef64, UE::Trace::FEventRef32>>> DefinitionType;\
		static constexpr uint32 GetSize() { return EventProps_Meta::Size; } \
		static uint32 TSAN_SAFE GetUid() { static uint32 Uid = 0; return (Uid = Uid ? Uid : Initialize()); } \
		static uint32 FORCENOINLINE Initialize() \
		{ \
			static const uint32 Uid_ThreadSafeInit = [] () \
			{ \
				using namespace UE::Trace; \
				static F##LoggerName##EventName##Fields Fields; \
				static UE::Trace::Private::FEventInfo Info = \
				{ \
					FLiteralName(#LoggerName), \
					FLiteralName(#EventName), \
					(FFieldDesc*)(&Fields), \
					EventProps_Meta::NumFields, \
					uint16(EventFlags), \
				}; \
				return LoggerName##EventName##Event.Initialize(&Info); \
			}(); \
			return Uid_ThreadSafeInit; \
		} \
		typedef UE::Trace::TField<0 /*Index*/, 0 /*Offset*/,
#define TRACE_PRIVATE_EVENT_FIELD(FieldType, FieldName) \
		FieldType> FieldName##_Meta; \
		FieldName##_Meta const FieldName##_Field = UE::Trace::FLiteralName(#FieldName); \
		template <typename... Ts> auto FieldName(Ts... ts) const { \
			LogScopeType::FFieldSet<FieldName##_Meta, FieldType>::Impl((LogScopeType*)this, Forward<Ts>(ts)...); \
			return true; \
		} \
		typedef UE::Trace::TField< \
			FieldName##_Meta::Index + 1, \
			FieldName##_Meta::Offset + FieldName##_Meta::Size,
#define TRACE_PRIVATE_EVENT_REFFIELD(RefLogger, RefEventType, FieldName) \
		F##RefLogger##RefEventType##Fields::DefinitionType> FieldName##_Meta; \
		FieldName##_Meta const FieldName##_Field = FieldName##_Meta(UE::Trace::FLiteralName(#FieldName), RefLogger##RefEventType##Event.GetUid()); \
		template <typename DefinitionType> auto FieldName(UE::Trace::TEventRef<DefinitionType> Reference) const { \
			checkfSlow(Reference.RefTypeId == F##RefLogger##RefEventType##Fields::GetUid(), TEXT("Incorrect reference type passed to event. Field expected %s with uid %u but got a reference with uid %u"), TEXT(#RefEventType), F##RefLogger##RefEventType##Fields::GetUid(), Reference.RefTypeId);\
			LogScopeType::FFieldSet<FieldName##_Meta, F##RefLogger##RefEventType##Fields::DefinitionType>::Impl((LogScopeType*)this, Reference); \
			return true; \
		} \
		typedef UE::Trace::TField< \
			FieldName##_Meta::Index + 1, \
			FieldName##_Meta::Offset + FieldName##_Meta::Size,
#define TRACE_PRIVATE_EVENT_END() \
		std::conditional<bIsDefinition != 0, DefinitionType, UE::Trace::DisabledField>::type> DefinitionId_Meta;\
		DefinitionId_Meta const DefinitionId_Field = UE::Trace::FLiteralName("");\
		static constexpr uint16 NumDefinitionFields = (bIsDefinition != 0) ? 1 : 0;\
		template<typename RefType>\
		auto SetDefinitionId(const RefType& Id) const \
		{ \
			LogScopeType::FFieldSet<DefinitionId_Meta, RefType>::Impl((LogScopeType*)this, Id); \
			return true; \
		} \
		typedef UE::Trace::TField<DefinitionId_Meta::Index + NumDefinitionFields, DefinitionId_Meta::Offset + DefinitionId_Meta::Size, UE::Trace::EventProps> EventProps_Meta; \
		EventProps_Meta const EventProps_Private = {}; \
		typedef std::conditional<bIsImportant != 0, UE::Trace::Private::FImportantLogScope, UE::Trace::Private::FLogScope>::type LogScopeType; \
		explicit operator bool () const { return true; } \
		enum { EventFlags = PartialEventFlags|((EventProps_Meta::NumAuxFields != 0) ? UE::Trace::Private::FEventInfo::Flag_MaybeHasAux : 0), }; \
		static_assert( \
			(bIsImportant == 0) || (uint32(EventFlags) & uint32(UE::Trace::Private::FEventInfo::Flag_NoSync)), \
			"Trace events flagged as Important events must be marked NoSync" \
		); \
	};
#define TRACE_PRIVATE_LOG_PRELUDE(EnterFunc, LoggerName, EventName, ChannelsExpr, ...) \
	if (TRACE_PRIVATE_CHANNELEXPR_IS_ENABLED(ChannelsExpr)) \
		if (auto LogScope = F##LoggerName##EventName##Fields::LogScopeType::EnterFunc<F##LoggerName##EventName##Fields>(__VA_ARGS__)) \
			if (const auto& __restrict EventName = *UE_LAUNDER((F##LoggerName##EventName##Fields*)(&LogScope))) \
				((void)EventName),
#define TRACE_PRIVATE_LOG_EPILOG() \
				LogScope += LogScope
#define TRACE_PRIVATE_LOG(LoggerName, EventName, ChannelsExpr, ...) \
	TRACE_PRIVATE_LOG_PRELUDE(Enter, LoggerName, EventName, ChannelsExpr, ##__VA_ARGS__) \
		TRACE_PRIVATE_LOG_EPILOG()
#define TRACE_PRIVATE_LOG_SCOPED(LoggerName, EventName, ChannelsExpr, ...) \
	UE::Trace::Private::FScopedLogScope PREPROCESSOR_JOIN(TheScope, __LINE__); \
	TRACE_PRIVATE_LOG_PRELUDE(ScopedEnter, LoggerName, EventName, ChannelsExpr, ##__VA_ARGS__) \
		PREPROCESSOR_JOIN(TheScope, __LINE__).SetActive(), \
		TRACE_PRIVATE_LOG_EPILOG()
#define TRACE_PRIVATE_LOG_SCOPED_T(LoggerName, EventName, ChannelsExpr, ...) \
	UE::Trace::Private::FScopedStampedLogScope PREPROCESSOR_JOIN(TheScope, __LINE__); \
	TRACE_PRIVATE_LOG_PRELUDE(ScopedStampedEnter, LoggerName, EventName, ChannelsExpr, ##__VA_ARGS__) \
		PREPROCESSOR_JOIN(TheScope, __LINE__).SetActive(), \
		TRACE_PRIVATE_LOG_EPILOG()
#define TRACE_PRIVATE_GET_DEFINITION_TYPE_ID(LoggerName, EventName) \
	F##LoggerName##EventName##Fields::GetUid()
#define TRACE_PRIVATE_LOG_DEFINITION(LoggerName, EventName, Id, ChannelsExpr, ...) \
	UE::Trace::MakeEventRef(Id, TRACE_PRIVATE_GET_DEFINITION_TYPE_ID(LoggerName, EventName)); \
	TRACE_PRIVATE_LOG(LoggerName, EventName, ChannelsExpr, ##__VA_ARGS__) \
		<< EventName.SetDefinitionId(UE::Trace::MakeEventRef(Id, F##LoggerName##EventName##Fields::GetUid()))
#else
#define TRACE_PRIVATE_CHANNEL(ChannelName, ...)
#define TRACE_PRIVATE_CHANNEL_EXTERN(ChannelName, ...)
#define TRACE_PRIVATE_CHANNEL_DEFINE(ChannelName, ...)
#define TRACE_PRIVATE_CHANNELEXPR_IS_ENABLED(ChannelsExpr) \
	false
#define TRACE_PRIVATE_EVENT_DEFINE(LoggerName, EventName) \
	int8* LoggerName##EventName##DummyPtr = nullptr;
#define TRACE_PRIVATE_EVENT_BEGIN(LoggerName, EventName, ...) \
	TRACE_PRIVATE_EVENT_BEGIN_IMPL(LoggerName, EventName)
#define TRACE_PRIVATE_EVENT_BEGIN_EXTERN(LoggerName, EventName, ...) \
	extern int8* LoggerName##EventName##DummyPtr; \
	TRACE_PRIVATE_EVENT_BEGIN_IMPL(LoggerName, EventName)
#define TRACE_PRIVATE_EVENT_BEGIN_IMPL(LoggerName, EventName) \
	struct F##LoggerName##EventName##Dummy \
	{ \
		struct FTraceDisabled \
		{ \
			const FTraceDisabled& operator () (...) const { return *this; } \
		}; \
		const F##LoggerName##EventName##Dummy& operator << (const FTraceDisabled&) const \
		{ \
			return *this; \
		} \
		explicit operator bool () const { return false; }
#define TRACE_PRIVATE_EVENT_FIELD(FieldType, FieldName) \
		const FTraceDisabled& FieldName;
#define TRACE_PRIVATE_EVENT_REFFIELD(RefLogger, RefEventType, FieldName) \
		const FTraceDisabled& FieldName;
#define TRACE_PRIVATE_EVENT_END() \
	};
#define TRACE_PRIVATE_LOG(LoggerName, EventName, ...) \
	if (const auto& EventName = *(F##LoggerName##EventName##Dummy*)1) \
		EventName
#define TRACE_PRIVATE_LOG_SCOPED(LoggerName, EventName, ...) \
	if (const auto& EventName = *(F##LoggerName##EventName##Dummy*)1) \
		EventName
#define TRACE_PRIVATE_LOG_SCOPED_T(LoggerName, EventName, ...) \
	if (const auto& EventName = *(F##LoggerName##EventName##Dummy*)1) \
		EventName
#define TRACE_PRIVATE_GET_DEFINITION_TYPE_ID(LoggerName, EventName) \
	0
#define TRACE_PRIVATE_LOG_DEFINITION(LoggerName, EventName, Id, ChannelsExpr, ...) \
	UE::Trace::MakeEventRef(Id, 0); \
	TRACE_PRIVATE_LOG(LoggerName, EventName, ChannelsExpr, ##__VA_ARGS__)
#endif // UE_TRACE_ENABLED
/* {{{1 Trace.h */

#if UE_TRACE_ENABLED
#	define UE_TRACE_IMPL(...)
#	define UE_TRACE_API			TRACELOG_API
#else
#	define UE_TRACE_IMPL(...)	{ return __VA_ARGS__; }
#	define UE_TRACE_API			inline
#endif
namespace UE {
namespace Trace {
enum AnsiString {};
enum WideString {};
template<typename IdType>	
struct TEventRef 
{
	using ReferenceType = IdType;
	TEventRef(IdType InId, uint32 InTypeId)
		: Id(InId)
		, RefTypeId(InTypeId)
	{
	}
	IdType Id;
	uint32 RefTypeId;
	uint64 GetHash() const;
private:
	TEventRef() = delete;
};
template <>
inline uint64 TEventRef<uint8>::GetHash() const
{
	return (uint64(RefTypeId) << 32) | Id;
}
template <>
inline uint64 TEventRef<uint16>::GetHash() const
{
	return (uint64(RefTypeId) << 32) | Id;
}
template <>
inline uint64 TEventRef<uint32>::GetHash() const
{
	return (uint64(RefTypeId) << 32) | Id;
}
template <>
inline uint64 TEventRef<uint64>::GetHash() const
{
	return (uint64(RefTypeId) << 32) ^ Id;
}
typedef TEventRef<uint8> FEventRef8;
typedef TEventRef<uint16> FEventRef16;	
typedef TEventRef<uint32> FEventRef32;
typedef TEventRef<uint64> FEventRef64;
template<typename IdType>
TEventRef<IdType> MakeEventRef(IdType InId, uint32 InTypeId)
{
	return TEventRef<IdType>(InId, InTypeId);
}
using OnConnectFunc = void(void);
struct FInitializeDesc
{
	uint32			TailSizeBytes		= 4 << 20; // can be set to 0 to disable the tail buffer
	uint32			ThreadSleepTimeInMS = 0;
	bool			bUseWorkerThread	= true;
	bool			bUseImportantCache	= true;
	OnConnectFunc*	OnConnectionFunc	= nullptr;
};
struct FChannelInfo;
typedef void*		AllocFunc(SIZE_T, uint32);
typedef void		FreeFunc(void*, SIZE_T);
typedef void		ChannelIterFunc(const ANSICHAR*, bool, void*);
/*  The callback provides information about a channel and a user provided pointer. 
	Returning false from the callback will stop the enumeration */
typedef bool		ChannelIterCallback(const FChannelInfo& Info, void*/*User*/);
struct FStatistics
{
	uint64		BytesSent;
	uint64		BytesTraced;
	uint64		MemoryUsed;
	uint32		CacheAllocated;	// Total memory allocated in cache buffers
	uint32		CacheUsed;		// Used cache memory; Important-marked events are stored in the cache.
	uint32		CacheWaste;		// Unused memory from retired cache buffers
};
struct FSendFlags
{
	static const uint16 None		= 0;
	static const uint16 ExcludeTail	= 1 << 0;	// do not send the tail of historical events
	static const uint16 _Reserved	= 1 << 15;	// this bit is used internally
};
UE_TRACE_API void	SetMemoryHooks(AllocFunc Alloc, FreeFunc Free) UE_TRACE_IMPL();
UE_TRACE_API void	Initialize(const FInitializeDesc& Desc) UE_TRACE_IMPL();
UE_TRACE_API void	StartWorkerThread() UE_TRACE_IMPL();	
UE_TRACE_API void	Shutdown() UE_TRACE_IMPL();
UE_TRACE_API void	Update() UE_TRACE_IMPL();
UE_TRACE_API void	GetStatistics(FStatistics& Out) UE_TRACE_IMPL();
UE_TRACE_API bool	SendTo(const TCHAR* Host, uint32 Port=0, uint16 Flags=FSendFlags::None) UE_TRACE_IMPL(false);
UE_TRACE_API bool	WriteTo(const TCHAR* Path, uint16 Flags=FSendFlags::None) UE_TRACE_IMPL(false);
UE_TRACE_API bool	WriteSnapshotTo(const TCHAR* Path) UE_TRACE_IMPL(false);
UE_TRACE_API bool	IsTracing() UE_TRACE_IMPL(false);
UE_TRACE_API bool	Stop() UE_TRACE_IMPL(false);
UE_TRACE_API bool	IsChannel(const TCHAR* ChanneName) UE_TRACE_IMPL(false);
UE_TRACE_API bool	ToggleChannel(const TCHAR* ChannelName, bool bEnabled) UE_TRACE_IMPL(false);
UE_TRACE_API void	EnumerateChannels(ChannelIterFunc IterFunc, void* User) UE_TRACE_IMPL();
UE_TRACE_API void	EnumerateChannels(ChannelIterCallback IterFunc, void* User) UE_TRACE_IMPL();
UE_TRACE_API void	ThreadRegister(const TCHAR* Name, uint32 SystemId, int32 SortHint) UE_TRACE_IMPL();
UE_TRACE_API void	ThreadGroupBegin(const TCHAR* Name) UE_TRACE_IMPL();
UE_TRACE_API void	ThreadGroupEnd() UE_TRACE_IMPL();
} // namespace Trace
} // namespace UE
#define UE_TRACE_EVENT_DEFINE(LoggerName, EventName)					TRACE_PRIVATE_EVENT_DEFINE(LoggerName, EventName)
#define UE_TRACE_EVENT_BEGIN(LoggerName, EventName, ...)				TRACE_PRIVATE_EVENT_BEGIN(LoggerName, EventName, ##__VA_ARGS__)
#define UE_TRACE_EVENT_BEGIN_EXTERN(LoggerName, EventName, ...)			TRACE_PRIVATE_EVENT_BEGIN_EXTERN(LoggerName, EventName, ##__VA_ARGS__)
#define UE_TRACE_EVENT_FIELD(FieldType, FieldName)						TRACE_PRIVATE_EVENT_FIELD(FieldType, FieldName)
#define UE_TRACE_EVENT_REFERENCE_FIELD(RefLogger, RefEvent, FieldName)	TRACE_PRIVATE_EVENT_REFFIELD(RefLogger, RefEvent, FieldName)
#define UE_TRACE_EVENT_END()											TRACE_PRIVATE_EVENT_END()
#define UE_TRACE_LOG(LoggerName, EventName, ChannelsExpr, ...)			TRACE_PRIVATE_LOG(LoggerName, EventName, ChannelsExpr, ##__VA_ARGS__)
#define UE_TRACE_LOG_SCOPED(LoggerName, EventName, ChannelsExpr, ...)	TRACE_PRIVATE_LOG_SCOPED(LoggerName, EventName, ChannelsExpr, ##__VA_ARGS__)
#define UE_TRACE_LOG_SCOPED_T(LoggerName, EventName, ChannelsExpr, ...)	TRACE_PRIVATE_LOG_SCOPED_T(LoggerName, EventName, ChannelsExpr, ##__VA_ARGS__)
#define UE_TRACE_GET_DEFINITION_TYPE_ID(LoggerName, EventName)			TRACE_PRIVATE_GET_DEFINITION_TYPE_ID(LoggerName, EventName)
#define UE_TRACE_LOG_DEFINITION(LoggerName, EventName, Id, ChannelsExpr, ...) TRACE_PRIVATE_LOG_DEFINITION(LoggerName, EventName, Id, ChannelsExpr, ##__VA_ARGS__)
#define UE_TRACE_CHANNEL(ChannelName, ...)				TRACE_PRIVATE_CHANNEL(ChannelName, ##__VA_ARGS__)
#define UE_TRACE_CHANNEL_EXTERN(ChannelName, ...)		TRACE_PRIVATE_CHANNEL_EXTERN(ChannelName, ##__VA_ARGS__)
#define UE_TRACE_CHANNEL_DEFINE(ChannelName, ...)		TRACE_PRIVATE_CHANNEL_DEFINE(ChannelName, ##__VA_ARGS__)
#define UE_TRACE_CHANNELEXPR_IS_ENABLED(ChannelsExpr)	TRACE_PRIVATE_CHANNELEXPR_IS_ENABLED(ChannelsExpr)
/* {{{1 Channel.h */

#if UE_TRACE_ENABLED
namespace UE {
namespace Trace {
struct FChannelInfo
{
	const ANSICHAR* Name;
	const ANSICHAR* Desc;
	bool bIsEnabled;
	bool bIsReadOnly;
};
typedef void ChannelIterFunc(const ANSICHAR*, bool, void*);	
typedef bool ChannelIterCallback(const FChannelInfo& OutChannelInfo, void*);
/*
	A named channel which can be used to filter trace events. Channels can be
	combined using the '|' operator which allows expressions like
	```
	UE_TRACE_LOG(FooWriter, FooEvent, FooChannel|BarChannel);
	```
	Note that this works as an AND operator, similar to how a bitmask is constructed.
	Channels are by default enabled until this method is called. This is to allow
	events to be emitted during static initialization. In fact all events during
	this phase are always emitted. In this method we disable all channels except
	those specified on the command line using -tracechannels argument.
*/
class FChannel
{
public:
	struct Iter
	{
						~Iter();
		const FChannel*	GetNext();
		void*			Inner[3];
	};
	struct InitArgs
	{
		const ANSICHAR* 	Desc;		// User facing description string
		bool				bReadOnly;	// If set, channel cannot be changed during a run, only set through command line.
	};
	TRACELOG_API void	Setup(const ANSICHAR* InChannelName, const InitArgs& Args);
	TRACELOG_API static void Initialize();
	static Iter			ReadNew();
	void				Announce() const;
	static bool			Toggle(const ANSICHAR* ChannelName, bool bEnabled);
	static void			ToggleAll(bool bEnabled);
	static FChannel*	FindChannel(const ANSICHAR* ChannelName);
	UE_DEPRECATED(5.2, "Please use the ChannelIterCallback overload for enumerating channels.")
	static void			EnumerateChannels(ChannelIterFunc Func, void* User);
	static void			EnumerateChannels(ChannelIterCallback Func, void* User);
	bool				Toggle(bool bEnabled);
	bool				IsEnabled() const;
	explicit			operator bool () const;
	bool				operator | (const FChannel& Rhs) const;
private:
	FChannel*			Next;
	struct
	{
		const ANSICHAR*	Ptr;
		uint32			Len;
		uint32			Hash;
	}					Name;
	volatile int32		Enabled;
	InitArgs			Args;
};
} // namespace Trace
} // namespace UE
#endif // UE_TRACE_ENABLED
/* {{{1 Channel.inl */

#if UE_TRACE_ENABLED
namespace UE {
namespace Trace {
extern TRACELOG_API FChannel& TraceLogChannel;
inline bool FChannel::IsEnabled() const
{
	return Enabled >= 0;
}
inline FChannel::operator bool () const
{
	return IsEnabled();
}
inline bool FChannel::operator | (const FChannel& Rhs) const
{
	return IsEnabled() && Rhs.IsEnabled();
}
} // namespace Trace
} // namespace UE
#endif // UE_TRACE_ENABLED
/* {{{1 Atomic.h */

#include <atomic>
#if PLATFORM_CPU_X86_FAMILY
#	include <immintrin.h>
#endif
namespace UE {
namespace Trace {
namespace Private {
template <typename Type> Type	AtomicLoadRelaxed(Type volatile* Source);
template <typename Type> Type	AtomicLoadAcquire(Type volatile* Source);
template <typename Type> void	AtomicStoreRelaxed(Type volatile* Target, Type Value);
template <typename Type> void	AtomicStoreRelease(Type volatile* Target, Type Value);
template <typename Type> Type	AtomicExchangeAcquire(Type volatile* Target, Type Value);
template <typename Type> Type	AtomicExchangeRelease(Type volatile* Target, Type Value);
template <typename Type> bool	AtomicCompareExchangeRelaxed(Type volatile* Target, Type New, Type Expected);
template <typename Type> bool	AtomicCompareExchangeAcquire(Type volatile* Target, Type New, Type Expected);
template <typename Type> bool	AtomicCompareExchangeRelease(Type volatile* Target, Type New, Type Expected);
template <typename Type> Type	AtomicAddRelaxed(Type volatile* Target, Type Value);
template <typename Type> Type	AtomicAddRelease(Type volatile* Target, Type Value);
template <typename Type> Type	AtomicAddAcquire(Type volatile* Target, Type Value);
void							PlatformYield();
inline void PlatformYield()
{
#if PLATFORM_CPU_X86_FAMILY
	_mm_pause();
#elif PLATFORM_CPU_ARM_FAMILY
#	if defined(_MSC_VER) && !defined(__clang__) // MSVC
		__yield();
#	else
		__builtin_arm_yield();
#	endif
#else
	#error Unsupported architecture!
#endif
}
template <typename Type>
inline Type AtomicLoadRelaxed(Type volatile* Source)
{
	std::atomic<Type>* T = (std::atomic<Type>*) Source;
	return T->load(std::memory_order_relaxed);
}
template <typename Type>
inline Type AtomicLoadAcquire(Type volatile* Source)
{
	std::atomic<Type>* T = (std::atomic<Type>*) Source;
	return T->load(std::memory_order_acquire);
}
template <typename Type>
inline void AtomicStoreRelaxed(Type volatile* Target, Type Value)
{
	std::atomic<Type>* T = (std::atomic<Type>*) Target;
	T->store(Value, std::memory_order_relaxed);
}
template <typename Type>
inline void AtomicStoreRelease(Type volatile* Target, Type Value)
{
	std::atomic<Type>* T = (std::atomic<Type>*) Target;
	T->store(Value, std::memory_order_release);
}
template <typename Type>
inline Type AtomicExchangeAcquire(Type volatile* Target, Type Value)
{
	std::atomic<Type>* T = (std::atomic<Type>*) Target;
	return T->exchange(Value, std::memory_order_acquire);
}
template <typename Type>
inline Type AtomicExchangeRelease(Type volatile* Target, Type Value)
{
	std::atomic<Type>* T = (std::atomic<Type>*) Target;
	return T->exchange(Value, std::memory_order_release);
}
template <typename Type>
inline bool AtomicCompareExchangeRelaxed(Type volatile* Target, Type New, Type Expected)
{
	std::atomic<Type>* T = (std::atomic<Type>*) Target;
	return T->compare_exchange_weak(Expected, New, std::memory_order_relaxed);
}
template <typename Type>
inline bool AtomicCompareExchangeAcquire(Type volatile* Target, Type New, Type Expected)
{
	std::atomic<Type>* T = (std::atomic<Type>*) Target;
	return T->compare_exchange_weak(Expected, New, std::memory_order_acquire);
}
template <typename Type>
inline bool AtomicCompareExchangeRelease(Type volatile* Target, Type New, Type Expected)
{
	std::atomic<Type>* T = (std::atomic<Type>*) Target;
	return T->compare_exchange_weak(Expected, New, std::memory_order_release);
}
template <typename Type>
inline Type AtomicAddRelaxed(Type volatile* Target, Type Value)
{
	std::atomic<Type>* T = (std::atomic<Type>*) Target;
	return T->fetch_add(Value, std::memory_order_relaxed);
}
template <typename Type>
inline Type AtomicAddAcquire(Type volatile* Target, Type Value)
{
	std::atomic<Type>* T = (std::atomic<Type>*) Target;
	return T->fetch_add(Value, std::memory_order_acquire);
}
template <typename Type>
inline Type AtomicAddRelease(Type volatile* Target, Type Value)
{
	std::atomic<Type>* T = (std::atomic<Type>*) Target;
	return T->fetch_add(Value, std::memory_order_release);
}
} // namespace Private
} // namespace Trace
} // namespace UE
/* {{{1 Protocol0.h */

namespace UE {
namespace Trace {
#if defined(TRACE_PRIVATE_PROTOCOL_0)
inline
#endif
namespace Protocol0
{
enum EProtocol : uint8 { Id = 0 };
enum : uint8
{
	/* Category */
	Field_CategoryMask	= 0300,
	Field_Integer		= 0000,
	Field_Float			= 0100,
	Field_Array			= 0200,
	/* Size */
	Field_Pow2SizeMask	= 0003,
	Field_8				= 0000,
	Field_16			= 0001,
	Field_32			= 0002,
	Field_64			= 0003,
#if PLATFORM_64BITS
	Field_Ptr			= Field_64,
#else
	Field_Ptr			= Field_32,
#endif
	/* Specials */
	Field_SpecialMask	= 0030,
	Field_Pod			= 0000,
	Field_String		= 0010,
	Field_Signed		= 0020,
	/*Field_Unused_3	= 0030,
	  ...
	  Field_Unused_7	= 0070,*/
};
enum class EFieldType : uint8
{
	Bool		= Field_Pod    | Field_Integer                 | Field_8,
	Int8		= Field_Pod    | Field_Integer|Field_Signed    | Field_8,
	Int16		= Field_Pod    | Field_Integer|Field_Signed    | Field_16,
	Int32		= Field_Pod    | Field_Integer|Field_Signed    | Field_32,
	Int64		= Field_Pod    | Field_Integer|Field_Signed    | Field_64,
	Uint8		= Field_Pod    | Field_Integer                 | Field_8,
	Uint16		= Field_Pod    | Field_Integer                 | Field_16,
	Uint32		= Field_Pod    | Field_Integer                 | Field_32,
	Uint64		= Field_Pod    | Field_Integer                 | Field_64,
	Pointer		= Field_Pod    | Field_Integer                 | Field_Ptr,
	Float32		= Field_Pod    | Field_Float                   | Field_32,
	Float64		= Field_Pod    | Field_Float                   | Field_64,
	AnsiString	= Field_String | Field_Integer|Field_Array     | Field_8,
	WideString	= Field_String | Field_Integer|Field_Array     | Field_16,
	Array		= Field_Array,
};
struct FNewEventEvent
{
	uint16		EventUid;
	uint8		FieldCount;
	uint8		Flags;
	uint8		LoggerNameSize;
	uint8		EventNameSize;
	struct
	{
		uint16	Offset;
		uint16	Size;
		uint8	TypeInfo;
		uint8	NameSize;
	}			Fields[];
	/*uint8		NameData[]*/
};
enum class EKnownEventUids : uint16
{
	NewEvent,
	User,
	Max				= (1 << 14) - 1, // ...leaves two MSB bits for other uses.
	UidMask			= Max,
	Invalid			= Max,
	Flag_Important	= 1 << 14,
	Flag_Unused		= 1 << 15,
};
struct FEventHeader
{
	uint16		Uid;
	uint16		Size;
	uint8		EventData[];
};
} // namespace Protocol0
} // namespace Trace
} // namespace UE
/* {{{1 Protocol1.h */

namespace UE {
namespace Trace {
#if defined(TRACE_PRIVATE_PROTOCOL_1)
inline
#endif
namespace Protocol1
{
enum EProtocol : uint8 { Id = 1 };
using Protocol0::EFieldType;
using Protocol0::FNewEventEvent;
enum class EEventFlags : uint8
{
	Important		= 1 << 0,
	MaybeHasAux		= 1 << 1,
	NoSync			= 1 << 2,
};
enum class EKnownEventUids : uint16
{
	NewEvent,
	User,
	Max			= (1 << 15) - 1,
	UidMask		= Max,
	Invalid		= Max,
};
struct FEventHeader
{
	uint16		Uid;
	uint16		Size;
	uint16		Serial;
	uint8		EventData[];
};
struct FAuxHeader
{
	enum : uint32
	{
		AuxDataBit	= 0x80,
		FieldMask	= 0x7f,
		SizeLimit	= 1 << 24,
	};
	union
	{
		uint8	FieldIndex;	// 7 bits max (MSB is used to indicate aux data)
		uint32	Size;		// encoded as (Size & 0x00ffffff) << 8
	};
	uint8		Data[];
};
} // namespace Protocol1
} // namespace Trace
} // namespace UE
/* {{{1 Protocol2.h */

namespace UE {
namespace Trace {
#if defined(TRACE_PRIVATE_PROTOCOL_2)
inline
#endif
namespace Protocol2
{
enum EProtocol : uint8 { Id = 2 };
using Protocol1::EFieldType;
using Protocol1::FNewEventEvent;
using Protocol1::EEventFlags;
using Protocol1::EKnownEventUids;
using Protocol1::FAuxHeader;
struct FEventHeader
{
	uint16		Uid;
	uint16		Size;
};
#pragma pack(push, 1)
struct FEventHeaderSync
	: public FEventHeader
{
	uint16		SerialLow;		// 24-bit...
	uint8		SerialHigh;		// ...serial no.
	uint8		EventData[];
};
#pragma pack(pop)
static_assert(sizeof(FEventHeaderSync) == 7, "Packing assumption doesn't hold");
} // namespace Protocol2
} // namespace Trace
} // namespace UE
/* {{{1 Protocol3.h */

namespace UE {
namespace Trace {
#if defined(TRACE_PRIVATE_PROTOCOL_3)
inline
#endif
namespace Protocol3
{
enum EProtocol : uint8 { Id = 3 };
using Protocol2::EFieldType;
using Protocol2::FNewEventEvent;
using Protocol2::EEventFlags;
using Protocol2::EKnownEventUids;
using Protocol2::FAuxHeader;
using Protocol2::FEventHeader;
using Protocol2::FEventHeaderSync;
} // namespace Protocol3
} // namespace Trace
} // namespace UE
/* {{{1 Protocol4.h */

namespace UE {
namespace Trace {
#if defined(TRACE_PRIVATE_PROTOCOL_4)
inline
#endif
namespace Protocol4
{
enum EProtocol : uint8 { Id = 4 };
using Protocol3::EFieldType;
using Protocol3::FNewEventEvent;
using Protocol3::EEventFlags;
using Protocol3::FAuxHeader;
using Protocol3::FEventHeader;
using Protocol3::FEventHeaderSync;
struct EKnownEventUids
{
	static const uint16 Flag_TwoByteUid	= 1 << 0;
	static const uint16 _UidShift		= 1;
	enum : uint16
	{
		NewEvent						= 0,
		EnterScope,
		EnterScope_T,
		LeaveScope,
		LeaveScope_T,
		_WellKnownNum,
	};
	static const uint16 User			= _WellKnownNum;
	static const uint16 Max				= (1 << (16 - _UidShift)) - 1;
	static const uint16 Invalid			= Max;
};
} // namespace Protocol4
} // namespace Trace
} // namespace UE
/* {{{1 Protocol5.h */

namespace UE {
namespace Trace {
#if defined(TRACE_PRIVATE_PROTOCOL_5)
inline
#endif
namespace Protocol5
{
enum EProtocol : uint8 { Id = 5 };
using Protocol4::EFieldType;
using Protocol4::FNewEventEvent;
using Protocol4::EEventFlags;
struct EKnownEventUids
{
	static const uint16 Flag_TwoByteUid	= 1 << 0;
	static const uint16 _UidShift		= 1;
	enum : uint16
	{
		NewEvent						= 0,
		AuxData,
		_AuxData_Unused,
		AuxDataTerminal,
		EnterScope,
		LeaveScope,
		_Unused6,
		_Unused7,
		EnterScope_T,
		_EnterScope_T_Unused0,	// reserved for variable
		_EnterScope_T_Unused1,	// length timestamps
		_EnterScope_T_Unused2,
		LeaveScope_T,
		_LeaveScope_T_Unused0,
		_LeaveScope_T_Unused1,
		_LeaveScope_T_Unused2,
		_WellKnownNum,
	};
	static const uint16 User			= _WellKnownNum;
	static const uint16 Max				= (1 << (16 - _UidShift)) - 1;
	static const uint16 Invalid			= Max;
};
struct FEventHeader
{
	uint16		Uid;
	uint8		Data[];
};
static_assert(sizeof(FEventHeader) == 2, "Struct layout assumption doesn't match expectation");
struct FImportantEventHeader
{
	uint16		Uid;
	uint16		Size;
	uint8		Data[];
};
static_assert(sizeof(FImportantEventHeader) == 4, "Struct layout assumption doesn't match expectation");
#pragma pack(push, 1)
struct FEventHeaderSync
{
	uint16		Uid;
	uint16		SerialLow;		// 24-bit
	uint8		SerialHigh;		// serial no.
	uint8		Data[];
};
#pragma pack(pop)
static_assert(sizeof(FEventHeaderSync) == 5, "Packing assumption doesn't hold");
struct FAuxHeader
{
	enum : uint32
	{
		FieldShift	= 8,
		FieldBits	= 5,
		FieldMask	= (1 << FieldBits) - 1,
		SizeShift	= FieldShift + FieldBits,
		SizeLimit	= 1 << (32 - SizeShift),
	};
	union
	{
		struct
		{
			uint8	Uid;
			uint8	FieldIndex_Size;
			uint16	Size;
		};
		uint32		Pack;
	};
	uint8		Data[];
};
static_assert(sizeof(FAuxHeader) == 4, "Struct layout assumption doesn't match expectation");
} // namespace Protocol5
} // namespace Trace
} // namespace UE
/* {{{1 Protocol6.h */

namespace UE {
namespace Trace {
#if defined(TRACE_PRIVATE_PROTOCOL_6)
inline
#endif
namespace Protocol6
{
enum EProtocol : uint8 { Id = 6 };
using Protocol5::EFieldType;
using Protocol5::FEventHeader;
using Protocol5::FImportantEventHeader;
using Protocol5::FEventHeaderSync;
using Protocol5::FAuxHeader;
using Protocol5::EKnownEventUids;
enum class EEventFlags : uint8
{
	Important		= 1 << 0,
	MaybeHasAux		= 1 << 1,
	NoSync			= 1 << 2,
	Definition		= 1 << 3,
};
enum EFieldFamily : uint8
{
	Regular,
	Reference,
	DefinitionId,
};
struct FNewEventEvent
{
	uint16		EventUid;
	uint8		FieldCount;
	uint8		Flags;
	uint8		LoggerNameSize;
	uint8		EventNameSize;
	struct
	{
		uint8	FieldType;
		union
		{
			struct
			{
				uint16	Offset;
				uint16	Size;
				uint8	TypeInfo;
				uint8	NameSize;
			} Regular;
			struct
			{
				uint16	Offset;
				uint16	RefUid; 
				uint8	TypeInfo;
				uint8	NameSize;
			} Reference;
			struct
			{
				uint16	Offset;
				uint16	Unused1;
				uint8	Unused2;
				uint8	TypeInfo;
			} DefinitionId;
		};
	} Fields[];
};
} // namespace Protocol6
} // namespace Trace
} // namespace UE
/* {{{1 Protocol.h */

#if defined(_MSC_VER)
	#pragma warning(push)
	#pragma warning(disable : 4200) // non-standard zero-sized array
#endif
#if defined(_MSC_VER)
	#pragma warning(pop)
#endif
/* {{{1 Writer.inl */

#if UE_TRACE_ENABLED
namespace UE {
namespace Trace {
namespace Private {
struct FWriteBuffer
{
	uint8						Overflow[8];
	uint64						PrevTimestamp;
	FWriteBuffer* __restrict	NextThread;
	FWriteBuffer* volatile		NextBuffer;
	uint8* __restrict			Cursor;
	uint8* __restrict volatile	Committed;
	uint8* __restrict			Reaped;
	int32 volatile				EtxOffset;
	uint16						Size;
	uint16						ThreadId;
};
TRACELOG_API uint64				TimeGetTimestamp();
TRACELOG_API FWriteBuffer*		Writer_NextBuffer();
TRACELOG_API FWriteBuffer*		Writer_GetBuffer();
#if IS_MONOLITHIC
extern thread_local FWriteBuffer* GTlsWriteBuffer;
inline FWriteBuffer* Writer_GetBuffer()
{
	return GTlsWriteBuffer;
}
#endif // IS_MONOLITHIC
inline uint64 Writer_GetTimestamp(FWriteBuffer* Buffer)
{
	uint64 Ret = TimeGetTimestamp() - Buffer->PrevTimestamp;
	Buffer->PrevTimestamp += Ret;
	return Ret;
}
} // namespace Private
} // namespace Trace
} // namespace UE
#endif // UE_TRACE_ENABLED
/* {{{1 Field.h */

#if UE_TRACE_ENABLED
/* Statically sized fields (e.g. UE_TRACE_EVENT_FIELD(float[4], Colours)) are
 * not supported as yet. No call for them. The following define is used to track
 * where and partially how to implement them */
#define STATICALLY_SIZED_ARRAY_FIELDS_SUPPORT 0
namespace UE {
namespace Trace {
namespace Private
{
UE_TRACE_API void Field_WriteAuxData(uint32, const uint8*, int32);
UE_TRACE_API void Field_WriteStringAnsi(uint32, const ANSICHAR*, int32);
UE_TRACE_API void Field_WriteStringAnsi(uint32, const WIDECHAR*, int32);
UE_TRACE_API void Field_WriteStringWide(uint32, const WIDECHAR*, int32);
class FEventNode;
} // namespace Private
enum DisabledField {};
template <typename Type> struct TFieldType;
template <> struct TFieldType<DisabledField>{ enum { Tid = 0,						Size = 0 }; };
template <> struct TFieldType<bool>			{ enum { Tid = int(EFieldType::Bool),	Size = sizeof(bool) }; };
template <> struct TFieldType<int8>			{ enum { Tid = int(EFieldType::Int8),	Size = sizeof(int8) }; };
template <> struct TFieldType<int16>		{ enum { Tid = int(EFieldType::Int16),	Size = sizeof(int16) }; };
template <> struct TFieldType<int32>		{ enum { Tid = int(EFieldType::Int32),	Size = sizeof(int32) }; };
template <> struct TFieldType<int64>		{ enum { Tid = int(EFieldType::Int64),	Size = sizeof(int64) }; };
template <> struct TFieldType<uint8>		{ enum { Tid = int(EFieldType::Uint8),	Size = sizeof(uint8) }; };
template <> struct TFieldType<uint16>		{ enum { Tid = int(EFieldType::Uint16),	Size = sizeof(uint16) }; };
template <> struct TFieldType<uint32>		{ enum { Tid = int(EFieldType::Uint32),	Size = sizeof(uint32) }; };
template <> struct TFieldType<uint64>		{ enum { Tid = int(EFieldType::Uint64),	Size = sizeof(uint64) }; };
template <> struct TFieldType<float>		{ enum { Tid = int(EFieldType::Float32),Size = sizeof(float) }; };
template <> struct TFieldType<double>		{ enum { Tid = int(EFieldType::Float64),Size = sizeof(double) }; };
template <class T> struct TFieldType<T*>	{ enum { Tid = int(EFieldType::Pointer),Size = sizeof(void*) }; };
template <typename T>
struct TFieldType<T[]>
{
	enum
	{
		Tid  = int(TFieldType<T>::Tid)|int(EFieldType::Array),
		Size = 0,
	};
};
#if STATICALLY_SIZED_ARRAY_FIELDS_SUPPORT
template <typename T, int N>
struct TFieldType<T[N]>
{
	enum
	{
		Tid  = int(TFieldType<T>::Tid)|int(EFieldType::Array),
		Size = sizeof(T[N]),
	};
};
#endif // STATICALLY_SIZED_ARRAY_FIELDS_SUPPORT
template <> struct TFieldType<AnsiString> { enum { Tid  = int(EFieldType::AnsiString), Size = 0, }; };
template <> struct TFieldType<WideString> { enum { Tid  = int(EFieldType::WideString), Size = 0, }; };
struct FLiteralName
{
	template <uint32 Size>
	explicit FLiteralName(const ANSICHAR (&Name)[Size])
	: Ptr(Name)
	, Length(Size - 1)
	{
		static_assert(Size < 256, "Field name is too large");
	}
	const ANSICHAR* Ptr;
	uint8 Length;
};
struct FFieldDesc
{
	FFieldDesc(const FLiteralName& Name, uint8 Type, uint16 Offset, uint16 Size, uint32 ReferencedUid = 0)
	: Name(Name.Ptr)
	, ValueOffset(Offset)
	, ValueSize(Size)
	, NameSize(Name.Length)
	, TypeInfo(Type)
	, Reference(ReferencedUid)
	{
	}
	const ANSICHAR* Name;
	uint16			ValueOffset;
	uint16			ValueSize;
	uint8			NameSize;
	uint8			TypeInfo;
	uint32			Reference;
};
template <int InIndex, int InOffset, typename Type> struct TField;
enum class EIndexPack
{
	NumFieldsMax	= 1 << FAuxHeader::FieldBits,
	NumFieldsShift	= 8,
	NumFieldsMask	= (1 << NumFieldsShift) - 1,
	AuxFieldCounter	= 1 << NumFieldsShift,
};
#define TRACE_PRIVATE_FIELD(InIndex, InOffset, Type) \
		enum \
		{ \
			Index	= InIndex, \
			Offset	= InOffset, \
			Tid		= TFieldType<Type>::Tid, \
			Size	= TFieldType<Type>::Size, \
		}; \
		static_assert((Index & int(EIndexPack::NumFieldsMask)) < int(EIndexPack::NumFieldsMax), "Trace events may only have up to EIndexPack::NumFieldsMax fields"); \
	private: \
		FFieldDesc FieldDesc; \
	public: \
		TField(const FLiteralName& Name) \
		: FieldDesc(Name, Tid, Offset, Size) \
		{ \
		}
template <int InIndex, int InOffset, typename Type>
struct TField<InIndex, InOffset, Type[]>
{
	TRACE_PRIVATE_FIELD(InIndex + int(EIndexPack::AuxFieldCounter), InOffset, Type[]);
};
#if STATICALLY_SIZED_ARRAY_FIELDS_SUPPORT
template <int InIndex, int InOffset, typename Type, int Count>
struct TField<InIndex, InOffset, Type[Count]>
{
	TRACE_PRIVATE_FIELD(InIndex, InOffset, Type[Count]);
};
#endif // STATICALLY_SIZED_ARRAY_FIELDS_SUPPORT
template <int InIndex, int InOffset>
struct TField<InIndex, InOffset, AnsiString>
{
	TRACE_PRIVATE_FIELD(InIndex + int(EIndexPack::AuxFieldCounter), InOffset, AnsiString);
};
template <int InIndex, int InOffset>
struct TField<InIndex, InOffset, WideString>
{
	TRACE_PRIVATE_FIELD(InIndex + int(EIndexPack::AuxFieldCounter), InOffset, WideString);
};
template <int InIndex, int InOffset, typename DefinitionType>
struct TField<InIndex, InOffset, TEventRef<DefinitionType>>
{
	TRACE_PRIVATE_FIELD(InIndex, InOffset, DefinitionType);
public:
	TField(const FLiteralName& Name, uint32 ReferencedUid)
		: FieldDesc(Name, Tid, Offset, Size, ReferencedUid)
	{
	}
};
template <int InIndex, int InOffset, typename Type>
struct TField
{
	TRACE_PRIVATE_FIELD(InIndex, InOffset, Type);
};
#undef TRACE_PRIVATE_FIELD
enum EventProps {};
template <int InNumFields, int InSize>
struct TField<InNumFields, InSize, EventProps>
{
	enum : uint16
	{
		NumFields		= InNumFields & int(EIndexPack::NumFieldsMask),
		Size			= InSize,
		NumAuxFields	= (InNumFields >> int(EIndexPack::NumFieldsShift)) & int(EIndexPack::NumFieldsMask),
	};
};
} // namespace Trace
} // namespace UE
#endif // UE_TRACE_ENABLED
/* {{{1 EventNode.h */

#if UE_TRACE_ENABLED
namespace UE {
namespace Trace {
namespace Private {
struct FEventInfo
{
	enum
	{
		Flag_None			= 0,
		Flag_Important		= 1 << 0,
		Flag_MaybeHasAux	= 1 << 1,
		Flag_NoSync			= 1 << 2,
		Flag_Definition8	= 1 << 3,
		Flag_Definition16	= 1 << 4,
		Flag_Definition32	= 1 << 5,
		Flag_Definition64	= 1 << 6,
		DefinitionBits		= Flag_Definition8 | Flag_Definition16 | Flag_Definition32 | Flag_Definition64,
	};
	FLiteralName			LoggerName;
	FLiteralName			EventName;
	const FFieldDesc*		Fields;
	uint16					FieldCount;
	uint16					Flags;
};
class FEventNode
{
public:
	struct FIter
	{
		const FEventNode*	GetNext();
		void*				Inner;
	};
	static FIter			Read();
	static FIter			ReadNew();
	static void				OnConnect();
	TRACELOG_API uint32		Initialize(const FEventInfo* InInfo);
	void					Describe() const;
	uint32					GetUid() const { return Uid; }
private:
	FEventNode*				Next;
	const FEventInfo*		Info;
	uint32					Uid;
};
} // namespace Private
} // namespace Trace
} // namespace UE
#endif // UE_TRACE_ENABLED
/* {{{1 ImportantLogScope.h */

#if UE_TRACE_ENABLED
namespace UE {
namespace Trace {
namespace Private {
class FImportantLogScope
{
public:
	template <typename EventType>
	static FImportantLogScope	Enter();
	template <typename EventType>
	static FImportantLogScope	Enter(uint32 ArrayDataSize);
	void						operator += (const FImportantLogScope&) const;
	const FImportantLogScope&	operator << (bool) const	{ return *this; }
	constexpr explicit			operator bool () const		{ return true; }
	template <typename FieldMeta, typename Type>
	struct FFieldSet;
private:
	static FImportantLogScope	EnterImpl(uint32 Uid, uint32 Size);
	uint8*						Ptr;
	int32						BufferOffset;
	int32						AuxCursor;
};
} // namespace Private
} // namespace Trace
} // namespace UE
#endif // UE_TRACE_ENABLED
/* {{{1 SharedBuffer.h */

#if UE_TRACE_ENABLED
namespace UE {
namespace Trace {
namespace Private {
struct FSharedBuffer
{
	enum : uint32 {	CursorShift	= 10 };
	enum : uint32 {	RefBit		= 1 << 0 };
	enum : uint32 {	RefInit		= (1 << CursorShift) - 1 };
	enum : uint32 {	MaxSize		= 1 << (32 - CursorShift - 1) };
	int32 volatile	Cursor; // also packs in a ref count.
	uint32			Size;
	uint32			Final;
	uint32			_Unused;
	FSharedBuffer*	Next;
};
struct FNextSharedBuffer
{
	FSharedBuffer*	Buffer;
	int32			RegionStart;
};
} // namespace Private
} // namespace Trace
} // namespace UE
#endif // UE_TRACE_ENABLED
/* {{{1 ImportantLogScope.inl */

#if UE_TRACE_ENABLED
namespace UE {
namespace Trace {
namespace Private {
extern TRACELOG_API FSharedBuffer* volatile GSharedBuffer;
TRACELOG_API FNextSharedBuffer				Writer_NextSharedBuffer(FSharedBuffer*, int32, int32);
template <class T>
FORCENOINLINE FImportantLogScope FImportantLogScope::Enter(uint32 ArrayDataSize)
{
	static_assert(!!(uint32(T::EventFlags) & uint32(FEventInfo::Flag_MaybeHasAux)), "Only important trace events with array-type fields need a size parameter to UE_TRACE_LOG()");
	ArrayDataSize += sizeof(FAuxHeader) * T::EventProps_Meta::NumAuxFields;
	ArrayDataSize += 1; // for AuxDataTerminal
	uint32 Size = T::GetSize();
	uint32 Uid = T::GetUid() >> EKnownEventUids::_UidShift;
	FImportantLogScope Ret = EnterImpl(Uid, Size + ArrayDataSize);
	Ret.AuxCursor += Size;
	Ret.Ptr[Ret.AuxCursor] = uint8(EKnownEventUids::AuxDataTerminal);
	return Ret;
}
template <class T>
inline FImportantLogScope FImportantLogScope::Enter()
{
	static_assert(!(uint32(T::EventFlags) & uint32(FEventInfo::Flag_MaybeHasAux)), "Important trace events with array-type fields must be traced with UE_TRACE_LOG(Logger, Event, Channel, ArrayDataSize)");
	uint32 Size = T::GetSize();
	uint32 Uid = T::GetUid() >> EKnownEventUids::_UidShift;
	return EnterImpl(Uid, Size);
}
inline FImportantLogScope FImportantLogScope::EnterImpl(uint32 Uid, uint32 Size)
{
	FSharedBuffer* Buffer = AtomicLoadAcquire(&GSharedBuffer);
	int32 AllocSize = Size;
	AllocSize += sizeof(FImportantEventHeader);
	int32 NegSizeAndRef = 0 - ((AllocSize << FSharedBuffer::CursorShift) | FSharedBuffer::RefBit);
	int32 RegionStart = AtomicAddRelaxed(&(Buffer->Cursor), NegSizeAndRef);
	if (UNLIKELY(RegionStart + NegSizeAndRef < 0))
	{
		FNextSharedBuffer Next = Writer_NextSharedBuffer(Buffer, RegionStart, NegSizeAndRef);
		Buffer = Next.Buffer;
		RegionStart = Next.RegionStart;
	}
	int32 Bias = (RegionStart >> FSharedBuffer::CursorShift);
	uint8* Out = (uint8*)Buffer - Bias;
	uint16 Values16[] = { uint16(Uid), uint16(Size) };
	memcpy(Out, Values16, sizeof(Values16)); /* FImportantEventHeader::Uid,Size */
	FImportantLogScope Ret;
	Ret.Ptr = Out + sizeof(FImportantEventHeader);
	Ret.BufferOffset = int32(PTRINT(Buffer) - PTRINT(Ret.Ptr));
	Ret.AuxCursor = 0;
	return Ret;
}
inline void FImportantLogScope::operator += (const FImportantLogScope&) const
{
	auto* Buffer = (FSharedBuffer*)(Ptr + BufferOffset);
	AtomicAddRelease(&(Buffer->Cursor), int32(FSharedBuffer::RefBit));
}
template <typename FieldMeta, typename Type>
struct FImportantLogScope::FFieldSet
{
	static void Impl(FImportantLogScope* Scope, const Type& Value)
	{
		uint8* Dest = (uint8*)(Scope->Ptr) + FieldMeta::Offset;
		::memcpy(Dest, &Value, sizeof(Type));
	}
};
template <typename FieldMeta, typename Type>
struct FImportantLogScope::FFieldSet<FieldMeta, Type[]>
{
	static void Impl(FImportantLogScope* Scope, Type const* Data, int32 Num)
	{
		uint32 Size = Num * sizeof(Type);
		uint32 Pack = Size << FAuxHeader::SizeShift;
		Pack |= (FieldMeta::Index & int32(EIndexPack::NumFieldsMask)) << FAuxHeader::FieldShift;
		uint8* Out = Scope->Ptr + Scope->AuxCursor;
		memcpy(Out, &Pack, sizeof(Pack)); /* FAuxHeader::Pack */
		Out[0] = uint8(EKnownEventUids::AuxData); /* FAuxHeader::Uid */
		memcpy(Out + sizeof(FAuxHeader), Data, Size);
		Scope->AuxCursor += sizeof(FAuxHeader) + Size;
		Scope->Ptr[Scope->AuxCursor] = uint8(EKnownEventUids::AuxDataTerminal);
	}
};
template <typename FieldMeta>
struct FImportantLogScope::FFieldSet<FieldMeta, AnsiString>
{
	static void Impl(FImportantLogScope* Scope, const ANSICHAR* String, int32 Length=-1)
	{
		if (Length < 0)
		{
			Length = int32(strlen(String));
		}
		uint32 Pack = Length << FAuxHeader::SizeShift;
		Pack |= (FieldMeta::Index & int32(EIndexPack::NumFieldsMask)) << FAuxHeader::FieldShift;
		uint8* Out = Scope->Ptr + Scope->AuxCursor;
		memcpy(Out, &Pack, sizeof(Pack)); /* FAuxHeader::FieldIndex_Size */
		Out[0] = uint8(EKnownEventUids::AuxData); /* FAuxHeader::Uid */
		memcpy(Out + sizeof(FAuxHeader), String, Length);
		Scope->AuxCursor += sizeof(FAuxHeader) + Length;
		Scope->Ptr[Scope->AuxCursor] = uint8(EKnownEventUids::AuxDataTerminal);
	}
	static void Impl(FImportantLogScope* Scope, const WIDECHAR* String, int32 Length=-1)
	{
		if (Length < 0)
		{
			Length = 0;
			for (const WIDECHAR* c = String; *c; ++c, ++Length);
		}
		uint32 Pack = Length << FAuxHeader::SizeShift;
		Pack |= (FieldMeta::Index & int32(EIndexPack::NumFieldsMask)) << FAuxHeader::FieldShift;
		uint8* Out = Scope->Ptr + Scope->AuxCursor;
		memcpy(Out, &Pack, sizeof(Pack)); /* FAuxHeader::FieldIndex_Size */
		Out[0] = uint8(EKnownEventUids::AuxData); /* FAuxHeader::Uid */
		Out += sizeof(FAuxHeader);
		for (int32 i = 0; i < Length; ++i)
		{
			*Out = int8(*String);
			++Out;
			++String;
		}
		Scope->AuxCursor += sizeof(FAuxHeader) + Length;
		Scope->Ptr[Scope->AuxCursor] = uint8(EKnownEventUids::AuxDataTerminal);
	}
};
template <typename FieldMeta>
struct FImportantLogScope::FFieldSet<FieldMeta, WideString>
{
	static void Impl(FImportantLogScope* Scope, const WIDECHAR* String, int32 Length=-1)
	{
		if (Length < 0)
		{
			Length = 0;
			for (const WIDECHAR* c = String; *c; ++c, ++Length);
		}
		uint32 Size = Length * sizeof(WIDECHAR);
		uint32 Pack = Size << FAuxHeader::SizeShift;
		Pack |= (FieldMeta::Index & int32(EIndexPack::NumFieldsMask)) << FAuxHeader::FieldShift;
		uint8* Out = Scope->Ptr + Scope->AuxCursor;
		memcpy(Out, &Pack, sizeof(Pack));
		Out[0] = uint8(EKnownEventUids::AuxData);
		memcpy(Out + sizeof(FAuxHeader), String, Size);
		Scope->AuxCursor += sizeof(FAuxHeader) + Size;
		Scope->Ptr[Scope->AuxCursor] = uint8(EKnownEventUids::AuxDataTerminal);
	}
};
template <typename FieldMeta, typename DefinitionType>
struct FImportantLogScope::FFieldSet<FieldMeta, TEventRef<DefinitionType>>
{
	static void Impl(FImportantLogScope* Scope, const TEventRef<DefinitionType>& Reference)
	{
		FFieldSet<FieldMeta, DefinitionType>::Impl(Scope, Reference.Id);
	}
};
} // namespace Private
} // namespace Trace
} // namespace UE
#endif // UE_TRACE_ENABLED
/* {{{1 LogScope.h */

#if UE_TRACE_ENABLED
namespace UE {
namespace Trace {
namespace Private {
struct FWriteBuffer;
template <bool bMaybeHasAux> class TLogScope;
class FLogScope
{
	friend class FEventNode;
public:
	template <typename EventType>
	static auto				Enter();
	template <typename EventType>
	static auto				ScopedEnter();
	template <typename EventType>
	static auto				ScopedStampedEnter();
	void*					GetPointer() const			{ return Ptr; }
	const FLogScope&		operator << (bool) const	{ return *this; }
	constexpr explicit		operator bool () const		{ return true; }
	template <typename FieldMeta, typename Type>
	struct FFieldSet;
protected:
	void					Commit() const;
	void					Commit(FWriteBuffer* __restrict LatestBuffer) const;
private:
	template <uint32 Flags>
	static auto				EnterImpl(uint32 Uid, uint32 Size);
	template <class T> void	EnterPrelude(uint32 Size);
	void					Enter(uint32 Uid, uint32 Size);
	void					EnterNoSync(uint32 Uid, uint32 Size);
	uint8*					Ptr;
	FWriteBuffer*			Buffer;
};
template <bool bMaybeHasAux>
class TLogScope
	: public FLogScope
{
public:
	void					operator += (const FLogScope&) const;
};
class FScopedLogScope
{
public:
			~FScopedLogScope();
	void	SetActive();
	bool	bActive = false;
};
class FScopedStampedLogScope
{
public:
			~FScopedStampedLogScope();
	void	SetActive();
	bool	bActive = false;
};
} // namespace Private
} // namespace Trace
} // namespace UE
#endif // UE_TRACE_ENABLED
/* {{{1 LogScope.inl */

#if UE_TRACE_ENABLED
namespace UE {
namespace Trace {
namespace Private {
extern TRACELOG_API uint32 volatile	GLogSerial;
inline void FLogScope::Commit() const
{
	AtomicStoreRelease((uint8**) &(Buffer->Committed), Buffer->Cursor);
}
inline void FLogScope::Commit(FWriteBuffer* __restrict LatestBuffer) const
{
	if (LatestBuffer != Buffer)
	{
		AtomicStoreRelease((uint8**) &(LatestBuffer->Committed), LatestBuffer->Cursor);
	}
	Commit();
}
template <uint32 Flags>
inline auto FLogScope::EnterImpl(uint32 Uid, uint32 Size)
{
	TLogScope<(Flags & FEventInfo::Flag_MaybeHasAux) != 0> Ret;
	if ((Flags & FEventInfo::Flag_NoSync) != 0)
	{
		Ret.EnterNoSync(Uid, Size);
	}
	else
	{
		Ret.Enter(Uid, Size);
	}
	return Ret;
}
template <class HeaderType>
inline void FLogScope::EnterPrelude(uint32 Size)
{
	uint32 AllocSize = sizeof(HeaderType) + Size;
	Buffer = Writer_GetBuffer();
	if (UNLIKELY(Buffer->Cursor + AllocSize > (uint8*)Buffer))
	{
		Buffer = Writer_NextBuffer();
	}
#if !UE_BUILD_SHIPPING && !UE_BUILD_TEST
	if (AllocSize >= Buffer->Size)
	{
		PLATFORM_BREAK();
	}
#endif
	Ptr = Buffer->Cursor + sizeof(HeaderType);
	Buffer->Cursor += AllocSize;
}
inline void FLogScope::Enter(uint32 Uid, uint32 Size)
{
	EnterPrelude<FEventHeaderSync>(Size);
	uint16 Uid16 = uint16(Uid) | int32(EKnownEventUids::Flag_TwoByteUid);
	uint32 Serial = uint32(AtomicAddRelaxed(&GLogSerial, 1u));
	memcpy(Ptr - 3, &Serial, sizeof(Serial)); /* FEventHeaderSync::SerialHigh,SerialLow */
	memcpy(Ptr - 5, &Uid16,  sizeof(Uid16));  /* FEventHeaderSync::Uid */
}
inline void FLogScope::EnterNoSync(uint32 Uid, uint32 Size)
{
	EnterPrelude<FEventHeader>(Size);
	uint16 Uid16 = uint16(Uid) | int32(EKnownEventUids::Flag_TwoByteUid);
	auto* Header = (uint16*)(Ptr);
	memcpy(Header - 1, &Uid16, sizeof(Uid16)); /* FEventHeader::Uid */
}
template </*bMaybeHasAux*/>
inline void TLogScope<false>::operator += (const FLogScope&) const
{
	Commit();
}
template </*bMaybeHasAux*/>
inline void TLogScope<true>::operator += (const FLogScope&) const
{
	FWriteBuffer* LatestBuffer = Writer_GetBuffer();
	LatestBuffer->Cursor[0] = uint8(EKnownEventUids::AuxDataTerminal << EKnownEventUids::_UidShift);
	LatestBuffer->Cursor++;
	Commit(LatestBuffer);
}
inline FScopedLogScope::~FScopedLogScope()
{
	if (!bActive)
	{
		return;
	}
	uint8 LeaveUid = uint8(EKnownEventUids::LeaveScope << EKnownEventUids::_UidShift);
	FWriteBuffer* Buffer = Writer_GetBuffer();
	if (UNLIKELY(int32((uint8*)Buffer - Buffer->Cursor) < int32(sizeof(LeaveUid))))
	{
		Buffer = Writer_NextBuffer();
	}
	Buffer->Cursor[0] = LeaveUid;
	Buffer->Cursor += sizeof(LeaveUid);
	AtomicStoreRelease((uint8**) &(Buffer->Committed), Buffer->Cursor);
}
inline void FScopedLogScope::SetActive()
{
	bActive = true;
}
inline FScopedStampedLogScope::~FScopedStampedLogScope()
{
	if (!bActive)
	{
		return;
	}
	FWriteBuffer* Buffer = Writer_GetBuffer();
	uint64 Stamp = Writer_GetTimestamp(Buffer);
	if (UNLIKELY(int32((uint8*)Buffer - Buffer->Cursor) < int32(sizeof(Stamp))))
	{
		Buffer = Writer_NextBuffer();
	}
	Stamp <<= 8;
	Stamp += uint8(EKnownEventUids::LeaveScope_T) << EKnownEventUids::_UidShift;
	memcpy((uint64*)(Buffer->Cursor), &Stamp, sizeof(Stamp));
	Buffer->Cursor += sizeof(Stamp);
	AtomicStoreRelease((uint8**) &(Buffer->Committed), Buffer->Cursor);
}
inline void FScopedStampedLogScope::SetActive()
{
	bActive = true;
}
template <class EventType>
FORCENOINLINE auto FLogScope::Enter()
{
	uint32 Size = EventType::GetSize();
	uint32 Uid = EventType::GetUid();
	return EnterImpl<EventType::EventFlags>(Uid, Size);
}
template <class EventType>
FORCENOINLINE auto FLogScope::ScopedEnter()
{
	uint8 EnterUid = uint8(EKnownEventUids::EnterScope << EKnownEventUids::_UidShift);
	FWriteBuffer* Buffer = Writer_GetBuffer();
	if (UNLIKELY(int32((uint8*)Buffer - Buffer->Cursor) < int32(sizeof(EnterUid))))
	{
		Buffer = Writer_NextBuffer();
	}
	Buffer->Cursor[0] = EnterUid;
	Buffer->Cursor += sizeof(EnterUid);
	AtomicStoreRelease((uint8**) &(Buffer->Committed), Buffer->Cursor);
	return Enter<EventType>();
}
template <class EventType>
FORCENOINLINE auto FLogScope::ScopedStampedEnter()
{
	uint64 Stamp;
	FWriteBuffer* Buffer = Writer_GetBuffer();
	if (UNLIKELY(int32((uint8*)Buffer - Buffer->Cursor) < int32(sizeof(Stamp))))
	{
		Buffer = Writer_NextBuffer();
	}
	Stamp = Writer_GetTimestamp(Buffer);
	Stamp <<= 8;
	Stamp += uint8(EKnownEventUids::EnterScope_T) << EKnownEventUids::_UidShift;
	memcpy((uint64*)(Buffer->Cursor), &Stamp, sizeof(Stamp));
	Buffer->Cursor += sizeof(Stamp);
	AtomicStoreRelease((uint8**) &(Buffer->Committed), Buffer->Cursor);
	return Enter<EventType>();
}
template <typename FieldMeta, typename Type>
struct FLogScope::FFieldSet
{
	static void Impl(FLogScope* Scope, const Type& Value)
	{
		uint8* Dest = (uint8*)(Scope->Ptr) + FieldMeta::Offset;
		::memcpy(Dest, &Value, sizeof(Type));
	}
};
template <typename FieldMeta, typename Type>
struct FLogScope::FFieldSet<FieldMeta, Type[]>
{
	static void Impl(FLogScope*, Type const* Data, int32 Num)
	{
		static const uint32 Index = FieldMeta::Index & int32(EIndexPack::NumFieldsMask);
		int32 Size = (Num * sizeof(Type)) & (FAuxHeader::SizeLimit - 1) & ~(sizeof(Type) - 1);
		Field_WriteAuxData(Index, (const uint8*)Data, Size);
	}
};
#if STATICALLY_SIZED_ARRAY_FIELDS_SUPPORT
template <typename FieldMeta, typename Type, int32 Count>
struct FLogScope::FFieldSet<FieldMeta, Type[Count]>
{
	static void Impl(FLogScope*, Type const* Data, int32 Num=-1) = delete;
};
#endif // STATICALLY_SIZED_ARRAY_FIELDS_SUPPORT
template <typename FieldMeta>
struct FLogScope::FFieldSet<FieldMeta, AnsiString>
{
	static void Impl(FLogScope*, const ANSICHAR* String, int32 Length=-1)
	{
		if (Length < 0)
		{
			Length = int32(strlen(String));
		}
		static const uint32 Index = FieldMeta::Index & int32(EIndexPack::NumFieldsMask);
		Field_WriteStringAnsi(Index, String, Length);
	}
	static void Impl(FLogScope*, const WIDECHAR* String, int32 Length=-1)
	{
		if (Length < 0)
		{
			Length = 0;
			for (const WIDECHAR* c = String; *c; ++c, ++Length);
		}
		static const uint32 Index = FieldMeta::Index & int32(EIndexPack::NumFieldsMask);
		Field_WriteStringAnsi(Index, String, Length);
	}
};
template <typename FieldMeta>
struct FLogScope::FFieldSet<FieldMeta, WideString>
{
	static void Impl(FLogScope*, const WIDECHAR* String, int32 Length=-1)
	{
		if (Length < 0)
		{
			Length = 0;
			for (const WIDECHAR* c = String; *c; ++c, ++Length);
		}
		static const uint32 Index = FieldMeta::Index & int32(EIndexPack::NumFieldsMask);
		Field_WriteStringWide(Index, String, Length);
	}
};
template <typename FieldMeta, typename DefinitionType>
struct FLogScope::FFieldSet<FieldMeta, TEventRef<DefinitionType>>
{
	static void Impl(FLogScope* Scope, const TEventRef<DefinitionType>& Reference)
	{
		FFieldSet<FieldMeta, DefinitionType>::Impl(Scope, Reference.Id);
	}
};
} // namespace Private
} // namespace Trace
} // namespace UE
#endif // UE_TRACE_ENABLED
/* {{{1 Trace.inl */

/* {{{1 Transport.h */

namespace UE {
namespace Trace {
enum ETransport : uint8
{
	_Unused			= 0,
	Raw				= 1,
	Packet			= 2,
	TidPacket		= 3,
	TidPacketSync	= 4,
	Active			= TidPacketSync,
};
enum ETransportTid : uint32
{
	Events		= 0,			// used to describe events
	Internal	= 1,			// events to make the trace stream function
	Importants	= Internal,		// important/cached events
	Bias,						// [Bias,End] = threads. Note bias can't be..
	/* ... */					// ..changed as it breaks backwards compat :(
	End			= 0x3ffe,		// two msbs are user for packet markers
	Sync		= 0x3fff,		// see Writer_SendSync()
};
namespace Private
{
struct FTidPacketBase
{
	enum : uint16
	{
		EncodedMarker = 0x8000,
		PartialMarker = 0x4000, // now unused. fragmented aux-data has an event header
		ThreadIdMask  = PartialMarker - 1,
	};
	uint16 PacketSize;
	uint16 ThreadId;
};
template <uint32 DataSize>
struct TTidPacket
	: public FTidPacketBase
{
	uint8	Data[DataSize];
};
template <uint32 DataSize>
struct TTidPacketEncoded
	: public FTidPacketBase
{
	uint16	DecodedSize;
	uint8	Data[DataSize];
};
using FTidPacket		= TTidPacket<0>;
using FTidPacketEncoded = TTidPacketEncoded<0>;
static_assert(sizeof(FTidPacket) == 4, "");
static_assert(sizeof(FTidPacketEncoded) == 6, "");
} // namespace Private
} // namespace Trace
} // namespace UE
/* {{{1 Platform.h */

#if UE_TRACE_ENABLED
namespace UE {
namespace Trace {
namespace Private {
UPTRINT	ThreadCreate(const ANSICHAR* Name, void (*Entry)());
void	ThreadSleep(uint32 Milliseconds);
void	ThreadJoin(UPTRINT Handle);
void	ThreadDestroy(UPTRINT Handle);
uint64				TimeGetFrequency();
TRACELOG_API uint64	TimeGetTimestamp();
UPTRINT	TcpSocketConnect(const ANSICHAR* Host, uint16 Port);
UPTRINT	TcpSocketListen(uint16 Port);
int32	TcpSocketAccept(UPTRINT Socket, UPTRINT& Out);
bool	TcpSocketHasData(UPTRINT Socket);
int32	IoRead(UPTRINT Handle, void* Data, uint32 Size);
bool	IoWrite(UPTRINT Handle, const void* Data, uint32 Size);
void	IoClose(UPTRINT Handle);
UPTRINT	FileOpen(const ANSICHAR* Path);
} // namespace Private
} // namespace Trace
} // namespace UE
#endif // UE_TRACE_ENABLED
/* {{{1 WriteBufferRedirect.h */

namespace UE {
namespace Trace {
namespace Private {
extern thread_local FWriteBuffer* GTlsWriteBuffer;
template <int BufferSize>
class TWriteBufferRedirect
{
public:
	enum : uint16 { ActiveRedirection = 0xffff };
					TWriteBufferRedirect();
					~TWriteBufferRedirect();
	void			Close();
	void			Abandon();
	uint8*			GetData();
	uint32			GetSize() const;
	uint32			GetCapacity() const;
	void			Reset();
private:
	FWriteBuffer*	PrevBuffer;
	uint8			Data[BufferSize];
	FWriteBuffer	Buffer;
};
template <int BufferSize>
inline TWriteBufferRedirect<BufferSize>::TWriteBufferRedirect()
{
	Reset();
	PrevBuffer = GTlsWriteBuffer;
	GTlsWriteBuffer = &Buffer;
	Buffer.Size = uint16(BufferSize);
	Buffer.ThreadId = ActiveRedirection;
}
template <int BufferSize>
inline TWriteBufferRedirect<BufferSize>::~TWriteBufferRedirect()
{
	Close();
}
template <int BufferSize>
inline void TWriteBufferRedirect<BufferSize>::Close()
{
	if (PrevBuffer == nullptr)
	{
		return;
	}
	GTlsWriteBuffer = PrevBuffer;
	PrevBuffer = nullptr;
}
template <int BufferSize>
inline void TWriteBufferRedirect<BufferSize>::Abandon()
{
	PrevBuffer = nullptr;
}
template <int BufferSize>
inline uint8* TWriteBufferRedirect<BufferSize>::GetData()
{
	return Buffer.Reaped;
}
template <int BufferSize>
inline uint32 TWriteBufferRedirect<BufferSize>::GetSize() const
{
	return uint32(Buffer.Committed - Buffer.Reaped);
}
template <int BufferSize>
inline uint32 TWriteBufferRedirect<BufferSize>::GetCapacity() const
{
	return BufferSize;
}
template <int BufferSize>
inline void TWriteBufferRedirect<BufferSize>::Reset()
{
	Buffer.Cursor = Data + sizeof(uint32);
	Buffer.Committed = Buffer.Cursor;
	Buffer.Reaped = Buffer.Cursor;
}
} // namespace Private
} // namespace Trace
} // namespace UE
#if TRACE_IMPLEMENT
/* {{{1 BlockPool.cpp */

#if UE_TRACE_ENABLED
namespace UE {
namespace Trace {
namespace Private {
void*		Writer_MemoryAllocate(SIZE_T, uint32);
void		Writer_MemoryFree(void*, uint32);
struct FPoolPage
{
	FPoolPage*	NextPage;
	uint32		AllocSize;
};
struct FPoolBlockList
{
	FWriteBuffer*	Head;
	FWriteBuffer*	Tail;
};
#define T_ALIGN alignas(PLATFORM_CACHE_LINE_SIZE)
static const uint32						GPoolBlockSize		= 4 << 10;
static const uint32						GPoolPageSize		= GPoolBlockSize << 4;
static const uint32						GPoolInitPageSize	= GPoolBlockSize << 6;
T_ALIGN static FWriteBuffer* volatile	GPoolFreeList;		// = nullptr;
T_ALIGN static UPTRINT volatile			GPoolFutex;			// = 0
T_ALIGN static FPoolPage* volatile		GPoolPageList;		// = nullptr;
static uint32							GPoolUsage;			// = 0;
#undef T_ALIGN
static FPoolBlockList Writer_AllocateBlockList(uint32 PageSize)
{
	uint8* PageBase = (uint8*)Writer_MemoryAllocate(PageSize, PLATFORM_CACHE_LINE_SIZE);
	GPoolUsage += PageSize;
	uint32 BufferSize = GPoolBlockSize;
	BufferSize -= sizeof(FWriteBuffer);
	BufferSize -= sizeof(uint32); // to preceed event data with a small header when sending.
	uint8* FirstBlock = PageBase + GPoolBlockSize - sizeof(FWriteBuffer);
	uint8* Block = FirstBlock;
	for (int i = 1, n = PageSize / GPoolBlockSize; ; ++i)
	{
		auto* Buffer = (FWriteBuffer*)Block;
		Buffer->Size = uint16(BufferSize);
		if (i >= n)
		{
			break;
		}
		AtomicStoreRelaxed(&(Buffer->NextBuffer), (FWriteBuffer*)(Block + GPoolBlockSize));
		Block += GPoolBlockSize;
	}
	FWriteBuffer* NextBuffer = (FWriteBuffer*)FirstBlock;
	NextBuffer->Size -= sizeof(FPoolPage);
	FPoolPage* PageListNode = (FPoolPage*)PageBase;
	PageListNode->NextPage = GPoolPageList;
	PageListNode->AllocSize = PageSize;
	GPoolPageList = PageListNode;
	return { NextBuffer, (FWriteBuffer*)Block };
}
FWriteBuffer* Writer_AllocateBlockFromPool()
{
	FWriteBuffer* Ret;
	while (true)
	{
		FWriteBuffer* Owned = AtomicLoadRelaxed(&GPoolFreeList);
		if (Owned != nullptr)
		{
			FWriteBuffer* OwnedNext = AtomicLoadRelaxed(&(Owned->NextBuffer));
			if (!AtomicCompareExchangeAcquire(&GPoolFreeList, OwnedNext, Owned))
			{
				PlatformYield();
				continue;
			}
		}
		if (Owned != nullptr)
		{
			Ret = (FWriteBuffer*)Owned;
			break;
		}
		UPTRINT Futex = AtomicLoadRelaxed(&GPoolFutex);
		if (Futex || !AtomicCompareExchangeAcquire(&GPoolFutex, Futex + 1, Futex))
		{
			ThreadSleep(0);
			continue;
		}
		FPoolBlockList BlockList = Writer_AllocateBlockList(GPoolPageSize);
		Ret = BlockList.Head;
		for (auto* ListNode = BlockList.Tail;; PlatformYield())
		{
			FWriteBuffer* FreeListValue = AtomicLoadRelaxed(&GPoolFreeList);
			AtomicStoreRelaxed(&(ListNode->NextBuffer), FreeListValue);
			if (AtomicCompareExchangeRelease(&GPoolFreeList, Ret->NextBuffer, FreeListValue))
			{
				break;
			}
		}
		for (;; Private::PlatformYield())
		{
			if (AtomicCompareExchangeRelease<UPTRINT>(&GPoolFutex, 0, 1))
			{
				break;
			}
		}
		break;
	}
	return Ret;
}
void Writer_FreeBlockListToPool(FWriteBuffer* Head, FWriteBuffer* Tail)
{
	for (FWriteBuffer* ListNode = Tail;; PlatformYield())
	{
		FWriteBuffer* FreeListValue = AtomicLoadRelaxed(&GPoolFreeList);
		AtomicStoreRelaxed(&(ListNode->NextBuffer), FreeListValue);
		if (AtomicCompareExchangeRelease(&GPoolFreeList, Head, FreeListValue))
		{
			break;
		}
	}
}
void Writer_InitializePool()
{
	static_assert(GPoolPageSize >= 0x10000, "Page growth must be >= 64KB");
	static_assert(GPoolInitPageSize >= 0x10000, "Initial page size must be >= 64KB");
}
void Writer_ShutdownPool()
{
	for (auto* Page = AtomicLoadRelaxed(&GPoolPageList); Page != nullptr;)
	{
		FPoolPage* NextPage = Page->NextPage;
		uint32 PageSize = (NextPage == nullptr) ? GPoolBlockSize : GPoolPageSize;
		Writer_MemoryFree(Page, PageSize);
		Page = NextPage;
	}
}
} // namespace Private
} // namespace Trace
} // namespace UE
#endif // UE_TRACE_ENABLED
/* {{{1 Channel.cpp */

#include <ctype.h>
#if UE_TRACE_ENABLED
namespace UE {
namespace Trace {
struct FTraceChannel : public FChannel
{
	bool IsEnabled() const { return true; }
	explicit operator bool() const { return true; }
};
static FTraceChannel	TraceLogChannelDetail;
FChannel&				TraceLogChannel			= TraceLogChannelDetail;
UE_TRACE_EVENT_BEGIN(Trace, ChannelAnnounce, NoSync|Important)
	UE_TRACE_EVENT_FIELD(uint32, Id)
	UE_TRACE_EVENT_FIELD(bool, IsEnabled)
	UE_TRACE_EVENT_FIELD(bool, ReadOnly)
	UE_TRACE_EVENT_FIELD(AnsiString, Name)
UE_TRACE_EVENT_END()
UE_TRACE_EVENT_BEGIN(Trace, ChannelToggle, NoSync|Important)
	UE_TRACE_EVENT_FIELD(uint32, Id)
	UE_TRACE_EVENT_FIELD(bool, IsEnabled)
UE_TRACE_EVENT_END()
static FChannel* volatile	GHeadChannel;			// = nullptr;
static FChannel* volatile	GNewChannelList;		// = nullptr;
static bool 				GChannelsInitialized;
static uint32 GetChannelHash(const ANSICHAR* Input, int32 Length)
{
	if (Length > 0 && (Input[Length - 1] | 0x20) == 's')
	{
		--Length;
	}
	uint32 Result = 0x811c9dc5;
	for (; Length; ++Input, --Length)
	{
		Result ^= *Input | 0x20; // a cheap ASCII-only case insensitivity.
		Result *= 0x01000193;
	}
	return Result;
}
static uint32 GetChannelNameLength(const ANSICHAR* ChannelName)
{
	size_t Len = uint32(strlen(ChannelName));
	if (Len > 7)
	{
		if (strcmp(ChannelName + Len - 7, "Channel") == 0)
		{
			Len -= 7;
		}
	}
	return uint32(Len);
}
FChannel::Iter::~Iter()
{
	if (Inner[2] == nullptr)
	{
		return;
	}
	using namespace Private;
	for (auto* Node = (FChannel*)Inner[2];; PlatformYield())
	{
		Node->Next = AtomicLoadRelaxed(&GHeadChannel);
		if (AtomicCompareExchangeRelaxed(&GHeadChannel, (FChannel*)Inner[1], Node->Next))
		{
			break;
		}
	}
}
const FChannel* FChannel::Iter::GetNext()
{
	auto* Ret = (const FChannel*)Inner[0];
	if (Ret != nullptr)
	{
		Inner[0] = Ret->Next;
		if (Inner[0] != nullptr)
		{
			Inner[2] = Inner[0];
		}
	}
	return Ret;
}
FChannel::Iter FChannel::ReadNew()
{
	using namespace Private;
	FChannel* List = AtomicLoadRelaxed(&GNewChannelList);
	if (List == nullptr)
	{
		return {};
	}
	while (!AtomicCompareExchangeAcquire(&GNewChannelList, (FChannel*)nullptr, List))
	{
		PlatformYield();
		List = AtomicLoadRelaxed(&GNewChannelList);
	}
	return { { List, List, List } };
}
void FChannel::Setup(const ANSICHAR* InChannelName, const InitArgs& InArgs)
{
	using namespace Private;
	Name.Ptr = InChannelName;
	Name.Len = GetChannelNameLength(Name.Ptr);
	Name.Hash = GetChannelHash(Name.Ptr, Name.Len);
	Args = InArgs;
	for (;; PlatformYield())
	{
		FChannel* HeadChannel = AtomicLoadRelaxed(&GNewChannelList);
		Next = HeadChannel;
		if (AtomicCompareExchangeRelease(&GNewChannelList, this, Next))
		{
			break;
		}
	}
	if (GChannelsInitialized)
	{
		Enabled = -1;
	}
}
void FChannel::Announce() const
{
	UE_TRACE_LOG(Trace, ChannelAnnounce, TraceLogChannel, Name.Len * sizeof(ANSICHAR))
		<< ChannelAnnounce.Id(Name.Hash)
		<< ChannelAnnounce.IsEnabled(IsEnabled())
		<< ChannelAnnounce.ReadOnly(Args.bReadOnly)
		<< ChannelAnnounce.Name(Name.Ptr, Name.Len);
}
void FChannel::Initialize()
{
	ToggleAll(false);
	GChannelsInitialized = true;
}
void FChannel::ToggleAll(bool bEnabled)
{
	using namespace Private;
	FChannel* ChannelLists[] =
	{
		AtomicLoadAcquire(&GNewChannelList),
		AtomicLoadAcquire(&GHeadChannel),
	};
	for (FChannel* Channel : ChannelLists)
	{
		for (; Channel != nullptr; Channel = (FChannel*)(Channel->Next))
		{
			Channel->Toggle(bEnabled);
		}
	}
}
FChannel* FChannel::FindChannel(const ANSICHAR* ChannelName)
{
	using namespace Private;
	const uint32 ChannelNameLen = GetChannelNameLength(ChannelName);
	const uint32 ChannelNameHash = GetChannelHash(ChannelName, ChannelNameLen);
	FChannel* ChannelLists[] =
	{
		AtomicLoadAcquire(&GNewChannelList),
		AtomicLoadAcquire(&GHeadChannel),
	};
	for (FChannel* Channel : ChannelLists)
	{
		for (; Channel != nullptr; Channel = Channel->Next)
		{
			if (Channel->Name.Hash == ChannelNameHash)
			{
				return Channel;
			}
		}
	}
	return nullptr;
}
void FChannel::EnumerateChannels(ChannelIterFunc Func, void* User) 
{
	using namespace Private;
	FChannel* ChannelLists[] =
	{
		AtomicLoadAcquire(&GNewChannelList),
		AtomicLoadAcquire(&GHeadChannel),
	};
	for (FChannel* Channel : ChannelLists)
	{
		for (; Channel != nullptr; Channel = Channel->Next)
		{
			Func(Channel->Name.Ptr, Channel->IsEnabled(), User);
		}
	}
}
void FChannel::EnumerateChannels(ChannelIterCallback Func, void* User)
{
	using namespace Private;
	FChannel* ChannelLists[] =
	{
		AtomicLoadAcquire(&GNewChannelList),
		AtomicLoadAcquire(&GHeadChannel),
	};
	FChannelInfo Info;
	for (FChannel* Channel : ChannelLists)
	{
		for (; Channel != nullptr; Channel = Channel->Next)
		{
			Info.Name = Channel->Name.Ptr;
			Info.Desc = Channel->Args.Desc;
			Info.bIsEnabled = Channel->IsEnabled();
			Info.bIsReadOnly = Channel->Args.bReadOnly;
			bool Result = Func(Info, User);
			if (!Result)
			{
				return;
			}
		}
	}
}
bool FChannel::Toggle(bool bEnabled)
{
	using namespace Private;
	AtomicStoreRelaxed(&Enabled, bEnabled ? 1 : -1);
	UE_TRACE_LOG(Trace, ChannelToggle, TraceLogChannel)
		<< ChannelToggle.Id(Name.Hash)
		<< ChannelToggle.IsEnabled(IsEnabled());
	return IsEnabled();
}
bool FChannel::Toggle(const ANSICHAR* ChannelName, bool bEnabled)
{
	if (FChannel* Channel = FChannel::FindChannel(ChannelName))
	{
		return Channel->Toggle(bEnabled);
	}
	return false;
}
} // namespace Trace
} // namespace UE
#endif // UE_TRACE_ENABLED
/* {{{1 Codec.cpp */

THIRD_PARTY_INCLUDES_START
#if defined(_MSC_VER)
#	pragma warning(push)
#	pragma warning(disable : 6239)
#endif
#if !defined(TRACE_PRIVATE_EXTERNAL_LZ4)
#	define LZ4_NAMESPACE Trace
#	undef LZ4_NAMESPACE
#	define TRACE_PRIVATE_LZ4_NAMESPACE ::Trace::
#else
#	define TRACE_PRIVATE_LZ4_NAMESPACE
#endif
#if defined(_MSC_VER)
#	pragma warning(pop)
#endif
THIRD_PARTY_INCLUDES_END
namespace UE {
namespace Trace {
namespace Private {
int32 Encode(const void* Src, int32 SrcSize, void* Dest, int32 DestSize)
{
	return TRACE_PRIVATE_LZ4_NAMESPACE LZ4_compress_fast(
		(const char*)Src,
		(char*)Dest,
		SrcSize,
		DestSize,
		1 // increase by 1 for small speed increase
	);
}
uint32 GetEncodeMaxSize(uint32 InputSize)
{
	return LZ4_COMPRESSBOUND(InputSize);
}
TRACELOG_API int32 Decode(const void* Src, int32 SrcSize, void* Dest, int32 DestSize)
{
	return TRACE_PRIVATE_LZ4_NAMESPACE LZ4_decompress_safe((const char*)Src, (char*)Dest, SrcSize, DestSize);
}
} // namespace Private
} // namespace Trace
} // namespace UE
/* {{{1 Control.cpp */

#if UE_TRACE_ENABLED
#include <type_traits>
namespace UE {
namespace Trace {
namespace Private {
#if !defined(TRACE_PRIVATE_CONTROL_ENABLED) || TRACE_PRIVATE_CONTROL_ENABLED
bool	Writer_SendTo(const ANSICHAR*, uint32=0, uint32=0);
bool	Writer_WriteTo(const ANSICHAR*, uint32=0);
bool	Writer_Stop();
enum class EControlState : uint8
{
	Closed = 0,
	Listening,
	Accepted,
	Failed,
};
struct FControlCommands
{
	enum { Max = 8 };
	struct
	{
		uint32	Hash;
		void*	Param;
		void	(*Thunk)(void*, uint32, ANSICHAR const* const*);
	}			Commands[Max];
	uint8		Count;
};
static_assert(std::is_trivial<FControlCommands>(), "FControlCommands must be trivial");
static FControlCommands	GControlCommands;
static UPTRINT			GControlListen		= 0;
static UPTRINT			GControlSocket		= 0;
static EControlState	GControlState;		// = EControlState::Closed;
static uint16			GControlPort		= 1985;
static uint32 Writer_ControlHash(const ANSICHAR* Word)
{
	uint32 Hash = 5381;
	for (; *Word; (Hash = (Hash * 33) ^ *Word), ++Word);
	return Hash;
}
static bool Writer_ControlAddCommand(
	const ANSICHAR* Name,
	void* Param,
	void (*Thunk)(void*, uint32, ANSICHAR const* const*))
{
	if (GControlCommands.Count >= FControlCommands::Max)
	{
		return false;
	}
	uint32 Index = GControlCommands.Count++;
	GControlCommands.Commands[Index] = { Writer_ControlHash(Name), Param, Thunk };
	return true;
}
static bool Writer_ControlDispatch(uint32 ArgC, ANSICHAR const* const* ArgV)
{
	if (ArgC == 0)
	{
		return false;
	}
	uint32 Hash = Writer_ControlHash(ArgV[0]);
	--ArgC;
	++ArgV;
	for (int i = 0, n = GControlCommands.Count; i < n; ++i)
	{
		const auto& Command = GControlCommands.Commands[i];
		if (Command.Hash == Hash)
		{
			Command.Thunk(Command.Param, ArgC, ArgV);
			return true;
		}
	}
	return false;
}
static bool Writer_ControlListen()
{
	GControlListen = TcpSocketListen(GControlPort);
	if (!GControlListen)
	{
		uint32 Seed = uint32(TimeGetTimestamp());
		for (uint32 i = 0; i < 10 && !GControlListen; Seed *= 13, ++i)
		{
			uint16 Port((Seed & 0x1fff) + 0x8000);
			GControlListen = TcpSocketListen(Port);
			if (GControlListen)
			{
				GControlPort = Port;
				break;
			}
		}
	}
	if (!GControlListen)
	{
		GControlState = EControlState::Failed;
		return false;
	}
	GControlState = EControlState::Listening;
	return true;
}
static bool Writer_ControlAccept()
{
	UPTRINT Socket;
	int Return = TcpSocketAccept(GControlListen, Socket);
	if (Return <= 0)
	{
		if (Return == -1)
		{
			IoClose(GControlListen);
			GControlListen = 0;
			GControlState = EControlState::Failed;
		}
		return false;
	}
	GControlState = EControlState::Accepted;
	GControlSocket = Socket;
	return true;
}
static void Writer_ControlRecv()
{
	ANSICHAR Buffer[512];
	ANSICHAR* __restrict Head = Buffer;
	while (TcpSocketHasData(GControlSocket))
	{
		int32 ReadSize = int32(UPTRINT(Buffer + sizeof(Buffer) - Head));
		int32 Recvd = IoRead(GControlSocket, Head, ReadSize);
		if (Recvd <= 0)
		{
			IoClose(GControlSocket);
			GControlSocket = 0;
			GControlState = EControlState::Listening;
			break;
		}
		Head += Recvd;
		enum EParseState
		{
			CrLfSkip,
			WhitespaceSkip,
			Word,
		} ParseState = EParseState::CrLfSkip;
		uint32 ArgC = 0;
		const ANSICHAR* ArgV[16];
		const ANSICHAR* __restrict Spent = Buffer;
		for (ANSICHAR* __restrict Cursor = Buffer; Cursor < Head; ++Cursor)
		{
			switch (ParseState)
			{
			case EParseState::CrLfSkip:
				if (*Cursor == '\n' || *Cursor == '\r')
				{
					continue;
				}
				ParseState = EParseState::WhitespaceSkip;
				/* [[fallthrough]] */
			case EParseState::WhitespaceSkip:
				if (*Cursor == ' ' || *Cursor == '\0')
				{
					continue;
				}
				if (ArgC < UE_ARRAY_COUNT(ArgV))
				{
					ArgV[ArgC] = Cursor;
					++ArgC;
				}
				ParseState = EParseState::Word;
				/* [[fallthrough]] */
			case EParseState::Word:
				if (*Cursor == ' ' || *Cursor == '\0')
				{
					*Cursor = '\0';
					ParseState = EParseState::WhitespaceSkip;
					continue;
				}
				if (*Cursor == '\r' || *Cursor == '\n')
				{
					*Cursor = '\0';
					Writer_ControlDispatch(ArgC, ArgV);
					ArgC = 0;
					Spent = Cursor + 1;
					ParseState = EParseState::CrLfSkip;
					continue;
				}
				break;
			}
		}
		int32 UnspentSize = int32(UPTRINT(Head - Spent));
		if (UnspentSize)
		{
			memmove(Buffer, Spent, UnspentSize);
		}
		Head = Buffer + UnspentSize;
	}
}
uint32 Writer_GetControlPort()
{
	return GControlPort;
}
void Writer_UpdateControl()
{
	switch (GControlState)
	{
	case EControlState::Closed:
		if (!Writer_ControlListen())
		{
			break;
		}
		/* [[fallthrough]] */
	case EControlState::Listening:
		if (!Writer_ControlAccept())
		{
			break;
		}
		/* [[fallthrough]] */
	case EControlState::Accepted:
		Writer_ControlRecv();
		break;
	}
}
void Writer_InitializeControl()
{
#if PLATFORM_SWITCH
	GControlState = EControlState::Failed;
	return;
#endif
	Writer_ControlAddCommand("SendTo", nullptr,
		[] (void*, uint32 ArgC, ANSICHAR const* const* ArgV)
		{
			if (ArgC > 0)
			{
				Writer_SendTo(ArgV[0]);
			}
		}
	);
	Writer_ControlAddCommand("WriteTo", nullptr,
		[] (void*, uint32 ArgC, ANSICHAR const* const* ArgV)
		{
			if (ArgC > 0)
			{
				Writer_WriteTo(ArgV[0]);
			}
		}
	);
	Writer_ControlAddCommand("Stop", nullptr,
		[] (void*, uint32 ArgC, ANSICHAR const* const* ArgV)
		{
			Writer_Stop();
		}
	);
	Writer_ControlAddCommand("ToggleChannels", nullptr,
		[] (void*, uint32 ArgC, ANSICHAR const* const* ArgV)
		{
			if (ArgC < 2)
			{
				return;
			}
			const size_t BufferSize = 512;
			ANSICHAR Channels[BufferSize] = {};
			ANSICHAR* Ctx;
			const bool bState = (ArgV[1][0] != '0');
			FCStringAnsi::Strcpy(Channels, BufferSize, ArgV[0]);
			ANSICHAR* Channel = FCStringAnsi::Strtok(Channels, ",", &Ctx);
			while (Channel)
			{
				FChannel::Toggle(Channel, bState);
				Channel = FCStringAnsi::Strtok(nullptr, ",", &Ctx);
			}
		}
	);
}
void Writer_ShutdownControl()
{
	if (GControlListen)
	{
		IoClose(GControlListen);
		GControlListen = 0;
	}
}
#else
void	Writer_InitializeControl()	{}
void	Writer_ShutdownControl()	{}
void	Writer_UpdateControl()		{}
uint32	Writer_GetControlPort()		{ return ~0u; }
#endif // TRACE_PRIVATE_CONTROL_ENABLED
} // namespace Private
} // namespace Trace
} // namespace UE
#endif // UE_TRACE_ENABLED
/* {{{1 EventNode.cpp */

#if UE_TRACE_ENABLED
namespace UE {
namespace Trace {
namespace Private {
void					Writer_InternalInitialize();
FEventNode* volatile	GNewEventList; // = nullptr;
FEventNode*				GEventListHead;// = nullptr;
FEventNode*				GEventListTail;// = nullptr;
const FEventNode* FEventNode::FIter::GetNext()
{
	auto* Ret = (FEventNode*)Inner;
	if (Ret != nullptr)
	{
		Inner = Ret->Next;
		if (Inner == nullptr)
		{
			GEventListTail = Ret;
		}
	}
	return Ret;
}
FEventNode::FIter FEventNode::Read()
{
	if (GEventListHead)
	{
		return { GEventListHead };
	}
	if (GNewEventList)
	{
		return { GNewEventList };
	}
	return {};
}
FEventNode::FIter FEventNode::ReadNew()
{
	FEventNode* EventList = AtomicExchangeAcquire(&GNewEventList, (FEventNode*)nullptr);
	if (EventList == nullptr)
	{
		return {};
	}
	if (GEventListHead == nullptr)
	{
		GEventListHead = EventList;
	}
	else
	{
		GEventListTail->Next = EventList;
	}
	return { EventList };
}
uint32 FEventNode::Initialize(const FEventInfo* InInfo)
{
	if (Uid != 0)
	{
		return Uid;
	}
	Writer_InternalInitialize();
	static uint32 volatile EventUidCounter; // = 0;
	uint32 NewUid = AtomicAddRelaxed(&EventUidCounter, 1u) + EKnownEventUids::User;
	if (NewUid >= uint32(EKnownEventUids::Max))
	{
		return Uid = EKnownEventUids::Invalid;
	}
	uint32 UidFlags = 0;
	if (NewUid >= (1 << (8 - EKnownEventUids::_UidShift)))
	{
		UidFlags |= EKnownEventUids::Flag_TwoByteUid;
	}
	NewUid <<= EKnownEventUids::_UidShift;
	NewUid |= UidFlags;
	Info = InInfo;
	Uid = uint16(NewUid);
	for (;; PlatformYield())
	{
		Next = AtomicLoadRelaxed(&GNewEventList);
		if (AtomicCompareExchangeRelease(&GNewEventList, this, Next))
		{
			break;
		}
	}
	return Uid;
}
void FEventNode::Describe() const
{
	const FLiteralName& LoggerName = Info->LoggerName;
	const FLiteralName& EventName = Info->EventName;
	const uint32 DefinitionIdFieldIdx = Info->FieldCount - ((Info->Flags & FEventInfo::DefinitionBits) ? 1 : 0);
	uint32 NamesSize = LoggerName.Length + EventName.Length;
	for (uint32 i = 0; i < Info->FieldCount; ++i)
	{
		NamesSize += Info->Fields[i].NameSize;
	}
	uint32 EventSize = sizeof(FNewEventEvent);
	EventSize += sizeof(FNewEventEvent::Fields[0]) * Info->FieldCount;
	EventSize += NamesSize;
	EventSize = (EventSize + 1) & ~1; // align to 2 to keep UBSAN happy
	FLogScope LogScope = FLogScope::EnterImpl<FEventInfo::Flag_NoSync>(0, EventSize + sizeof(uint16));
	auto* Ptr = (uint16*)(LogScope.GetPointer());
	Ptr[-1] = EKnownEventUids::NewEvent; // Make event look like an important one. Ideally they are sent
	Ptr[ 0] = uint16(EventSize);		 // as important and not Writer_DescribeEvents()'s redirected buf.
	auto& Event = *(FNewEventEvent*)(Ptr + 1);
	Event.EventUid = uint16(Uid) >> EKnownEventUids::_UidShift;
	Event.LoggerNameSize = LoggerName.Length;
	Event.EventNameSize = EventName.Length;
	Event.Flags = 0;
	const uint32 Flags = Info->Flags;
	if (Flags & FEventInfo::Flag_Important)		Event.Flags |= uint8(EEventFlags::Important);
	if (Flags & FEventInfo::Flag_MaybeHasAux)	Event.Flags |= uint8(EEventFlags::MaybeHasAux);
	if (Flags & FEventInfo::Flag_NoSync)		Event.Flags |= uint8(EEventFlags::NoSync);
	if (Flags & FEventInfo::DefinitionBits)		Event.Flags |= uint8(EEventFlags::Definition);
	Event.FieldCount = uint8(Info->FieldCount);
	for (uint32 i = 0; i < Info->FieldCount; ++i)
	{
		const FFieldDesc& Field = Info->Fields[i];
		auto& Out = Event.Fields[i];
		if (i == DefinitionIdFieldIdx)
		{
			Out.FieldType = EFieldFamily::DefinitionId;
			Out.DefinitionId.Offset = Field.ValueOffset;
			Out.DefinitionId.TypeInfo = Field.TypeInfo;
		}
		else if (Field.Reference != 0)
		{
			Out.FieldType = EFieldFamily::Reference;
			Out.Reference.Offset = Field.ValueOffset;
			Out.Reference.TypeInfo = Field.TypeInfo;
			Out.Reference.NameSize = Field.NameSize;
			Out.Reference.RefUid = uint16(Field.Reference) >> EKnownEventUids::_UidShift;
		}
		else
		{
			Out.FieldType = EFieldFamily::Regular;
			Out.Regular.Offset = Field.ValueOffset;
			Out.Regular.Size = Field.ValueSize;
			Out.Regular.TypeInfo = Field.TypeInfo;
			Out.Regular.NameSize = Field.NameSize;
		}
	}
	uint8* Cursor = (uint8*)(Event.Fields + Info->FieldCount);
	auto WriteName = [&Cursor] (const ANSICHAR* Data, uint32 Size)
	{
		memcpy(Cursor, Data, Size);
		Cursor += Size;
	};
	WriteName(LoggerName.Ptr, LoggerName.Length);
	WriteName(EventName.Ptr, EventName.Length);
	for (uint32 i = 0; i < Info->FieldCount; ++i)
	{
		const FFieldDesc& Field = Info->Fields[i];
		WriteName(Field.Name, Field.NameSize);
	}
	LogScope.Commit();
}
void FEventNode::OnConnect()
{
	if (GEventListHead == nullptr)
	{
		return;
	}
	GEventListTail->Next = AtomicExchangeAcquire(&GNewEventList, GEventListHead);
	GEventListHead = GEventListTail = nullptr;
}
} // namespace Private
} // namespace Trace
} // namespace UE
#endif // UE_TRACE_ENABLED
/* {{{1 Field.cpp */

#if UE_TRACE_ENABLED
namespace UE {
namespace Trace {
namespace Private {
template <typename CallbackType>
static void Field_WriteAuxData(uint32 Index, int32 Size, CallbackType&& Callback)
{
	static_assert(
		sizeof(Private::FWriteBuffer::Overflow) >= 1 + sizeof(FAuxHeader) + sizeof(uint8 /*AuxDataTerminal*/),
		"FWriteBuffer::Overflow is not large enough"
	);
	if (Size == 0)
	{
		return;
	}
	FWriteBuffer* Buffer = Writer_GetBuffer();
	bool bCommit = (Buffer->Cursor == Buffer->Committed);
	int32 Remaining = int32(ptrdiff_t((uint8*)Buffer - Buffer->Cursor));
	auto NextBuffer = [&Buffer, &Remaining, &bCommit] ()
	{
		if (bCommit)
		{
			AtomicStoreRelease(&(uint8* volatile&)(Buffer->Committed), Buffer->Cursor);
		}
		bCommit = true;
		Buffer = Writer_NextBuffer();
		Remaining = int32(ptrdiff_t((uint8*)Buffer - Buffer->Cursor));
	};
	if (Remaining <= 0)
	{
		NextBuffer();
	}
	while (true)
	{
		Remaining += sizeof(FWriteBuffer::Overflow);
		Remaining -= 1;						// for the aux-terminal
		Remaining -= sizeof(FAuxHeader);	// header also assume to always fit
		int32 SegmentSize = (Remaining < Size) ? Remaining : Size;
		uint32 Pack = SegmentSize << FAuxHeader::SizeShift;
		Pack |= Index << FAuxHeader::FieldShift;
		memcpy(Buffer->Cursor, &Pack, sizeof(uint32)); /* FAuxHeader::Pack */
		Buffer->Cursor[0] = uint8(EKnownEventUids::AuxData) << EKnownEventUids::_UidShift; /* FAuxHeader::Uid */
		Buffer->Cursor += sizeof(FAuxHeader);
		Callback(Buffer->Cursor, SegmentSize);
		Buffer->Cursor += SegmentSize;
		Size -= SegmentSize;
		if (Size <= 0)
		{
			break;
		}
		NextBuffer();
	}
	if (bCommit)
	{
		AtomicStoreRelease(&(uint8* volatile&)(Buffer->Committed), Buffer->Cursor);
	}
}
void Field_WriteAuxData(uint32 Index, const uint8* Data, int32 Size)
{
	auto MemcpyLambda = [&Data] (uint8* Cursor, int32 NumBytes)
	{
		memcpy(Cursor, Data, NumBytes);
		Data += NumBytes;
	};
	return Field_WriteAuxData(Index, Size, MemcpyLambda);
}
void Field_WriteStringAnsi(uint32 Index, const WIDECHAR* String, int32 Length)
{
	int32 Size = Length;
	Size &= (FAuxHeader::SizeLimit - 1);
	auto WriteLambda = [&String] (uint8* Cursor, int32 NumBytes)
	{
		for (int32 i = 0; i < NumBytes; ++i)
		{
			*Cursor = uint8(*String & 0x7f);
			Cursor++;
			String++;
		}
	};
	return Field_WriteAuxData(Index, Size, WriteLambda);
}
void Field_WriteStringAnsi(uint32 Index, const ANSICHAR* String, int32 Length)
{
	int32 Size = Length * sizeof(String[0]);
	Size &= (FAuxHeader::SizeLimit - 1); // a very crude "clamp"
	return Field_WriteAuxData(Index, (const uint8*)String, Size);
}
void Field_WriteStringWide(uint32 Index, const WIDECHAR* String, int32 Length)
{
	int32 Size = Length * sizeof(String[0]);
	Size &= (FAuxHeader::SizeLimit - 1); // (see above)
	return Field_WriteAuxData(Index, (const uint8*)String, Size);
}
} // namespace Private
} // namespace Trace
} // namespace UE
#endif // UE_TRACE_ENABLED
/* {{{1 Tail.cpp */

#if UE_TRACE_ENABLED
#include <string.h>
#include <type_traits>
#include <initializer_list>
namespace UE {
namespace Trace {
namespace Private {
static_assert(ETransport::Active == ETransport::TidPacketSync, "Tail-tracing is transport aware");
uint32		GetEncodeMaxSize(uint32);
int32		Encode(const void*, int32, void*, int32);
void*		Writer_MemoryAllocate(SIZE_T, uint32);
void		Writer_MemoryFree(void*, uint32);
void		Writer_SendData(uint32, uint8* __restrict, uint32);
void		Writer_SendDataRaw(const void*, uint32);
class FPacketRing
{
public:
	struct FRange
	{
		const void*	Data;
		uint32		Size;
	};
	void			Initialize(uint32 InSize);
	void			Shutdown();
	void			Reset();
	uint32			GetSize() const;
	bool			IsActive() const;
	FRange			GetBackPackets() const;
	FRange			GetFrontPackets() const;
	template <typename CallbackType>
	void			IterateRanges(CallbackType&& Callback);
	template <typename PacketType>
	PacketType*		Append(uint32 InSize);
	void			BackUp(uint32 InSize);
private:
	FTidPacketBase*	AppendImpl(uint32 InSize);
	uint8*			Data;
	uint32			Size;
	uint32			Cursor;
	uint32			Left;
	uint32			Right;
};
static_assert(std::is_trivial<FPacketRing>(), "FPacketRing must be trivial");
void FPacketRing::Initialize(uint32 InSize)
{
	Data = (uint8*)Writer_MemoryAllocate(InSize, 16);
	Size = InSize;
	Reset();
}
void FPacketRing::Shutdown()
{
	Writer_MemoryFree(Data, Size);
	Data = nullptr;
}
void FPacketRing::Reset()
{
	Cursor = 0;
    Left = Right = Size;
}
uint32 FPacketRing::GetSize() const
{
	return Size;
}
bool FPacketRing::IsActive() const
{
	return Data != nullptr;
}
FPacketRing::FRange FPacketRing::GetBackPackets() const
{
	return { Data + Left, Right - Left };
}
FPacketRing::FRange FPacketRing::GetFrontPackets() const
{
	return { Data, Cursor };
}
template <typename CallbackType>
void FPacketRing::IterateRanges(CallbackType&& Callback)
{
	FPacketRing::FRange Ranges[] = { GetBackPackets(), GetFrontPackets() };
	for (const auto& Range : Ranges)
	{
		if (Range.Size == 0)
		{
			continue;
		}
		Callback(Range);
	}
}
template <typename PacketType>
PacketType* FPacketRing::Append(uint32 InSize)
{
	FTidPacketBase* Ptr = AppendImpl(InSize + sizeof(PacketType));
	return static_cast<PacketType*>(Ptr);
}
void FPacketRing::BackUp(uint32 InSize)
{
	Cursor -= InSize;
}
FTidPacketBase* FPacketRing::AppendImpl(uint32 InSize)
{
	if (UNLIKELY(InSize > Size))
	{
		Reset();
		return nullptr;
	}
	uint32 NextCursor = Cursor + InSize;
	if (UNLIKELY(NextCursor > Size))
	{
		Left = 0;
		Right = Cursor;
		Cursor = 0;
		NextCursor = InSize;
	}
	while (true)
	{
		if (LIKELY(Left >= NextCursor))
		{
			break;
		}
		if (UNLIKELY(Left >= Right))
		{
			break;
		}
		const auto* TidPacket = (const FTidPacketBase*)(Data + Left);
		Left += TidPacket->PacketSize;
	}
	auto* TidPacket = (FTidPacketBase*)(Data + Cursor);
	TidPacket->PacketSize = uint16(InSize);
	Cursor = NextCursor;
	return TidPacket;
}
static FPacketRing GPacketRing; // = {};
void Writer_TailAppend(uint32 ThreadId, uint8* __restrict Data, uint32 Size)
{
	if (!GPacketRing.IsActive())
	{
		return Writer_SendData(ThreadId, Data, Size);
	}
	if (uint32(Size + sizeof(FTidPacketEncoded)) > GPacketRing.GetSize())
	{
		GPacketRing.Reset();
		return Writer_SendData(ThreadId, Data, Size);
	}
	ThreadId &= FTidPacketBase::ThreadIdMask;
	if (Size <= 384)
	{
		auto* Packet = GPacketRing.Append<FTidPacket>(Size);
		Packet->ThreadId = uint16(ThreadId);
		::memcpy(Packet->Data, Data, Size);
		Writer_SendDataRaw(Packet, Packet->PacketSize);
		return;
	}
	uint32 EncodeMaxSize = GetEncodeMaxSize(Size);
	auto* Packet = GPacketRing.Append<FTidPacketEncoded>(EncodeMaxSize);
	Packet->ThreadId = uint16(ThreadId);
	Packet->ThreadId |= FTidPacketBase::EncodedMarker;
	Packet->DecodedSize = uint16(Size);
	uint32 EncodeSize = Encode(Data, Size, Packet->Data, EncodeMaxSize);
	uint32 BackUp = EncodeMaxSize - EncodeSize;
	GPacketRing.BackUp(BackUp);
	Packet->PacketSize -= uint16(BackUp);
	Writer_SendDataRaw(Packet, Packet->PacketSize);
}
void Writer_TailOnConnect()
{
	if (!GPacketRing.IsActive())
	{
		return;
	}
	GPacketRing.IterateRanges([] (const FPacketRing::FRange& Range)
	{
		Writer_SendDataRaw(Range.Data, Range.Size);
	});
}
void Writer_InitializeTail(int32 BufferSize)
{
#if defined(STRESS_PACKET_RING)
	static void	StressRingPacket();
	StressRingPacket();
#endif
	if (BufferSize <= 0)
	{
		return;
	}
	uint32 Rounding = (1 << 10) - 1;
	BufferSize = (BufferSize + Rounding) & ~Rounding;
	if (BufferSize < (128 << 10))
	{
		BufferSize = 128 << 10;
	}
	GPacketRing.Initialize(BufferSize);
}
bool Writer_IsTailing()
{
	return GPacketRing.IsActive();
}
void Writer_ShutdownTail()
{
	GPacketRing.Shutdown();
}
#if defined(STRESS_PACKET_RING)
static void StressRingPacket()
{
	FPacketRing Ring;
	Ring.Initialize(300);
	uint32 Bits = 0x0493'0493;
	for (int32 i = 0; i < 1024; ++i)
	{
		FTidPacket* Packet = Ring.Append<FTidPacket>((Bits & 0x1f) + 6);
		Packet->ThreadId = i;
		Ring.IterateRanges([] (const FPacketRing::FRange&)
		{
			/* nop */
		});
		Bits = (Bits ^ 0xa93a'93a9) * 0x0493;
	}
	for (int32 i = 7; i < 448; i += 67)
	{
		if (auto* Packet = Ring.Append<FTidPacket>(i))
		{
			Packet->ThreadId = 0;
		}
	}
}
#endif // STRESS_PACKET_RING
} // namespace Private
} // namespace Trace
} // namespace UE
#endif // UE_TRACE_ENABLED
/*
FPacketRing ring-buffers packets. Internally the buffer is divided up into two
ranges; [0-Cursor) and [Left-Right) which are initially empty;
 0                                                                      L
 C----------------------------------------------------------------------R
A packet consists of a size and a opaque blob of data. Reading the sizes allows
one to stride through the packets.
                                                                        L
 0[SZ]==============>[SZ]=============>[SZ]=======>C--------------------R
Eventually the next packet will not fit in the buffer because the next cursor (N)
is off the buffer's end;
                                                                        L
 0[SZ]==============>[SZ]=============>[SZ]=======>[SZ]==========>C-----R
                                                                  [SZ]========>N
When this happens the 0-Cursor range is transferred to Left-Right and the 0-Cursor
range is set such that it can contain the new packet being added.
 L[SZ]==============>[SZ]=============>[SZ]=======>[SZ]==========>R-----|
 0[SZ]========>C
The two ranges now overlap so packets are then removed from Left until there is
enough space for the new packet.
 0[SZ]========>C-----L[SZ]============>[SZ]=======>[SZ]==========>R-----|
The Left-Right range has the oldest packets. Left will eventually advance to meet
Right at which point the Left-Right range becomes empty The process above repeats
as if the buffer was being filled for the first time.
*/
/* {{{1 TlsBuffer.cpp */

#if UE_TRACE_ENABLED
namespace UE {
namespace Trace {
namespace Private {
void				Writer_TailAppend(uint32, uint8* __restrict, uint32);
FWriteBuffer*		Writer_AllocateBlockFromPool();
uint32				Writer_GetThreadId();
void				Writer_FreeBlockListToPool(FWriteBuffer*, FWriteBuffer*);
extern uint64		GStartCycle;
extern FStatistics	GTraceStatistics;
UE_TRACE_EVENT_BEGIN($Trace, ThreadTiming, NoSync)
	UE_TRACE_EVENT_FIELD(uint64, BaseTimestamp)
UE_TRACE_EVENT_END()
#define T_ALIGN alignas(PLATFORM_CACHE_LINE_SIZE)
static FWriteBuffer						GNullWriteBuffer	= { {}, 0, nullptr, nullptr, (uint8*)&GNullWriteBuffer, nullptr, nullptr, 0, 0, 0 };
thread_local FWriteBuffer*				GTlsWriteBuffer		= &GNullWriteBuffer;
static FWriteBuffer* __restrict			GActiveThreadList;	// = nullptr;
T_ALIGN static FWriteBuffer* volatile	GNewThreadList;		// = nullptr;
#undef T_ALIGN
#if !IS_MONOLITHIC
TRACELOG_API FWriteBuffer* Writer_GetBuffer()
{
	return GTlsWriteBuffer;
}
#endif
static FWriteBuffer* Writer_NextBufferInternal(FWriteBuffer* CurrentBuffer)
{
	TWriteBufferRedirect<2048> TraceData;
#if !UE_BUILD_SHIPPING && !UE_BUILD_TEST
	if (CurrentBuffer->ThreadId == decltype(TraceData)::ActiveRedirection)
	{
		PLATFORM_BREAK();
	}
#endif
	FWriteBuffer* NextBuffer = Writer_AllocateBlockFromPool();
	NextBuffer->Cursor = (uint8*)NextBuffer - NextBuffer->Size;
	NextBuffer->Reaped = NextBuffer->Cursor;
	NextBuffer->EtxOffset = 0 - int32(sizeof(FWriteBuffer));
	AtomicStoreRelaxed(&(NextBuffer->NextBuffer), (FWriteBuffer*)nullptr);
	if (uint32 RedirectSize = TraceData.GetSize())
	{
		memcpy(NextBuffer->Cursor, TraceData.GetData(), RedirectSize);
		NextBuffer->Cursor += RedirectSize;
	}
	TraceData.Abandon();
	GTlsWriteBuffer = NextBuffer;
	NextBuffer->Committed = NextBuffer->Cursor;
	if (CurrentBuffer == &GNullWriteBuffer)
	{
		NextBuffer->ThreadId = uint16(Writer_GetThreadId());
		NextBuffer->PrevTimestamp = TimeGetTimestamp();
		UE_TRACE_LOG($Trace, ThreadTiming, TraceLogChannel)
			<< ThreadTiming.BaseTimestamp(NextBuffer->PrevTimestamp - GStartCycle);
		for (;; PlatformYield())
		{
			NextBuffer->NextThread = AtomicLoadRelaxed(&GNewThreadList);
			if (AtomicCompareExchangeRelease(&GNewThreadList, NextBuffer, NextBuffer->NextThread))
			{
				break;
			}
		}
	}
	else
	{
		NextBuffer->ThreadId = CurrentBuffer->ThreadId;
		NextBuffer->PrevTimestamp = CurrentBuffer->PrevTimestamp;
		AtomicStoreRelease(&(CurrentBuffer->NextBuffer), NextBuffer);
		int32 EtxOffset = int32(PTRINT((uint8*)(CurrentBuffer) - CurrentBuffer->Cursor));
		AtomicStoreRelease(&(CurrentBuffer->EtxOffset), EtxOffset);
	}
	return NextBuffer;
}
TRACELOG_API FWriteBuffer* Writer_NextBuffer()
{
	FWriteBuffer* CurrentBuffer = GTlsWriteBuffer;
	return Writer_NextBufferInternal(CurrentBuffer);
}
static bool Writer_DrainBuffer(uint32 ThreadId, FWriteBuffer* Buffer)
{
	uint8* Committed = AtomicLoadAcquire((uint8**)&Buffer->Committed);
	if (uint32 SizeToReap = uint32(Committed - Buffer->Reaped))
	{
#if TRACE_PRIVATE_STATISTICS
		GTraceStatistics.BytesTraced += SizeToReap;
#endif
		Writer_TailAppend(ThreadId, Buffer->Reaped, SizeToReap);
		Buffer->Reaped = Committed;
	}
	int32 EtxOffset = AtomicLoadAcquire(&Buffer->EtxOffset);
	return ((uint8*)Buffer - EtxOffset) > Committed;
}
void Writer_DrainBuffers()
{
	struct FRetireList
	{
		FWriteBuffer* __restrict Head = nullptr;
		FWriteBuffer* __restrict Tail = nullptr;
		void Insert(FWriteBuffer* __restrict Buffer)
		{
			AtomicStoreRelaxed(&(Buffer->NextBuffer), Head);
			Head = Buffer;
			Tail = (Tail != nullptr) ? Tail : Head;
		}
	};
	FWriteBuffer* __restrict NewThreadList = AtomicExchangeAcquire(&GNewThreadList, (FWriteBuffer*)nullptr);
	FWriteBuffer* __restrict NewThreadCursor = NewThreadList;
	NewThreadList = nullptr;
	while (NewThreadCursor != nullptr)
	{
		FWriteBuffer* __restrict NextThread = NewThreadCursor->NextThread;
		NewThreadCursor->NextThread = NewThreadList;
		NewThreadList = NewThreadCursor;
		NewThreadCursor = NextThread;
	}
	FRetireList RetireList;
	FWriteBuffer* __restrict ActiveThreadList = GActiveThreadList;
	GActiveThreadList = nullptr;
	for (FWriteBuffer* __restrict Buffer : { ActiveThreadList, NewThreadList })
	{
		for (FWriteBuffer* __restrict NextThread; Buffer != nullptr; Buffer = NextThread)
		{
			NextThread = Buffer->NextThread;
			uint32 ThreadId = Buffer->ThreadId;
			for (FWriteBuffer* __restrict NextBuffer; Buffer != nullptr; Buffer = NextBuffer)
			{
				if (Writer_DrainBuffer(ThreadId, Buffer))
				{
					break;
				}
				NextBuffer = AtomicLoadAcquire(&(Buffer->NextBuffer));
				RetireList.Insert(Buffer);
			}
			if (Buffer != nullptr)
			{
				Buffer->NextThread = GActiveThreadList;
				GActiveThreadList = Buffer;
			}
		}
	}
	if (RetireList.Head != nullptr)
	{
		Writer_FreeBlockListToPool(RetireList.Head, RetireList.Tail);
	}
}
void Writer_DrainLocalBuffers()
{
	if (GTlsWriteBuffer == &GNullWriteBuffer)
	{
		return;
	}
	const uint32 LocalThreadId = GTlsWriteBuffer->ThreadId;
	struct FRetireList
	{
		FWriteBuffer* __restrict Head = nullptr;
		FWriteBuffer* __restrict Tail = nullptr;
		void Insert(FWriteBuffer* __restrict Buffer)
		{
			AtomicStoreRelaxed(&(Buffer->NextBuffer), Head);
			Head = Buffer;
			Tail = (Tail != nullptr) ? Tail : Head;
		}
	};
	FRetireList RetireList;
	FWriteBuffer* __restrict ActiveThreadList = GActiveThreadList;
	GActiveThreadList = nullptr;
	{
		FWriteBuffer* __restrict Buffer = ActiveThreadList;
		FWriteBuffer* __restrict NextThread;
		for (; Buffer != nullptr; Buffer = NextThread)
		{
			NextThread = Buffer->NextThread;
			uint32 ThreadId = Buffer->ThreadId;
			if (ThreadId == LocalThreadId)
			{
				for (FWriteBuffer* __restrict NextBuffer; Buffer != nullptr; Buffer = NextBuffer)
				{
					if (Writer_DrainBuffer(ThreadId, Buffer))
					{
						break;
					}
					NextBuffer = AtomicLoadAcquire(&(Buffer->NextBuffer));
					RetireList.Insert(Buffer);
				}
			}
			if (Buffer != nullptr)
			{
				Buffer->NextThread = GActiveThreadList;
				GActiveThreadList = Buffer;
			}
		}
	}
	if (RetireList.Head != nullptr)
	{
		Writer_FreeBlockListToPool(RetireList.Head, RetireList.Tail);
	}
}
void Writer_EndThreadBuffer()
{
	if (GTlsWriteBuffer == &GNullWriteBuffer)
	{
		return;
	}
	int32 EtxOffset = int32(PTRINT((uint8*)GTlsWriteBuffer - GTlsWriteBuffer->Cursor));
	AtomicStoreRelaxed(&(GTlsWriteBuffer->EtxOffset), EtxOffset);
}
} // namespace Private
} // namespace Trace
} // namespace UE
#endif // UE_TRACE_ENABLED
/* {{{1 Trace.cpp */

#if UE_TRACE_ENABLED
namespace UE {
namespace Trace {
namespace Private
{
void	Writer_MemorySetHooks(AllocFunc, FreeFunc);
void	Writer_Initialize(const FInitializeDesc&);
void	Writer_WorkerCreate();
void	Writer_Shutdown();
void	Writer_Update();
bool	Writer_SendTo(const ANSICHAR*, uint32, uint32);
bool	Writer_WriteTo(const ANSICHAR*, uint32);
bool	Writer_WriteSnapshotTo(const ANSICHAR*);
bool	Writer_IsTracing();
bool	Writer_Stop();
uint32	Writer_GetThreadId();
extern FStatistics GTraceStatistics;
} // namespace Private
template <int DestSize, typename SRC_TYPE>
static uint32 ToAnsiCheap(ANSICHAR (&Dest)[DestSize], const SRC_TYPE* Src)
{
	const SRC_TYPE* Cursor = Src;
	for (ANSICHAR& Out : Dest)
	{
		Out = ANSICHAR(*Cursor++ & 0x7f);
		if (Out == '\0')
		{
			break;
		}
	}
	Dest[DestSize - 1] = '\0';
	return uint32(UPTRINT(Cursor - Src));
};
void SetMemoryHooks(AllocFunc Alloc, FreeFunc Free)
{
	Private::Writer_MemorySetHooks(Alloc, Free);
}
void Initialize(const FInitializeDesc& Desc)
{
	Private::Writer_Initialize(Desc);
	FChannel::Initialize();
}
void Shutdown()
{
	Private::Writer_Shutdown();
}
void Update()
{
	Private::Writer_Update();
}
void GetStatistics(FStatistics& Out)
{
	Out = Private::GTraceStatistics;
}
bool SendTo(const TCHAR* InHost, uint32 Port, uint16 Flags)
{
	char Host[256];
	ToAnsiCheap(Host, InHost);
	return Private::Writer_SendTo(Host, Flags, Port);
}
bool WriteTo(const TCHAR* InPath, uint16 Flags)
{
	char Path[512];
	ToAnsiCheap(Path, InPath);
	return Private::Writer_WriteTo(Path, Flags);
}
bool WriteSnapshotTo(const TCHAR* InPath)
{
	char Path[512];
	ToAnsiCheap(Path, InPath);
	return Private::Writer_WriteSnapshotTo(Path);
}
bool IsTracing()
{
	return Private::Writer_IsTracing();
}
bool Stop()
{
	return Private::Writer_Stop();
}
bool IsChannel(const TCHAR* ChannelName)
{
	ANSICHAR ChannelNameA[64];
	ToAnsiCheap(ChannelNameA, ChannelName);
	return FChannel::FindChannel(ChannelNameA) != nullptr;
}
bool ToggleChannel(const TCHAR* ChannelName, bool bEnabled)
{
	ANSICHAR ChannelNameA[64];
	ToAnsiCheap(ChannelNameA, ChannelName);
	return FChannel::Toggle(ChannelNameA, bEnabled);
}
void EnumerateChannels(ChannelIterFunc IterFunc, void* User)
{
	struct FCallbackDataWrapper
	{
		ChannelIterFunc* Func;
		void* User;
	};
	FCallbackDataWrapper Wrapper;
	Wrapper.Func = IterFunc;
	Wrapper.User = User;
	FChannel::EnumerateChannels([](const FChannelInfo& Info, void* User)
		{
			FCallbackDataWrapper* Wrapper = (FCallbackDataWrapper*)User;
			(*Wrapper).Func(Info.Name, Info.bIsEnabled, (*Wrapper).User);
			return true;
		}, &Wrapper);
}
void EnumerateChannels(ChannelIterCallback IterFunc, void* User)
{
	FChannel::EnumerateChannels(IterFunc, User);
}
void StartWorkerThread()
{
	Private::Writer_WorkerCreate();
}
UE_TRACE_CHANNEL_EXTERN(TraceLogChannel)
UE_TRACE_EVENT_BEGIN($Trace, ThreadInfo, NoSync|Important)
	UE_TRACE_EVENT_FIELD(uint32, ThreadId)
	UE_TRACE_EVENT_FIELD(uint32, SystemId)
	UE_TRACE_EVENT_FIELD(int32, SortHint)
	UE_TRACE_EVENT_FIELD(AnsiString, Name)
UE_TRACE_EVENT_END()
UE_TRACE_EVENT_BEGIN($Trace, ThreadGroupBegin, NoSync|Important)
	UE_TRACE_EVENT_FIELD(AnsiString, Name)
UE_TRACE_EVENT_END()
UE_TRACE_EVENT_BEGIN($Trace, ThreadGroupEnd, NoSync|Important)
UE_TRACE_EVENT_END()
void ThreadRegister(const TCHAR* Name, uint32 SystemId, int32 SortHint)
{
	ANSICHAR NameA[96];
	uint32 ThreadId = Private::Writer_GetThreadId();
	uint32 NameLen = ToAnsiCheap(NameA, Name);
	UE_TRACE_LOG($Trace, ThreadInfo, TraceLogChannel, NameLen * sizeof(ANSICHAR))
		<< ThreadInfo.ThreadId(ThreadId)
		<< ThreadInfo.SystemId(SystemId)
		<< ThreadInfo.SortHint(SortHint)
		<< ThreadInfo.Name(NameA, NameLen);
}
void ThreadGroupBegin(const TCHAR* Name)
{
	ANSICHAR NameA[96];
	uint32 NameLen = ToAnsiCheap(NameA, Name);
	UE_TRACE_LOG($Trace, ThreadGroupBegin, TraceLogChannel, NameLen * sizeof(ANSICHAR))
		<< ThreadGroupBegin.Name(Name, NameLen);
}
void ThreadGroupEnd()
{
	UE_TRACE_LOG($Trace, ThreadGroupEnd, TraceLogChannel);
}
} // namespace Trace
} // namespace UE
#else
TRACELOG_API int TraceLogExportedSymbol = 0;
#endif // UE_TRACE_ENABLED
/* {{{1 Writer.cpp */

#if UE_TRACE_ENABLED
#include <limits.h>
#include <stdlib.h>
#if PLATFORM_WINDOWS
#	define TRACE_PRIVATE_STOMP 0 // 1=overflow, 2=underflow
#	if TRACE_PRIVATE_STOMP
#	endif
#else
#	define TRACE_PRIVATE_STOMP 0
#endif
#ifndef TRACE_PRIVATE_BUFFER_SEND
#	define TRACE_PRIVATE_BUFFER_SEND 0
#endif
namespace UE {
namespace Trace {
namespace Private {
int32			Encode(const void*, int32, void*, int32);
void			Writer_SendData(uint32, uint8* __restrict, uint32);
void			Writer_InitializeTail(int32);
void			Writer_ShutdownTail();
void			Writer_TailOnConnect();
void			Writer_InitializeSharedBuffers();
void			Writer_ShutdownSharedBuffers();
void			Writer_UpdateSharedBuffers();
void			Writer_InitializeCache();
void			Writer_ShutdownCache();
void			Writer_CacheOnConnect();
void			Writer_CallbackOnConnect();
void			Writer_InitializePool();
void			Writer_ShutdownPool();
void			Writer_DrainBuffers();
void			Writer_DrainLocalBuffers();
void			Writer_EndThreadBuffer();
uint32			Writer_GetControlPort();
void			Writer_UpdateControl();
void			Writer_InitializeControl();
void			Writer_ShutdownControl();
bool			Writer_IsTailing();
static bool		Writer_SessionPrologue();
void			Writer_FreeBlockListToPool(FWriteBuffer*, FWriteBuffer*);
UE_TRACE_EVENT_BEGIN($Trace, NewTrace, Important|NoSync)
	UE_TRACE_EVENT_FIELD(uint64, StartCycle)
	UE_TRACE_EVENT_FIELD(uint64, CycleFrequency)
	UE_TRACE_EVENT_FIELD(uint16, Endian)
	UE_TRACE_EVENT_FIELD(uint8, PointerSize)
UE_TRACE_EVENT_END()
static volatile bool			GInitialized;		// = false;
FStatistics						GTraceStatistics;	// = {};
uint64							GStartCycle;		// = 0;
TRACELOG_API uint32 volatile	GLogSerial;			// = 0;
static uint32					GUpdateCounter;		// = 0;
#if !defined(UE_TRACE_USE_TLS_CONTEXT_OBJECT)
#	define UE_TRACE_USE_TLS_CONTEXT_OBJECT 1
#endif
#if UE_TRACE_USE_TLS_CONTEXT_OBJECT
struct FWriteTlsContext
{
				~FWriteTlsContext();
	uint32		GetThreadId();
private:
	uint32		ThreadId = 0;
};
FWriteTlsContext::~FWriteTlsContext()
{
	if (AtomicLoadRelaxed(&GInitialized))
	{
		Writer_EndThreadBuffer();
	}
}
uint32 FWriteTlsContext::GetThreadId()
{
	if (ThreadId)
	{
		return ThreadId;
	}
	static uint32 volatile Counter;
	ThreadId = AtomicAddRelaxed(&Counter, 1u) + ETransportTid::Bias;
	return ThreadId;
}
thread_local FWriteTlsContext	GTlsContext;
uint32 Writer_GetThreadId()
{
	return GTlsContext.GetThreadId();
}
#else // UE_TRACE_USE_TLS_CONTEXT_OBJECT
void ThreadOnThreadExit(void (*)());
uint32 Writer_GetThreadId()
{
	static thread_local uint32 ThreadId;
	if (ThreadId)
	{
		return ThreadId;
	}
	ThreadOnThreadExit([] () { Writer_EndThreadBuffer(); });
	static uint32 volatile Counter;
	ThreadId = AtomicAddRelaxed(&Counter, 1u) + ETransportTid::Bias;
	return ThreadId;
}
#endif // UE_TRACE_USE_TLS_CONTEXT_OBJECT
void*			(*AllocHook)(SIZE_T, uint32);			// = nullptr
void			(*FreeHook)(void*, SIZE_T);				// = nullptr
void Writer_MemorySetHooks(decltype(AllocHook) Alloc, decltype(FreeHook) Free)
{
	AllocHook = Alloc;
	FreeHook = Free;
}
void* Writer_MemoryAllocate(SIZE_T Size, uint32 Alignment)
{
	void* Ret = nullptr;
#if TRACE_PRIVATE_STOMP
	static uint8* Base;
	if (Base == nullptr)
	{
		Base = (uint8*)VirtualAlloc(0, 1ull << 40, MEM_RESERVE, PAGE_READWRITE);
	}
	static SIZE_T PageSize = 4096;
	Base += PageSize;
	uint8* NextBase = Base + ((PageSize - 1 + Size) & ~(PageSize - 1));
	VirtualAlloc(Base, SIZE_T(NextBase - Base), MEM_COMMIT, PAGE_READWRITE);
#if TRACE_PRIVATE_STOMP == 1
	Ret = NextBase - Size;
#elif TRACE_PRIVATE_STOMP == 2
	Ret = Base;
#endif
	Base = NextBase;
#else // TRACE_PRIVATE_STOMP
	if (AllocHook != nullptr)
	{
		Ret = AllocHook(Size, Alignment);
	}
	else
	{
#if defined(_MSC_VER)
		Ret = _aligned_malloc(Size, Alignment);
#elif (defined(__ANDROID_API__) && __ANDROID_API__ < 28) || defined(__APPLE__)
		posix_memalign(&Ret, Alignment, Size);
#else
		Ret = aligned_alloc(Alignment, Size);
#endif
	}
#endif // TRACE_PRIVATE_STOMP
#if TRACE_PRIVATE_STATISTICS
	AtomicAddRelaxed(&GTraceStatistics.MemoryUsed, uint64(Size));
#endif
	return Ret;
}
void Writer_MemoryFree(void* Address, uint32 Size)
{
#if TRACE_PRIVATE_STOMP
	if (Address == nullptr)
	{
		return;
	}
	*(uint8*)Address = 0xfe;
	MEMORY_BASIC_INFORMATION MemInfo;
	VirtualQuery(Address, &MemInfo, sizeof(MemInfo));
	DWORD Unused;
	VirtualProtect(MemInfo.BaseAddress, MemInfo.RegionSize, PAGE_READONLY, &Unused);
#else // TRACE_PRIVATE_STOMP
	if (FreeHook != nullptr)
	{
		FreeHook(Address, Size);
	}
	else
	{
#if defined(_MSC_VER)
		_aligned_free(Address);
#else
		free(Address);
#endif
	}
#endif // TRACE_PRIVATE_STOMP
#if TRACE_PRIVATE_STATISTICS
	AtomicAddRelaxed(&GTraceStatistics.MemoryUsed, uint64(-int64(Size)));
#endif
}
static UPTRINT					GDataHandle;		// = 0
static volatile UPTRINT			GPendingDataHandle;	// = 0
#if TRACE_PRIVATE_BUFFER_SEND
static const SIZE_T GSendBufferSize = 1 << 20; // 1Mb
uint8* GSendBuffer; // = nullptr;
uint8* GSendBufferCursor; // = nullptr;
static bool Writer_FlushSendBuffer()
{
	if( GSendBufferCursor > GSendBuffer )
	{
		if (!IoWrite(GDataHandle, GSendBuffer, GSendBufferCursor - GSendBuffer))
		{
			IoClose(GDataHandle);
			GDataHandle = 0;
			return false;
		}
		GSendBufferCursor = GSendBuffer;
	}
	return true;
}
#else
static bool Writer_FlushSendBuffer() { return true; }
#endif
static void Writer_SendDataImpl(const void* Data, uint32 Size)
{
#if TRACE_PRIVATE_STATISTICS
	GTraceStatistics.BytesSent += Size;
#endif
#if TRACE_PRIVATE_BUFFER_SEND
	if (GSendBufferCursor + Size > GSendBuffer + GSendBufferSize)
	{
		if (!Writer_FlushSendBuffer())
		{
			return;
		}
	}
	if (Size > GSendBufferSize)
	{
		if (!IoWrite(GDataHandle, Data, Size))
		{
			IoClose(GDataHandle);
			GDataHandle = 0;
		}
	}
	else
	{
		memcpy(GSendBufferCursor, Data, Size);
		GSendBufferCursor += Size;
	}
#else
	if (!IoWrite(GDataHandle, Data, Size))
	{
		IoClose(GDataHandle);
		GDataHandle = 0;
	}
#endif
}
void Writer_SendDataRaw(const void* Data, uint32 Size)
{
	if (!GDataHandle)
	{
		return;
	}
	Writer_SendDataImpl(Data, Size);
}
void Writer_SendData(uint32 ThreadId, uint8* __restrict Data, uint32 Size)
{
	static_assert(ETransport::Active == ETransport::TidPacketSync, "Active should be set to what the compiled code uses. It is used to track places that assume transport packet format");
	if (!GDataHandle)
	{
		return;
	}
	if (Size <= 384)
	{
		Data -= sizeof(FTidPacket);
		Size += sizeof(FTidPacket);
		auto* Packet = (FTidPacket*)Data;
		Packet->ThreadId = uint16(ThreadId & FTidPacketBase::ThreadIdMask);
		Packet->PacketSize = uint16(Size);
		Writer_SendDataImpl(Data, Size);
		return;
	}
	TTidPacketEncoded<8192 + 64> Packet;
	Packet.ThreadId = FTidPacketBase::EncodedMarker;
	Packet.ThreadId |= uint16(ThreadId & FTidPacketBase::ThreadIdMask);
	Packet.DecodedSize = uint16(Size);
	Packet.PacketSize = uint16(Encode(Data, Packet.DecodedSize, Packet.Data, sizeof(Packet.Data)));
	Packet.PacketSize += sizeof(FTidPacketEncoded);
	Writer_SendDataImpl(&Packet, Packet.PacketSize);
}
static void Writer_DescribeEvents(FEventNode::FIter Iter)
{
	TWriteBufferRedirect<4096> TraceData;
	while (const FEventNode* Event = Iter.GetNext())
	{
		Event->Describe();
		if (TraceData.GetSize() >= (TraceData.GetCapacity() - 512))
		{
			Writer_SendData(ETransportTid::Events, TraceData.GetData(), TraceData.GetSize());
			TraceData.Reset();
		}
	}
	if (TraceData.GetSize())
	{
		Writer_SendData(ETransportTid::Events, TraceData.GetData(), TraceData.GetSize());
	}
}
static void Writer_AnnounceChannels()
{
	FChannel::Iter Iter = FChannel::ReadNew();
	while (const FChannel* Channel = Iter.GetNext())
	{
		Channel->Announce();
	}
}
static void Writer_DescribeAnnounce()
{
	if (!GDataHandle)
	{
		return;
	}
	Writer_AnnounceChannels();
	Writer_DescribeEvents(FEventNode::ReadNew());
}
static int8			GSyncPacketCountdown;	// = 0
static const int8	GNumSyncPackets			= 3;
static OnConnectFunc*	GOnConnection = nullptr;
static void Writer_SendSync()
{
	if (GSyncPacketCountdown <= 0)
	{
		return;
	}
	FTidPacketBase SyncPacket = { sizeof(SyncPacket), ETransportTid::Sync };
	Writer_SendDataImpl(&SyncPacket, sizeof(SyncPacket));
	--GSyncPacketCountdown;
}
static void Writer_Close()
{
	if (GDataHandle)
	{
		Writer_FlushSendBuffer();
		IoClose(GDataHandle);
	}
	GDataHandle = 0;
}
static bool Writer_UpdateConnection()
{
	UPTRINT PendingDataHandle = AtomicLoadRelaxed(&GPendingDataHandle);
	if (!PendingDataHandle)
	{
		return false;
	}
	static int32 CloseInertia = 0;
	if (PendingDataHandle == ~UPTRINT(0))
	{
		if (CloseInertia <= 0)
			CloseInertia = 2;
		--CloseInertia;
		if (CloseInertia <= 0)
		{
			Writer_Close();
			AtomicStoreRelaxed(&GPendingDataHandle, UPTRINT(0));
		}
		return true;
	}
	AtomicStoreRelaxed(&GPendingDataHandle, UPTRINT(0));
	uint32 SendFlags = uint32(PendingDataHandle >> 48ull);
	PendingDataHandle &= 0x0000'ffff'ffff'ffffull;
	if (GDataHandle)
	{
		IoClose(PendingDataHandle);
		return false;
	}
	GDataHandle = PendingDataHandle;
	if (!Writer_SessionPrologue())
	{
		return false;
	}
	GTraceStatistics.BytesSent = 0;
	GTraceStatistics.BytesTraced = 0;
	FEventNode::OnConnect();
	Writer_DescribeEvents(FEventNode::ReadNew());
	Writer_CacheOnConnect();
	Writer_CallbackOnConnect();
	if ((SendFlags & FSendFlags::ExcludeTail) == 0)
	{
		Writer_TailOnConnect();
	}
	GSyncPacketCountdown = GNumSyncPackets;
	return true;
}
static bool Writer_SessionPrologue()
{
	if (!GDataHandle)
	{
		return false;
	}
#if TRACE_PRIVATE_BUFFER_SEND
	if (!GSendBuffer)
	{
		GSendBuffer = static_cast<uint8*>(Writer_MemoryAllocate(GSendBufferSize, 16));
	}
	GSendBufferCursor = GSendBuffer;
#endif
#define UE_TRACE_ROTATE 1
	struct FHandshake
	{
		uint32 Magic			= '2' | ('C' << 8) | ('R' << 16) | ('T' << 24);
		uint16 MetadataSize;
		uint16 MetadataField0	= uint16(sizeof(ControlPort) | (ControlPortFieldId << 8));
		uint16 ControlPort		= uint16(Writer_GetControlPort());
#if UE_TRACE_ROTATE
		uint16 MetadataField1	= uint16(sizeof(RotationParams) | (RotatingTraceId << 8));
		uint16 RotationParams	= uint16(5 /*rotate-num*/ | (4/*50*/ /*size-MiB*/ << 8));
#endif
		enum
		{
			ControlPortFieldId	= 0,
			RotatingTraceId		= 1,
		};
	};
	FHandshake Handshake;
#if UE_TRACE_ROTATE
	Handshake.MetadataSize = 8;
#else
	Handshake.MetadataSize = 4;
#endif
	uint32 WriteSize = sizeof(FHandshake::Magic) + sizeof(FHandshake::MetadataSize) + Handshake.MetadataSize;
	bool bOk = IoWrite(GDataHandle, &Handshake, WriteSize);
#undef UE_TRACE_ROTATE
	const struct {
		uint8 TransportVersion	= ETransport::TidPacketSync;
		uint8 ProtocolVersion	= EProtocol::Id;
	} TransportHeader;
	bOk &= IoWrite(GDataHandle, &TransportHeader, sizeof(TransportHeader));
	if (!bOk)
	{
		IoClose(GDataHandle);
		GDataHandle = 0;
		return false;
	}
	return true;
}
void Writer_CallbackOnConnect()
{
	if (!GOnConnection)
	{
		return;
	}
	UPTRINT DataHandle = GDataHandle;
	GDataHandle = 0;
	Writer_DrainLocalBuffers();
	GDataHandle = DataHandle;
	GOnConnection();
}
static UPTRINT			GWorkerThread;		// = 0;
static volatile bool	GWorkerThreadQuit;	// = false;
static uint32			GSleepTimeInMS = 17;
static volatile uint32	GUpdateInProgress = 1;	// Don't allow updates until initialized
static void Writer_WorkerUpdateInternal()
{
	Writer_UpdateControl();
	Writer_UpdateConnection();
	Writer_DescribeAnnounce();
	Writer_UpdateSharedBuffers();
	Writer_DrainBuffers();
	Writer_SendSync();
#if TRACE_PRIVATE_BUFFER_SEND
	const uint32 FlushSendBufferCadenceMask = 8-1; // Flush every 8 calls
	if( (++GUpdateCounter & FlushSendBufferCadenceMask) == 0)
	{
		Writer_FlushSendBuffer();
	}
#endif
}
static void Writer_WorkerUpdate()
{
	if (!AtomicCompareExchangeAcquire(&GUpdateInProgress, 1u, 0u))
	{
		return;
	}
	Writer_WorkerUpdateInternal();
	AtomicExchangeRelease(&GUpdateInProgress, 0u);
}
static void Writer_WorkerThread()
{
	ThreadRegister(TEXT("Trace"), 0, INT_MAX);
	while (!AtomicLoadRelaxed(&GWorkerThreadQuit))
	{
		Writer_WorkerUpdate();
		ThreadSleep(GSleepTimeInMS);
	}
}
void Writer_WorkerCreate()
{
	if (GWorkerThread)
	{
		return;
	}
	GWorkerThread = ThreadCreate("TraceWorker", Writer_WorkerThread);
}
static void Writer_WorkerJoin()
{
	if (!GWorkerThread)
	{
		return;
	}
	AtomicStoreRelaxed(&GWorkerThreadQuit, true);
	ThreadJoin(GWorkerThread);
	ThreadDestroy(GWorkerThread);
	Writer_WorkerUpdate();
	GWorkerThread = 0;
}
static void Writer_InternalInitializeImpl()
{
	if (AtomicLoadRelaxed(&GInitialized))
	{
		return;
	}
	GStartCycle = TimeGetTimestamp();
	Writer_InitializeSharedBuffers();
	Writer_InitializePool();
	Writer_InitializeControl();
	AtomicStoreRelaxed(&GInitialized, true);
	UE_TRACE_LOG($Trace, NewTrace, TraceLogChannel)
		<< NewTrace.StartCycle(GStartCycle)
		<< NewTrace.CycleFrequency(TimeGetFrequency())
		<< NewTrace.Endian(uint16(0x524d))
		<< NewTrace.PointerSize(uint8(sizeof(void*)));
}
static void Writer_InternalShutdown()
{
	if (!AtomicLoadRelaxed(&GInitialized))
	{
		return;
	}
	Writer_WorkerJoin();
	if (GDataHandle)
	{
		Writer_FlushSendBuffer();
		IoClose(GDataHandle);
		GDataHandle = 0;
	}
	Writer_ShutdownControl();
	Writer_ShutdownPool();
	Writer_ShutdownSharedBuffers();
	Writer_ShutdownCache();
	Writer_ShutdownTail();
#if TRACE_PRIVATE_BUFFER_SEND
	if (GSendBuffer)
	{
		Writer_MemoryFree(GSendBuffer, GSendBufferSize);
		GSendBuffer = nullptr;
		GSendBufferCursor = nullptr;
	}
#endif
	AtomicStoreRelaxed(&GInitialized, false);
}
void Writer_InternalInitialize()
{
	using namespace Private;
	if (AtomicLoadRelaxed(&GInitialized))
	{
		return;
	}
	static struct FInitializer
	{
		FInitializer()
		{
			Writer_InternalInitializeImpl();
		}
		~FInitializer()
		{
			/* We'll not shut anything down here so we can hopefully capture
			 * any subsequent events. However, we will shutdown the worker
			 * thread and leave it for something else to call update() (mem
			 * tracing at time of writing). Windows will have already done
			 * this implicitly in ExitProcess() anyway. */
			Writer_WorkerJoin();
		}
	} Initializer;
}
void Writer_Initialize(const FInitializeDesc& Desc)
{
	Writer_InitializeTail(Desc.TailSizeBytes);
	if (Desc.bUseImportantCache)
	{
		Writer_InitializeCache();
	}
	if (Desc.ThreadSleepTimeInMS != 0)
	{
		GSleepTimeInMS = Desc.ThreadSleepTimeInMS;
	}
	if (Desc.bUseWorkerThread)
	{
		Writer_WorkerCreate();
	}
	GOnConnection = Desc.OnConnectionFunc;
	AtomicStoreRelease(&GUpdateInProgress, uint32(0));
}
void Writer_Shutdown()
{
	Writer_InternalShutdown();
}
void Writer_Update()
{
	if (!GWorkerThread)
	{
		Writer_WorkerUpdate();
	}
}
static UPTRINT Writer_PackSendFlags(UPTRINT DataHandle, uint32 Flags)
{
	if (DataHandle & 0xffff'0000'0000'0000ull)
	{
		IoClose(DataHandle);
		return 0;
	}
	return DataHandle | (UPTRINT(Flags) << 48ull);
}
bool Writer_SendTo(const ANSICHAR* Host, uint32 Flags, uint32 Port)
{
	if (AtomicLoadRelaxed(&GPendingDataHandle))
	{
		return false;
	}
	Writer_InternalInitialize();
	Port = Port ? Port : 1981;
	UPTRINT DataHandle = TcpSocketConnect(Host, uint16(Port));
	if (!DataHandle)
	{
		return false;
	}
	DataHandle = Writer_PackSendFlags(DataHandle, Flags);
	if (!DataHandle)
	{
		return false;
	}
	AtomicStoreRelaxed(&GPendingDataHandle, DataHandle);
	return true;
}
bool Writer_WriteTo(const ANSICHAR* Path, uint32 Flags)
{
	if (AtomicLoadRelaxed(&GPendingDataHandle))
	{
		return false;
	}
	Writer_InternalInitialize();
	UPTRINT DataHandle = FileOpen(Path);
	if (!DataHandle)
	{
		return false;
	}
	DataHandle = Writer_PackSendFlags(DataHandle, Flags);
	if (!DataHandle)
	{
		return false;
	}
	AtomicStoreRelaxed(&GPendingDataHandle, DataHandle);
	return true;
}
struct WorkerUpdateLock
{
	WorkerUpdateLock()
	{
		CyclesPerSecond = TimeGetFrequency();
		StartSeconds = GetTime();
		while (!AtomicCompareExchangeAcquire(&GUpdateInProgress, 1u, 0u))
		{
			ThreadSleep(0);
			if (TimedOut())
			{
				break;
			}
		}
	}
	~WorkerUpdateLock()
	{
		AtomicExchangeRelease(&GUpdateInProgress, 0u);
	}
	double GetTime()
	{
		return static_cast<double>(TimeGetTimestamp()) / static_cast<double>(CyclesPerSecond);
	}
	bool TimedOut()
	{
		const double WaitTime = GetTime() - StartSeconds;
		return WaitTime > MaxWaitSeconds;
	}
	uint64 CyclesPerSecond;
	double StartSeconds;
	inline const static double MaxWaitSeconds = 1.0;
};
template <typename Type>
struct TStashGlobal
{
	TStashGlobal(Type& Global)
		: Variable(Global)
		, Stashed(Global)
	{
		Variable = {};
	}
	TStashGlobal(Type& Global, const Type& Value)
		: Variable(Global)
		, Stashed(Global)
	{
		Variable = Value;
	}
	~TStashGlobal()
	{
		Variable = Stashed;
	}
private:
	Type& Variable;
	Type Stashed;
};
bool Writer_WriteSnapshotTo(const ANSICHAR* Path)
{
	if (!Writer_IsTailing())
	{
		return false;
	}
	WorkerUpdateLock UpdateLock;
	if (UpdateLock.TimedOut())
	{
		return false;
	}
	Writer_WorkerUpdateInternal();
	{
		TStashGlobal DataHandle(GDataHandle);
		TStashGlobal PendingDataHandle(GPendingDataHandle);
		TStashGlobal SyncPacketCountdown(GSyncPacketCountdown, GNumSyncPackets);
		TStashGlobal TraceStatistics(GTraceStatistics);
		GDataHandle = FileOpen(Path);
		if (!GDataHandle || !Writer_SessionPrologue())
		{
			return false;
		}
		Writer_DescribeEvents(FEventNode::Read());
		Writer_CacheOnConnect();
		Writer_CallbackOnConnect();
		Writer_TailOnConnect();
		GSyncPacketCountdown = GNumSyncPackets;
		while (GSyncPacketCountdown > 0)
		{
			Writer_SendSync();
		}
		Writer_Close();
	}
	return true;
}
bool Writer_IsTracing()
{
	return GDataHandle != 0 || GPendingDataHandle != 0;
}
bool Writer_Stop()
{
	if (GPendingDataHandle || !GDataHandle)
	{
		return false;
	}
	AtomicStoreRelaxed(&GPendingDataHandle, ~UPTRINT(0));
	return true;
}
} // namespace Private
} // namespace Trace
} // namespace UE
#endif // UE_TRACE_ENABLED
/* {{{1 AndroidTrace.cpp */

#if UE_TRACE_ENABLED && PLATFORM_ANDROID
#include <arpa/inet.h>
#include <errno.h>
#include <fcntl.h>
#include <pthread.h>
#include <sys/mman.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>
namespace UE {
namespace Trace {
namespace Private {
UPTRINT ThreadCreate(const ANSICHAR* Name, void (*Entry)())
{
	void* (*PthreadThunk)(void*) = [] (void* Param) -> void * {
		typedef void (*EntryType)(void);
		pthread_setname_np(pthread_self(), "Trace");
		(EntryType(Param))();
		return nullptr;
	};
	pthread_t ThreadHandle;
	if (pthread_create(&ThreadHandle, nullptr, PthreadThunk, reinterpret_cast<void *>(Entry)) != 0)
	{
		return 0;
	}
	return static_cast<UPTRINT>(ThreadHandle);
}
void ThreadSleep(uint32 Milliseconds)
{
	usleep(Milliseconds * 1000U);
}
void ThreadJoin(UPTRINT Handle)
{
	pthread_join(static_cast<pthread_t>(Handle), nullptr);
}
void ThreadDestroy(UPTRINT Handle)
{
}
uint64 TimeGetFrequency()
{
	return 1000000ull;
}
uint64 TimeGetTimestamp()
{
	struct timespec TimeSpec;
	clock_gettime(CLOCK_MONOTONIC, &TimeSpec);
	return static_cast<uint64>(static_cast<uint64>(TimeSpec.tv_sec) * 1000000ULL + static_cast<uint64>(TimeSpec.tv_nsec) / 1000ULL);
}
static bool TcpSocketSetNonBlocking(int Socket, bool bNonBlocking)
{
	int Flags = fcntl(Socket, F_GETFL, 0);
	if (Flags == -1)
	{
		return false;
	}
	Flags = bNonBlocking ? (Flags|O_NONBLOCK) : (Flags & ~O_NONBLOCK);
	return fcntl(Socket, F_SETFL, Flags) >= 0;
}
UPTRINT TcpSocketConnect(const ANSICHAR* Host, uint16 Port)
{
	int Socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (Socket < 0)
	{
		return 0;
	}
	sockaddr_in SockAddr;
	SockAddr.sin_family = AF_INET;
	SockAddr.sin_addr.s_addr = inet_addr(Host);
	SockAddr.sin_port = htons(Port);
	int Result = connect(Socket, (sockaddr*)&SockAddr, sizeof(SockAddr));
	if (Result < 0)
	{
		close(Socket);
		return 0;
	}
	if (!TcpSocketSetNonBlocking(Socket, false))
	{
		close(Socket);
		return 0;
	}
	return UPTRINT(Socket + 1);
}
UPTRINT TcpSocketListen(uint16 Port)
{
	int Socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (Socket < 0)
	{
		return 0;
	}
	sockaddr_in SockAddr;
	SockAddr.sin_family = AF_INET;
	SockAddr.sin_addr.s_addr = 0;
	SockAddr.sin_port = htons(Port);
	int Result = bind(Socket, reinterpret_cast<sockaddr*>(&SockAddr), sizeof(SockAddr));
	if (Result < 0)
	{
		close(Socket);
		return 0;
	}
	Result = listen(Socket, 1);
	if (Result < 0)
	{
		close(Socket);
		return 0;
	}
	if (!TcpSocketSetNonBlocking(Socket, true))
	{
		close(Socket);
		return 0;
	}
	return UPTRINT(Socket + 1);
}
int32 TcpSocketAccept(UPTRINT Socket, UPTRINT& Out)
{
	int Inner = Socket - 1;
	Inner = accept(Inner, nullptr, nullptr);
	if (Inner < 0)
	{
		return (errno == EAGAIN || errno == EWOULDBLOCK) - 1; // 0 if would block else -1
	}
	if (!TcpSocketSetNonBlocking(Inner, false))
	{
		close(Inner);
		return 0;
	}
	Out = UPTRINT(Inner + 1);
	return 1;
}
bool TcpSocketHasData(UPTRINT Socket)
{
	int Inner = Socket - 1;
	fd_set FdSet;
	FD_ZERO(&FdSet);
	FD_SET(Inner, &FdSet);
	timeval TimeVal = {};
	return (select(Inner + 1, &FdSet, nullptr, nullptr, &TimeVal) != 0);
}
bool IoWrite(UPTRINT Handle, const void* Data, uint32 Size)
{
	int Inner = int(Handle) - 1;
	return write(Inner, Data, Size) == Size;
}
int32 IoRead(UPTRINT Handle, void* Data, uint32 Size)
{
	int Inner = int(Handle) - 1;
	return read(Inner, Data, Size);
}
void IoClose(UPTRINT Handle)
{
	int Inner = int(Handle) - 1;
	close(Inner);
}
UPTRINT FileOpen(const ANSICHAR* Path)
{
	int Flags = O_CREAT|O_WRONLY|O_TRUNC;
	int Mode = S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH;
	int Out = open(Path, Flags, Mode);
	if (Out < 0)
	{
		return 0;
	}
	return UPTRINT(Out + 1);
}
} // namespace Private
} // namespace Trace
} // namespace UE
#endif // UE_TRACE_ENABLED
/* {{{1 AppleTrace.cpp */

#if UE_TRACE_ENABLED && PLATFORM_APPLE
#include <arpa/inet.h>
#include <errno.h>
#include <fcntl.h>
#include <mach/mach.h>
#include <mach/mach_time.h>
#include <netdb.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <unistd.h>
namespace UE {
namespace Trace {
namespace Private {
UPTRINT ThreadCreate(const ANSICHAR* Name, void (*Entry)())
{
	void* (*PthreadThunk)(void*) = [] (void* Param) -> void * {
		typedef void (*EntryType)(void);
		(EntryType(Param))();
		return nullptr;
	};
	pthread_t ThreadHandle;
	if (pthread_create(&ThreadHandle, nullptr, PthreadThunk, reinterpret_cast<void *>(Entry)) != 0)
	{
		return 0;
	}
	return reinterpret_cast<UPTRINT>(ThreadHandle);
}
void ThreadSleep(uint32 Milliseconds)
{
	usleep(Milliseconds * 1000U);
}
void ThreadJoin(UPTRINT Handle)
{
	pthread_join(reinterpret_cast<pthread_t>(Handle), nullptr);
}
void ThreadDestroy(UPTRINT Handle)
{
}
uint64 TimeGetFrequency()
{
	mach_timebase_info_data_t Info;
	mach_timebase_info(&Info);
	return (uint64(1 * 1000 * 1000 * 1000) * uint64(Info.denom)) / uint64(Info.numer);
}
TRACELOG_API uint64 TimeGetTimestamp()
{
	return mach_absolute_time();
}
static bool TcpSocketSetNonBlocking(int Socket, bool bNonBlocking)
{
	int Flags = fcntl(Socket, F_GETFL, 0);
	if (Flags == -1)
	{
		return false;
	}
	Flags = bNonBlocking ? (Flags|O_NONBLOCK) : (Flags & ~O_NONBLOCK);
	return fcntl(Socket, F_SETFL, Flags) >= 0;
}
UPTRINT TcpSocketConnect(const ANSICHAR* Host, uint16 Port)
{
#if PLATFORM_MAC // We're only accepting named hosts on desktop platforms
	struct FAddrInfoPtr
	{
					~FAddrInfoPtr()	{ freeaddrinfo(Value); }
		addrinfo*	operator -> ()	{ return Value; }
		addrinfo**	operator & ()	{ return &Value; }
		addrinfo*	Value;
	};
	FAddrInfoPtr Info;
	addrinfo Hints = {};
	Hints.ai_family = AF_INET;
	Hints.ai_socktype = SOCK_STREAM;
	Hints.ai_protocol = IPPROTO_TCP;
	if (getaddrinfo(Host, nullptr, &Hints, &Info))
	{
		return 0;
	}
	if (&Info == nullptr)
	{
		return 0;
	}
	auto* SockAddr = (sockaddr_in*)Info->ai_addr;
	SockAddr->sin_port = htons(Port);
	int SockAddrSize = int(Info->ai_addrlen);
#else
	sockaddr_in SockAddrIp;
	SockAddrIp.sin_family = AF_INET;
	SockAddrIp.sin_addr.s_addr = inet_addr(Host);
	SockAddrIp.sin_port = htons(Port);
	auto* SockAddr = &SockAddrIp;
	int SockAddrSize = sizeof(SockAddrIp);
#endif
	int Socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (Socket < 0)
	{
		return 0;
	}
	int Result = connect(Socket, (sockaddr*)SockAddr, SockAddrSize);
	if (Result < 0)
	{
		close(Socket);
		return 0;
	}
	if (!TcpSocketSetNonBlocking(Socket, false))
	{
		close(Socket);
		return 0;
	}
	return UPTRINT(Socket + 1);
}
UPTRINT TcpSocketListen(uint16 Port)
{
	int Socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (Socket < 0)
	{
		return 0;
	}
	sockaddr_in SockAddr;
	SockAddr.sin_family = AF_INET;
	SockAddr.sin_addr.s_addr = 0;
	SockAddr.sin_port = htons(Port);
	int Result = bind(Socket, reinterpret_cast<sockaddr*>(&SockAddr), sizeof(SockAddr));
	if (Result < 0)
	{
		close(Socket);
		return 0;
	}
	Result = listen(Socket, 1);
	if (Result < 0)
	{
		close(Socket);
		return 0;
	}
	if (!TcpSocketSetNonBlocking(Socket, true))
	{
		close(Socket);
		return 0;
	}
	return UPTRINT(Socket + 1);
}
int32 TcpSocketAccept(UPTRINT Socket, UPTRINT& Out)
{
	int Inner = int(Socket - 1);
	Inner = accept(Inner, nullptr, nullptr);
	if (Inner < 0)
	{
		return (errno == EAGAIN || errno == EWOULDBLOCK) - 1; // 0 if would block else -1
	}
	if (!TcpSocketSetNonBlocking(Inner, false))
	{
		close(Inner);
		return 0;
	}
	Out = UPTRINT(Inner + 1);
	return 1;
}
bool TcpSocketHasData(UPTRINT Socket)
{
	int Inner = int(Socket - 1);
	fd_set FdSet;
	FD_ZERO(&FdSet);
	FD_SET(Inner, &FdSet);
	timeval TimeVal = {};
	int result = select(Inner + 1, &FdSet, nullptr, nullptr, &TimeVal);
	return ((result != 0) || ((result == -1) && (errno == ETIMEDOUT)));
}
bool IoWrite(UPTRINT Handle, const void* Data, uint32 Size)
{
	int Inner = int(Handle - 1);
	return (write(Inner, Data, Size) == Size);
}
int32 IoRead(UPTRINT Handle, void* Data, uint32 Size)
{
	int Inner = int(Handle - 1);
	return read(Inner, Data, Size);
}
void IoClose(UPTRINT Handle)
{
	int Inner = int(Handle - 1);
	close(Inner);
}
UPTRINT FileOpen(const ANSICHAR* Path)
{
	int Flags = O_CREAT|O_WRONLY|O_TRUNC|O_SHLOCK;
	int Mode = S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH;
	int Out = open(Path, Flags, Mode);
	if (Out < 0)
	{
		return 0;
	}
	return UPTRINT(Out + 1);
}
} // namespace Private
} // namespace Trace
} // namespace UE
#endif // UE_TRACE_ENABLED
/* {{{1 UnixTrace.cpp */

#if UE_TRACE_ENABLED && PLATFORM_UNIX
#include <errno.h>
#include <fcntl.h>
#include <netdb.h>
#include <pthread.h>
#include <sys/mman.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>
#if defined(_GNU_SOURCE)
	#include <sys/syscall.h>
#endif // _GNU_SOURCE
namespace UE {
namespace Trace {
namespace Private {
UPTRINT ThreadCreate(const ANSICHAR* Name, void (*Entry)())
{
	void* (*PthreadThunk)(void*) = [] (void* Param) -> void * {
		typedef void (*EntryType)(void);
		(EntryType(Param))();
		return nullptr;
	};
	pthread_t ThreadHandle;
	if (pthread_create(&ThreadHandle, nullptr, PthreadThunk, reinterpret_cast<void *>(Entry)) != 0)
	{
		return 0;
	}
	return static_cast<UPTRINT>(ThreadHandle);
}
void ThreadSleep(uint32 Milliseconds)
{
	usleep(Milliseconds * 1000U);
}
void ThreadJoin(UPTRINT Handle)
{
	pthread_join(static_cast<pthread_t>(Handle), nullptr);
}
void ThreadDestroy(UPTRINT Handle)
{
}
uint64 TimeGetFrequency()
{
	return 10000000ull;
}
TRACELOG_API uint64 TimeGetTimestamp()
{
	struct timespec TimeSpec;
	clock_gettime(CLOCK_MONOTONIC, &TimeSpec);
	return static_cast<uint64>(static_cast<uint64>(TimeSpec.tv_sec) * 10000000ULL + static_cast<uint64>(TimeSpec.tv_nsec) / 100ULL);
}
static bool TcpSocketSetNonBlocking(int Socket, bool bNonBlocking)
{
	int Flags = fcntl(Socket, F_GETFL, 0);
	if (Flags == -1)
	{
		return false;
	}
	Flags = bNonBlocking ? (Flags|O_NONBLOCK) : (Flags & ~O_NONBLOCK);
	return fcntl(Socket, F_SETFL, Flags) >= 0;
}
UPTRINT TcpSocketConnect(const ANSICHAR* Host, uint16 Port)
{
	struct FAddrInfoPtr
	{
		~FAddrInfoPtr()	
		{
			if (Value != nullptr)
			{
				freeaddrinfo(Value);
			}
		}
		addrinfo*	operator -> ()	{ return Value; }
		addrinfo**	operator & ()	{ return &Value; }
		addrinfo*	Value;
	};
	FAddrInfoPtr Info;
	addrinfo Hints = {};
	Hints.ai_family = AF_INET;
	Hints.ai_socktype = SOCK_STREAM;
	Hints.ai_protocol = IPPROTO_TCP;
	if (getaddrinfo(Host, nullptr, &Hints, &Info))
	{
		Info.Value = nullptr;
		return 0;
	}
	if (&Info == nullptr)
	{
		return 0;
	}
	auto* SockAddr = (sockaddr_in*)Info->ai_addr;
	SockAddr->sin_port = htons(Port);
	int Socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (Socket < 0)
	{
		return 0;
	}
	int Result = connect(Socket, Info->ai_addr, int(Info->ai_addrlen));
	if (Result < 0)
	{
		close(Socket);
		return 0;
	}
	if (!TcpSocketSetNonBlocking(Socket, false))
	{
		close(Socket);
		return 0;
	}
	return UPTRINT(Socket + 1);
}
UPTRINT TcpSocketListen(uint16 Port)
{
	int Socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (Socket < 0)
	{
		return 0;
	}
	sockaddr_in SockAddr;
	SockAddr.sin_family = AF_INET;
	SockAddr.sin_addr.s_addr = 0;
	SockAddr.sin_port = htons(Port);
	int Result = bind(Socket, reinterpret_cast<sockaddr*>(&SockAddr), sizeof(SockAddr));
	if (Result < 0)
	{
		close(Socket);
		return 0;
	}
	Result = listen(Socket, 1);
	if (Result < 0)
	{
		close(Socket);
		return 0;
	}
	if (!TcpSocketSetNonBlocking(Socket, true))
	{
		close(Socket);
		return 0;
	}
	return UPTRINT(Socket + 1);
}
int32 TcpSocketAccept(UPTRINT Socket, UPTRINT& Out)
{
	int Inner = Socket - 1;
	Inner = accept(Inner, nullptr, nullptr);
	if (Inner < 0)
	{
		return (errno == EAGAIN || errno == EWOULDBLOCK) - 1; // 0 if would block else -1
	}
	if (!TcpSocketSetNonBlocking(Inner, false))
	{
		close(Inner);
		return 0;
	}
	Out = UPTRINT(Inner + 1);
	return 1;
}
bool TcpSocketHasData(UPTRINT Socket)
{
	int Inner = Socket - 1;
	fd_set FdSet;
	FD_ZERO(&FdSet);
	FD_SET(Inner, &FdSet);
	timeval TimeVal = {};
	return (select(Inner + 1, &FdSet, nullptr, nullptr, &TimeVal) != 0);
}
bool IoWrite(UPTRINT Handle, const void* Data, uint32 Size)
{
	int Inner = int(Handle) - 1;
	return write(Inner, Data, Size) == Size;
}
int32 IoRead(UPTRINT Handle, void* Data, uint32 Size)
{
	int Inner = int(Handle) - 1;
	return read(Inner, Data, Size);
}
void IoClose(UPTRINT Handle)
{
	int Inner = int(Handle) - 1;
	close(Inner);
}
UPTRINT FileOpen(const ANSICHAR* Path)
{
	int Flags = O_CREAT|O_WRONLY|O_TRUNC;
	int Mode = S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH;
	int Out = open(Path, Flags, Mode);
	if (Out < 0)
	{
		return 0;
	}
	return UPTRINT(Out + 1);
}
} // namespace Private
} // namespace Trace
} // namespace UE
#endif // UE_TRACE_ENABLED
/* {{{1 WindowsTrace.cpp */

#if UE_TRACE_ENABLED && PLATFORM_WINDOWS
#	define _WINSOCK_DEPRECATED_NO_WARNINGS
#	include <winsock2.h>
#	include <ws2tcpip.h>
#	pragma comment(lib, "ws2_32.lib")
#pragma warning(push)
#pragma warning(disable : 6031) // WSAStartup() return ignore  - we're error tolerant
namespace UE {
namespace Trace {
namespace Private {
UPTRINT ThreadCreate(const ANSICHAR*, void (*Entry)())
{
	DWORD (WINAPI *WinApiThunk)(void*) = [] (void* Param) -> DWORD
	{
		typedef void (*EntryType)(void);
		(EntryType(Param))();
		return 0;
	};
	HANDLE Handle = CreateThread(nullptr, 0, WinApiThunk, (void*)Entry, 0, nullptr);
	return UPTRINT(Handle);
}
void ThreadSleep(uint32 Milliseconds)
{
	Sleep(Milliseconds);
}
void ThreadJoin(UPTRINT Handle)
{
	WaitForSingleObject(HANDLE(Handle), INFINITE);
}
void ThreadDestroy(UPTRINT Handle)
{
	CloseHandle(HANDLE(Handle));
}
uint64 TimeGetFrequency()
{
	LARGE_INTEGER Value;
	QueryPerformanceFrequency(&Value);
	return Value.QuadPart;
}
TRACELOG_API uint64 TimeGetTimestamp()
{
	LARGE_INTEGER Value;
	QueryPerformanceCounter(&Value);
	return Value.QuadPart;
}
static void TcpSocketInitialize()
{
	WSADATA WsaData;
	WSAStartup(MAKEWORD(2, 2), &WsaData);
}
static bool TcpSocketSetNonBlocking(SOCKET Socket, bool bNonBlocking)
{
	unsigned long NonBlockingMode = !!bNonBlocking;
	return ioctlsocket(Socket, FIONBIO, &NonBlockingMode) != SOCKET_ERROR;
}
UPTRINT TcpSocketConnect(const ANSICHAR* Host, uint16 Port)
{
	TcpSocketInitialize();
	struct FAddrInfoPtr
	{
					~FAddrInfoPtr()	{ freeaddrinfo(Value); }
		addrinfo*	operator -> ()	{ return Value; }
		addrinfo**	operator & ()	{ return &Value; }
		addrinfo*	Value;
	};
	FAddrInfoPtr Info;
	addrinfo Hints = {};
	Hints.ai_family = AF_INET;
	Hints.ai_socktype = SOCK_STREAM;
	Hints.ai_protocol = IPPROTO_TCP;
	if (getaddrinfo(Host, nullptr, &Hints, &Info))
	{
		return 0;
	}
	if (&Info == nullptr)
	{
		return 0;
	}
	auto* SockAddr = (sockaddr_in*)Info->ai_addr;
	SockAddr->sin_port = htons(Port);
	SOCKET Socket = WSASocket(AF_INET, SOCK_STREAM, IPPROTO_TCP, nullptr, 0, WSA_FLAG_NO_HANDLE_INHERIT);
	if (Socket == INVALID_SOCKET)
	{
		return 0;
	}
	int Result = connect(Socket, Info->ai_addr, int(Info->ai_addrlen));
	if (Result == SOCKET_ERROR)
	{
		closesocket(Socket);
		return 0;
	}
	if (!TcpSocketSetNonBlocking(Socket, 0))
	{
		closesocket(Socket);
		return 0;
	}
	return UPTRINT(Socket) + 1;
}
UPTRINT TcpSocketListen(uint16 Port)
{
	TcpSocketInitialize();
	SOCKET Socket = WSASocket(AF_INET, SOCK_STREAM, IPPROTO_TCP, nullptr, 0, WSA_FLAG_NO_HANDLE_INHERIT);
	if (Socket == INVALID_SOCKET)
	{
		return 0;
	}
	sockaddr_in SockAddr;
	SockAddr.sin_family = AF_INET;
	SockAddr.sin_addr.s_addr = 0;
	SockAddr.sin_port = htons(Port);
	int Result = bind(Socket, (SOCKADDR*)&SockAddr, sizeof(SockAddr));
	if (Result == INVALID_SOCKET)
	{
		closesocket(Socket);
		return 0;
	}
	Result = listen(Socket, 1);
	if (Result == INVALID_SOCKET)
	{
		closesocket(Socket);
		return 0;
	}
	if (!TcpSocketSetNonBlocking(Socket, 1))
	{
		closesocket(Socket);
		return 0;
	}
	return UPTRINT(Socket) + 1;
}
int32 TcpSocketAccept(UPTRINT Socket, UPTRINT& Out)
{
	SOCKET Inner = Socket - 1;
	Inner = accept(Inner, nullptr, nullptr);
	if (Inner == INVALID_SOCKET)
	{
		return (WSAGetLastError() == WSAEWOULDBLOCK) - 1; // 0 if would block else -1
	}
	if (!TcpSocketSetNonBlocking(Inner, 0))
	{
		closesocket(Inner);
		return 0;
	}
	Out = UPTRINT(Inner) + 1;
	return 1;
}
bool TcpSocketHasData(UPTRINT Socket)
{
	SOCKET Inner = Socket - 1;
	fd_set FdSet = { 1, { Inner }, };
	TIMEVAL TimeVal = {};
	return (select(0, &FdSet, nullptr, nullptr, &TimeVal) != 0);
}
bool IoWrite(UPTRINT Handle, const void* Data, uint32 Size)
{
	HANDLE Inner = HANDLE(Handle - 1);
	DWORD BytesWritten = 0;
	if (!WriteFile(Inner, (const char*)Data, Size, &BytesWritten, nullptr))
	{
		return false;
	}
	return (BytesWritten == Size);
}
int32 IoRead(UPTRINT Handle, void* Data, uint32 Size)
{
	HANDLE Inner = HANDLE(Handle - 1);
	DWORD BytesRead = 0;
	if (!ReadFile(Inner, (char*)Data, Size, &BytesRead, nullptr))
	{
		return -1;
	}
	return BytesRead;
}
void IoClose(UPTRINT Handle)
{
	HANDLE Inner = HANDLE(Handle - 1);
	CloseHandle(Inner);
}
UPTRINT FileOpen(const ANSICHAR* Path)
{
	DWORD Access = GENERIC_WRITE;
	DWORD Share = FILE_SHARE_READ;
	DWORD Disposition = CREATE_ALWAYS;
	DWORD Flags = FILE_ATTRIBUTE_NORMAL;
	HANDLE Out = CreateFileA(Path, Access, Share, nullptr, Disposition, Flags, nullptr);
	if (Out == INVALID_HANDLE_VALUE)
	{
		return 0;
	}
	return UPTRINT(Out) + 1;
}
} // namespace Private
} // namespace Trace
} // namespace UE
#pragma warning(pop)
#endif // UE_TRACE_ENABLED
/* {{{1 Cache.cpp */

#if UE_TRACE_ENABLED
#include <memory.h>
namespace UE {
namespace Trace {
namespace Private {
uint32	GetEncodeMaxSize(uint32);
int32	Encode(const void*, int32, void*, int32);
void*	Writer_MemoryAllocate(SIZE_T, uint32);
void	Writer_MemoryFree(void*, uint32);
void	Writer_SendDataRaw(const void*, uint32);
void	Writer_SendData(uint32, uint8* __restrict, uint32);
struct alignas(16) FCacheBuffer
{
	union
	{
		FCacheBuffer*	Next;
		FCacheBuffer**	TailNext;
	};
	uint32				Size;
	uint32				Remaining;
	uint32				_Unused[3];
	uint32				Underflow; // For packet header
	uint8				Data[];
};
static const uint32		GCacheBufferSize	= 64 << 10;
static const uint32		GCacheCollectorSize	= 1 << 10;
static FCacheBuffer*	GCacheCollector;	// = nullptr;
static FCacheBuffer*	GCacheActiveBuffer;	// = nullptr;
static FCacheBuffer*	GCacheHeadBuffer;	// = nullptr;
extern FStatistics		GTraceStatistics;
static FCacheBuffer* Writer_CacheCreateBuffer(uint32 Size)
{
#if TRACE_PRIVATE_STATISTICS
	GTraceStatistics.CacheAllocated += Size;
#endif
	void* Block = Writer_MemoryAllocate(sizeof(FCacheBuffer) + Size, alignof(FCacheBuffer));
	auto* Buffer = (FCacheBuffer*)Block;
	Buffer->Size = Size;
	Buffer->Remaining = Buffer->Size;
	Buffer->Next = nullptr;
	return Buffer;
}
static void Writer_CacheCommit(const FCacheBuffer* Collector)
{
	uint32 InputSize = uint32(Collector->Size - Collector->Remaining);
	uint32 EncodeMaxSize = GetEncodeMaxSize(InputSize);
	if (EncodeMaxSize + sizeof(FTidPacketEncoded) > GCacheActiveBuffer->Remaining)
	{
#if TRACE_PRIVATE_STATISTICS
		GTraceStatistics.CacheWaste += GCacheActiveBuffer->Remaining;
#endif
		*(GCacheActiveBuffer->TailNext) = GCacheActiveBuffer;
		GCacheActiveBuffer->TailNext = nullptr;
		FCacheBuffer* NewBuffer = Writer_CacheCreateBuffer(GCacheBufferSize);
		NewBuffer->TailNext = &(GCacheActiveBuffer->Next);
		GCacheActiveBuffer = NewBuffer;
	}
	uint32 Used = GCacheActiveBuffer->Size - GCacheActiveBuffer->Remaining;
	auto* Packet = (FTidPacketEncoded*)(GCacheActiveBuffer->Data + Used);
	uint32 OutputSize = Encode(Collector->Data, InputSize, Packet->Data, EncodeMaxSize);
	Packet->PacketSize = uint16(OutputSize + sizeof(FTidPacketEncoded));
	Packet->ThreadId = FTidPacketBase::EncodedMarker | uint16(ETransportTid::Importants);
	Packet->DecodedSize = uint16(InputSize);
	Used = sizeof(FTidPacketEncoded) + OutputSize;
	GCacheActiveBuffer->Remaining -= Used;
#if TRACE_PRIVATE_STATISTICS
	GTraceStatistics.CacheUsed += Used;
#endif
}
void Writer_CacheData(uint8* Data, uint32 Size)
{
	Writer_SendData(ETransportTid::Importants, Data, Size);
	if (GCacheCollector == nullptr)
	{
		return;
	}
	while (true)
	{
		uint32 StepSize = (Size < GCacheCollector->Remaining) ? Size : GCacheCollector->Remaining;
		uint32 Used = GCacheCollector->Size - GCacheCollector->Remaining;
		memcpy(GCacheCollector->Data + Used, Data, StepSize);
		GCacheCollector->Remaining -= StepSize;
		if (GCacheCollector->Remaining == 0)
		{
			Writer_CacheCommit(GCacheCollector);
			GCacheCollector->Remaining = GCacheCollector->Size;
		}
		Size -= StepSize;
		if (Size == 0)
		{
			break;
		}
		Data += StepSize;
	}
}
void Writer_CacheOnConnect()
{
	if (GCacheCollector == nullptr)
	{
		return;
	}
	for (FCacheBuffer* Buffer = GCacheHeadBuffer; Buffer != nullptr; Buffer = Buffer->Next)
	{
		uint32 Used = Buffer->Size - Buffer->Remaining;
		Writer_SendDataRaw(Buffer->Data, Used);
	}
	if (uint32 Used = GCacheActiveBuffer->Size - GCacheActiveBuffer->Remaining)
	{
		Writer_SendDataRaw(GCacheActiveBuffer->Data, Used);
	}
	if (uint32 Used = GCacheCollector->Size - GCacheCollector->Remaining)
	{
		Writer_SendData(ETransportTid::Importants, GCacheCollector->Data, Used);
	}
}
void Writer_InitializeCache()
{
	GCacheCollector = Writer_CacheCreateBuffer(GCacheCollectorSize);
	GCacheActiveBuffer = Writer_CacheCreateBuffer(GCacheBufferSize);
	GCacheActiveBuffer->TailNext = &GCacheHeadBuffer;
	static_assert(ETransport::Active == ETransport::TidPacketSync, "The important cache is transport aware");
}
void Writer_ShutdownCache()
{
	for (FCacheBuffer* Buffer = GCacheHeadBuffer; Buffer != nullptr;)
	{
		FCacheBuffer* Next = Buffer->Next;
		Writer_MemoryFree(Buffer, GCacheBufferSize);
		Buffer = Next;
	}
	Writer_MemoryFree(GCacheActiveBuffer, GCacheBufferSize);
	Writer_MemoryFree(GCacheCollector, GCacheCollectorSize);
}
} // namespace Private
} // namespace Trace
} // namespace UE
#endif // UE_TRACE_ENABLED
/* {{{1 SharedBuffer.cpp */

#if UE_TRACE_ENABLED
namespace UE {
namespace Trace {
namespace Private {
void*	Writer_MemoryAllocate(SIZE_T, uint32);
void	Writer_MemoryFree(void*, uint32);
void	Writer_CacheData(uint8*, uint32);
static FSharedBuffer	GNullSharedBuffer	= { 0, FSharedBuffer::RefInit };
FSharedBuffer* volatile GSharedBuffer		= &GNullSharedBuffer;
static FSharedBuffer*	GTailBuffer;		// = nullptr
static uint32			GTailPreSent;		// = 0
static const uint32		GBlockSize			= 1024;		// Block size must be a power of two!
extern FStatistics		GTraceStatistics;
static FSharedBuffer* Writer_CreateSharedBuffer(uint32 SizeHint=0)
{
	const uint32 OverheadSize = sizeof(FSharedBuffer) + sizeof(uint32);
	uint32 BlockSize = GBlockSize;
	if (SizeHint + OverheadSize > GBlockSize)
	{
		BlockSize += SizeHint + OverheadSize - GBlockSize;
		BlockSize += GBlockSize - 1;
		BlockSize &= ~(GBlockSize - 1);
	}
	void* Block = Writer_MemoryAllocate(BlockSize, alignof(FSharedBuffer));
	auto* Buffer = (FSharedBuffer*)(UPTRINT(Block) + BlockSize) - 1;
	Buffer->Size = uint32(UPTRINT(Buffer) - UPTRINT(Block));
	Buffer->Size -= sizeof(uint32); // to preceed event data with a small header when sending.
	Buffer->Cursor = (Buffer->Size << FSharedBuffer::CursorShift) | FSharedBuffer::RefInit;
	Buffer->Next = nullptr;
	Buffer->Final = 0;
	return Buffer;
}
FNextSharedBuffer Writer_NextSharedBuffer(FSharedBuffer* Buffer, int32 RegionStart, int32 NegSizeAndRef)
{
	FSharedBuffer* NextBuffer;
	while (true)
	{
		bool bBufferOwner = (RegionStart >= 0);
		if (LIKELY(bBufferOwner))
		{
			uint32 Size = -NegSizeAndRef >> FSharedBuffer::CursorShift;
			NextBuffer = Writer_CreateSharedBuffer(Size);
			Buffer->Next = NextBuffer;
			Buffer->Final = RegionStart >> FSharedBuffer::CursorShift;
			AtomicStoreRelease(&GSharedBuffer, NextBuffer);
		}
		else
		{
			for (;; PlatformYield())
			{
				NextBuffer = AtomicLoadAcquire(&GSharedBuffer);
				if (NextBuffer != Buffer)
				{
					break;
				}
			}
		}
		AtomicAddRelease(&(Buffer->Cursor), int32(FSharedBuffer::RefBit));
		RegionStart = AtomicAddRelaxed(&(NextBuffer->Cursor), NegSizeAndRef);
		if (LIKELY(RegionStart + NegSizeAndRef >= 0))
		{
			break;
		}
		Buffer = NextBuffer;
	}
	return { NextBuffer, RegionStart };
}
static void Writer_RetireSharedBufferImpl()
{
	uint8* Data = (uint8*)GTailBuffer - GTailBuffer->Size + GTailPreSent;
	if (auto SendSize = UPTRINT(GTailBuffer) - UPTRINT(Data) - GTailBuffer->Final)
	{
#if TRACE_PRIVATE_STATISTICS
		GTraceStatistics.BytesTraced += SendSize;
#endif
		Writer_CacheData(Data, uint32(SendSize));
	}
	FSharedBuffer* Temp = GTailBuffer->Next;
	void* Block = (uint8*)GTailBuffer - GTailBuffer->Size - sizeof(uint32);
	Writer_MemoryFree(Block, GBlockSize);
	GTailBuffer = Temp;
	GTailPreSent = 0;
}
static void Writer_RetireSharedBuffer()
{
	for (;; PlatformYield())
	{
		int32 TailCursor = AtomicLoadAcquire(&(GTailBuffer->Cursor));
		if (LIKELY(((TailCursor + 1) & FSharedBuffer::RefInit) == 0))
		{
			break;
		}
	}
	Writer_RetireSharedBufferImpl();
}
void Writer_UpdateSharedBuffers()
{
	FSharedBuffer* HeadBuffer = AtomicLoadAcquire(&GSharedBuffer);
	while (true)
	{
		if (GTailBuffer != HeadBuffer)
		{
			Writer_RetireSharedBuffer();
			continue;
		}
		int32 Cursor = AtomicLoadAcquire(&(HeadBuffer->Cursor));
		if ((Cursor + 1) & FSharedBuffer::RefInit)
		{
			continue;
		}
		Cursor = Cursor >> FSharedBuffer::CursorShift;
		if (Cursor < 0)
		{
			Writer_RetireSharedBufferImpl();
			break;
		}
		uint32 PreSentBias = HeadBuffer->Size - GTailPreSent;
		if (uint32 Sendable = PreSentBias - Cursor)
		{
			uint8* Data = (uint8*)(UPTRINT(HeadBuffer) - PreSentBias);
			Writer_CacheData(Data, Sendable);
			GTailPreSent += Sendable;
		}
		break;
	}
}
void Writer_InitializeSharedBuffers()
{
	FSharedBuffer* Buffer = Writer_CreateSharedBuffer();
	GTailBuffer = Buffer;
	GTailPreSent = 0;
	AtomicStoreRelease(&GSharedBuffer, Buffer);
}
void Writer_ShutdownSharedBuffers()
{
}
} // namespace Private
} // namespace Trace
} // namespace UE
#endif // UE_TRACE_ENABLED
#endif // TRACE_IMPLEMENT
/* {{{1 standalone_epilogue.h */

// Copyright Epic Games, Inc. All Rights Reserved.

// HEADER_UNIT_SKIP - Not included directly

// {{{1 amalgamation-tail //////////////////////////////////////////////////////

#if PLATFORM_WINDOWS
#	pragma warning(pop)
#endif

#if TRACE_UE_COMPAT_LAYER

namespace trace {

#if defined(TRACE_HAS_ANALYSIS)

inline void SerializeToCborImpl(TArray<uint8>&, const IAnalyzer::FEventData&, uint32)
{
	*(int32*)0 = 0; // unsupported
}

#endif // TRACE_HAS_ANALYSIS

} // namespace trace

#if PLATFORM_WINDOWS
#	if defined(UNICODE) || defined(_UNICODE)
#		undef TEXT
#		undef TCHAR
#		define TEXT(x)	L##x
#	endif
#endif

#undef check

#endif // TRACE_UE_COMPAT_LAYER



// {{{1 library-setup //////////////////////////////////////////////////////////

#include <string_view>

#define TRACE_EVENT_DEFINE			UE_TRACE_EVENT_DEFINE
#define TRACE_EVENT_BEGIN			UE_TRACE_EVENT_BEGIN
#define TRACE_EVENT_BEGIN_EXTERN	UE_TRACE_EVENT_BEGIN_EXTERN
#define TRACE_EVENT_FIELD			UE_TRACE_EVENT_FIELD
#define TRACE_EVENT_END				UE_TRACE_EVENT_END
#define TRACE_LOG					UE_TRACE_LOG
#define TRACE_LOG_SCOPED			UE_TRACE_LOG_SCOPED
#define TRACE_LOG_SCOPED_T			UE_TRACE_LOG_SCOPED_T
#define TRACE_CHANNEL				UE_TRACE_CHANNEL
#define TRACE_CHANNEL_EXTERN		UE_TRACE_CHANNEL_EXTERN
#define TRACE_CHANNEL_DEFINE		UE_TRACE_CHANNEL_DEFINE

namespace trace {
	using namespace UE::Trace;
	namespace detail {
		using namespace UE::Trace::Private;
	}
}

#define TRACE_PRIVATE_CONCAT_(x, y)		x##y
#define TRACE_PRIVATE_CONCAT(x, y)		TRACE_PRIVATE_CONCAT_(x, y)
#define TRACE_PRIVATE_UNIQUE_VAR(name)	TRACE_PRIVATE_CONCAT($trace_##name, __LINE__)



// {{{1 session-header /////////////////////////////////////////////////////////

namespace trace {

enum class Build
{
	Unknown,
	Debug,
	DebugGame,
	Development,
	Shipping,
	Test
};

void DescribeSession(
	const std::string_view& AppName,
	Build Variant=Build::Unknown,
	const std::string_view& CommandLine="",
	const std::string_view& BuildVersion="unknown_ver");

} // namespace trace

// {{{1 session-source /////////////////////////////////////////////////////////

#if TRACE_IMPLEMENT

namespace trace {
namespace detail {

TRACE_EVENT_BEGIN(Diagnostics, Session2, NoSync|Important)
	TRACE_EVENT_FIELD(uint8, ConfigurationType)
	TRACE_EVENT_FIELD(trace::AnsiString, AppName)
	TRACE_EVENT_FIELD(trace::AnsiString, BuildVersion)
	TRACE_EVENT_FIELD(trace::AnsiString, Platform)
	TRACE_EVENT_FIELD(trace::AnsiString, CommandLine)
TRACE_EVENT_END()

} // namespace detail

////////////////////////////////////////////////////////////////////////////////
void DescribeSession(
	const std::string_view& AppName,
	Build Variant,
	const std::string_view& CommandLine,
	const std::string_view& BuildVersion)
{
	using namespace detail;
	using namespace std::literals;

	std::string_view Platform;
#if PLATFORM_WINDOWS
	Platform = "Windows"sv;
#elif PLATFORM_UNIX
	Platform = "Linux"sv;
#elif PLATFORM_MAC
	Platform = "Mac"sv;
#else
	Platform = "Unknown"sv;
#endif

	int32 DataSize = 0;
	DataSize += int32(AppName.size());
	DataSize += int32(BuildVersion.size());
	DataSize += int32(Platform.size());
	DataSize += int32(CommandLine.size());

	TRACE_LOG(Diagnostics, Session2, true, DataSize)
		<< Session2.AppName(AppName.data(), int32(AppName.size()))
		<< Session2.BuildVersion(BuildVersion.data(), int32(BuildVersion.size()))
		<< Session2.Platform(Platform.data(), int32(Platform.size()))
		<< Session2.CommandLine(CommandLine.data(), int32(CommandLine.size()))
		<< Session2.ConfigurationType(uint8(Variant));
}

} // namespace trace

#endif // TRACE_IMPLEMENT



// {{{1 cpu-header /////////////////////////////////////////////////////////////

TRACE_CHANNEL_EXTERN(CpuChannel)

namespace trace {

enum CpuScopeFlags : int32
{
	CpuFlush = 1 << 0,
};

struct TraceCpuScope
{
			~TraceCpuScope();
	void	Enter(int32 ScopeId, int32 Flags=0);
	int32	_ScopeId = 0;
};

int32	ScopeNew(const std::string_view& Name);

class	Lane;
bool	LaneIsTracing();
Lane*	LaneNew(const std::string_view& Name);
void	LaneDelete(Lane* Handle);
void	LaneEnter(Lane* Handle, int32 ScopeId);
void	LaneLeave();

} // namespace trace

#define TRACE_CPU_SCOPE(name, ...) \
	trace::TraceCpuScope TRACE_PRIVATE_UNIQUE_VAR(cpu_scope); \
	if (CpuChannel) { \
		using namespace std::literals; \
		static int32 TRACE_PRIVATE_UNIQUE_VAR(scope_id); \
		if (0 == TRACE_PRIVATE_UNIQUE_VAR(scope_id)) \
			TRACE_PRIVATE_UNIQUE_VAR(scope_id) = trace::ScopeNew(name##sv); \
		TRACE_PRIVATE_UNIQUE_VAR(cpu_scope).Enter(TRACE_PRIVATE_UNIQUE_VAR(scope_id), ##__VA_ARGS__); \
	} \
	do {} while (0)

// {{{1 cpu-source /////////////////////////////////////////////////////////////

#if TRACE_IMPLEMENT

TRACE_CHANNEL_DEFINE(CpuChannel)

namespace trace {
namespace detail {

TRACE_EVENT_BEGIN(CpuProfiler, EventSpec, NoSync|Important)
	TRACE_EVENT_FIELD(uint32, Id)
	TRACE_EVENT_FIELD(trace::AnsiString, Name)
TRACE_EVENT_END()

TRACE_EVENT_BEGIN(CpuProfiler, NextBatchContext, NoSync)
	TRACE_EVENT_FIELD(uint16, ThreadId)
TRACE_EVENT_END()

TRACE_EVENT_BEGIN(CpuProfiler, EventBatch, NoSync)
	TRACE_EVENT_FIELD(uint8[], Data)
TRACE_EVENT_END()

////////////////////////////////////////////////////////////////////////////////
static int32 encode32_7bit(int32 value, void* __restrict out)
{
	// Calculate the number of bytes
	int32 length = 1;
	length += (value >= (1 <<  7));
	length += (value >= (1 << 14));
	length += (value >= (1 << 21));

	// Add a gap every eigth bit for the continuations
	int32 ret = value;
	ret = (ret & 0x0000'3fff) | ((ret & 0x0fff'c000) << 2);
	ret = (ret & 0x007f'007f) | ((ret & 0x3f80'3f80) << 1);

	// Set the bits indicating another byte follows
	int32 continuations = 0x0080'8080;
	continuations >>= (sizeof(value) - length) * 8;
	ret |= continuations;

	::memcpy(out, &ret, sizeof(value));

	return length;
}

////////////////////////////////////////////////////////////////////////////////
static int32 encode64_7bit(int64 value, void* __restrict out)
{
	// Calculate the output length
	uint32 length = 1;
	length += (value >= (1ll <<  7));
	length += (value >= (1ll << 14));
	length += (value >= (1ll << 21));
	length += (value >= (1ll << 28));
	length += (value >= (1ll << 35));
	length += (value >= (1ll << 42));
	length += (value >= (1ll << 49));

	// Add a gap every eigth bit for the continuations
	int64 ret = value;
	ret = (ret & 0x0000'0000'0fff'ffffull) | ((ret & 0x00ff'ffff'f000'0000ull) << 4);
	ret = (ret & 0x0000'3fff'0000'3fffull) | ((ret & 0x0fff'c000'0fff'c000ull) << 2);
	ret = (ret & 0x007f'007f'007f'007full) | ((ret & 0x3f80'3f80'3f80'3f80ull) << 1);

	// Set the bits indicating another byte follows
	int64 continuations = 0x0080'8080'8080'8080ull;
	continuations >>= (sizeof(value) - length) * 8;
	ret |= continuations;

	::memcpy(out, &ret, sizeof(value));

	return length;
}

////////////////////////////////////////////////////////////////////////////////
class ScopeBuffer
{
public:
	void		SetThreadId(uint32 Value) { ThreadIdOverride = Value; }
	void		Flush(bool Force);
	void		Enter(uint64 Timestamp, uint32 ScopeId, int32 Flag=0);
	void		Leave(uint64 Timestamp);

private:
	enum
	{
		BufferSize	= 256,
		Overflow	= 16,
		EnterLsb	= 1,
		LeaveLsb	= 0,
	};
	uint64		PrevTimestamp = 0;
	uint8*		Cursor = Buffer;
	uint32		ThreadIdOverride = 0;
	uint8		Buffer[BufferSize];
};

////////////////////////////////////////////////////////////////////////////////
void ScopeBuffer::Flush(bool Force)
{
	using namespace detail;

	if (Cursor == Buffer)
		return;

	if (!Force && (Cursor <= (Buffer + BufferSize - Overflow)))
		return;

	if (ThreadIdOverride)
		TRACE_LOG(CpuProfiler, NextBatchContext, true)
			<< NextBatchContext.ThreadId(uint16(ThreadIdOverride));

	TRACE_LOG(CpuProfiler, EventBatch, true)
		<< EventBatch.Data(Buffer, uint32(ptrdiff_t(Cursor - Buffer)));

	PrevTimestamp = 0;
	Cursor = Buffer;
}

////////////////////////////////////////////////////////////////////////////////
void ScopeBuffer::Enter(uint64 Timestamp, uint32 ScopeId, int32 Flags)
{
	Timestamp -= PrevTimestamp;
	PrevTimestamp += Timestamp;
	Cursor += encode64_7bit((Timestamp) << 1 | EnterLsb, Cursor);
	Cursor += encode32_7bit(ScopeId, Cursor);

	bool ShouldFlush = (Flags & CpuScopeFlags::CpuFlush);
	Flush(ShouldFlush);
}

////////////////////////////////////////////////////////////////////////////////
void ScopeBuffer::Leave(uint64 Timestamp)
{
	Timestamp -= PrevTimestamp;
	PrevTimestamp += Timestamp;
	Cursor += encode64_7bit((Timestamp << 1) | LeaveLsb, Cursor);

	Flush(false);
}



////////////////////////////////////////////////////////////////////////////////
class ThreadBuffer
{
public:
	static void	Enter(uint64 Timestamp, uint32 ScopeId, int32 Flags);
	static void	Leave(uint64 Timestamp);

private:
				~ThreadBuffer();
	ScopeBuffer	Inner;

	static thread_local ThreadBuffer TlsInstance;
};

thread_local ThreadBuffer ThreadBuffer::TlsInstance;

////////////////////////////////////////////////////////////////////////////////
inline void	ThreadBuffer::Enter(uint64 Timestamp, uint32 ScopeId, int32 Flags)
{
	TlsInstance.Inner.Enter(Timestamp, ScopeId, Flags);
}

////////////////////////////////////////////////////////////////////////////////
inline void	ThreadBuffer::Leave(uint64 Timestamp)
{
	TlsInstance.Inner.Leave(Timestamp);
}

////////////////////////////////////////////////////////////////////////////////
ThreadBuffer::~ThreadBuffer()
{
	Inner.Flush(true);
}



////////////////////////////////////////////////////////////////////////////////
class Lane
{
public:
				Lane(const std::string_view& Name);
				~Lane();
	void		Enter(int32 ScopeId);
	void		Leave();

private:
	ScopeBuffer	Buffer;
	uint32		Id;
};

////////////////////////////////////////////////////////////////////////////////
Lane::Lane(const std::string_view& Name)
: Id(ScopeNew(Name))
{
	Buffer.SetThreadId(Id);
}

////////////////////////////////////////////////////////////////////////////////
Lane::~Lane()
{
	Buffer.Flush(true);
}

////////////////////////////////////////////////////////////////////////////////
void Lane::Enter(int32 ScopeId)
{
	uint64 Timestamp = detail::TimeGetTimestamp();
	Buffer.Enter(Timestamp, ScopeId);
	Buffer.Flush(false);
}

////////////////////////////////////////////////////////////////////////////////
void Lane::Leave()
{
	uint64 Timestamp = detail::TimeGetTimestamp();
	Buffer.Leave(Timestamp);
	Buffer.Flush(false);
}

} // namespace detail



////////////////////////////////////////////////////////////////////////////////
int32 ScopeNew(const std::string_view& Name)
{
	using namespace detail;

	static int32 volatile NextSpecId = 1;
	int32 SpecId = AtomicAddRelaxed(&NextSpecId, 1);

	uint32 NameSize = uint32(Name.size());
	TRACE_LOG(CpuProfiler, EventSpec, true, NameSize)
		<< EventSpec.Id(uint32(SpecId))
		<< EventSpec.Name(Name.data(), NameSize);

	return SpecId;
}



////////////////////////////////////////////////////////////////////////////////
class Lane
	: public detail::Lane
{
	using detail::Lane::Lane;
};

bool	LaneIsTracing()							{ return bool(CpuChannel); }
Lane*	LaneNew(const std::string_view& Name)	{ return new Lane(Name); }
void	LaneDelete(Lane* Handle)				{ delete Handle; }
void	LaneEnter(Lane* Handle, int32 ScopeId)	{ Handle->Enter(ScopeId); }
void	LaneLeave(Lane* Handle)					{ Handle->Leave(); }



////////////////////////////////////////////////////////////////////////////////
TraceCpuScope::~TraceCpuScope()
{
	using namespace detail;

	if (!_ScopeId)
		return;

	uint64 Timestamp = TimeGetTimestamp();
	ThreadBuffer::Leave(Timestamp);
}

////////////////////////////////////////////////////////////////////////////////
void TraceCpuScope::Enter(int32 ScopeId, int32 Flags)
{
	using namespace detail;

	_ScopeId = ScopeId;
	uint64 Timestamp = TimeGetTimestamp();
	ThreadBuffer::Enter(Timestamp, ScopeId, Flags);
}

} // namespace trace

#endif // TRACE_IMPLEMENT



// {{{1 fmt-args-header ////////////////////////////////////////////////////////

namespace trace::detail {

template <typename T> concept IsIntegral	= std::is_integral<T>::value;
template <typename T> concept IsFloat		= std::is_floating_point<T>::value;

////////////////////////////////////////////////////////////////////////////////
class ArgPacker
{
public:
	template <typename... Types> ArgPacker(Types... Args);
	const uint8*	GetData() const { return Buffer; }
	uint32			GetSize() const { return uint32(ptrdiff_t(Cursor - Buffer)); }

private:
	template <typename T> struct TypeId {};
	template <typename T> requires IsIntegral<T>	struct TypeId<T> { enum { Value = 1 << 6 }; };
	template <typename T> requires IsFloat<T>		struct TypeId<T> { enum { Value = 2 << 6 }; };

	enum {
		BufferSize	= 512,
		TypeIdStr	= 3 << 6,
	};
	template <typename Type> uint32					PackValue(Type&& Value);
	template <typename Type, typename... U> void	Pack(Type&& Value, U... Next);
	void			Pack() {}
	uint8*			Cursor = Buffer;
	const uint8*	End = Buffer + BufferSize;
	uint8			Buffer[BufferSize];
};

////////////////////////////////////////////////////////////////////////////////
template <typename Type>
uint32 ArgPacker::PackValue(Type&& Value)
{
	uint32 Size = sizeof(Type);
	if (Cursor + Size > End)
		return 0;

	std::memcpy(Cursor, &Value, sizeof(Type));
	Cursor += Size;
	return TypeId<Type>::Value | sizeof(Type);
}

////////////////////////////////////////////////////////////////////////////////
template <typename... Types>
ArgPacker::ArgPacker(Types... Args)
{
	static_assert(sizeof...(Args) <= 0xff);

	int32 ArgCount = sizeof...(Args);
	if (ArgCount == 0)
		return;

	*Cursor = uint8(ArgCount);
	Cursor += ArgCount + 1;

	Pack(std::forward<Types>(Args)...);
}

////////////////////////////////////////////////////////////////////////////////
template <typename T, typename... U>
void ArgPacker::Pack(T&& Value, U... Next)
{
	uint8* TypeCursor = Buffer + Buffer[0] - sizeof...(Next);
	if ((*TypeCursor = uint8(PackValue(std::forward<T>(Value)))) != 0)
		return Pack(std::forward<U>(Next)...);

	while (++TypeCursor < Buffer + Buffer[0] + 1)
		*TypeCursor = 0;
}

} // namespace trace::detail

// {{{1 log-header /////////////////////////////////////////////////////////////

TRACE_CHANNEL_EXTERN(LogChannel)

namespace trace::detail {

////////////////////////////////////////////////////////////////////////////////
void	LogMessageImpl(int32 Id, const uint8* ParamBuffer, int32 ParamSize);
int32	LogMessageNew(const std::string_view& Format, const std::string_view& File, int32 Line);

////////////////////////////////////////////////////////////////////////////////
template <typename... Types>
void LogMessage(int32 Id, Types&&... Args)
{
	ArgPacker Packer(std::forward<Types>(Args)...);
	LogMessageImpl(Id, Packer.GetData(), Packer.GetSize());
}

} // namespace trace::detail

////////////////////////////////////////////////////////////////////////////////
#define TRACE_LOG_MESSAGE(format, ...) \
	if (LogChannel) { \
		using namespace std::literals; \
		static int32 message_id; \
		if (message_id == 0) \
			message_id = trace::detail::LogMessageNew( \
				format##sv, \
				TRACE_PRIVATE_CONCAT(__FILE__, sv), \
				__LINE__); \
		trace::detail::LogMessage(message_id, ##__VA_ARGS__); \
	} \
	do {} while (0)

// {{{1 log-source /////////////////////////////////////////////////////////////

#if TRACE_IMPLEMENT

TRACE_CHANNEL_DEFINE(LogChannel)

namespace trace::detail {

////////////////////////////////////////////////////////////////////////////////
#if 0
TRACE_EVENT_BEGIN(Logging, LogCategory, NoSync|Important)
	TRACE_EVENT_FIELD(const void*, CategoryPointer)
	TRACE_EVENT_FIELD(uint8, DefaultVerbosity)
	TRACE_EVENT_FIELD(trace::AnsiString, Name)
TRACE_EVENT_END()
#endif

TRACE_EVENT_BEGIN(Logging, LogMessageSpec, NoSync|Important)
	TRACE_EVENT_FIELD(uint32, LogPoint)
	//TRACE_EVENT_FIELD(uint16, CategoryPointer)
	TRACE_EVENT_FIELD(uint16, Line)
	TRACE_EVENT_FIELD(trace::AnsiString, FileName)
	TRACE_EVENT_FIELD(trace::AnsiString, FormatString)
TRACE_EVENT_END()

TRACE_EVENT_BEGIN(Logging, LogMessage, NoSync)
	TRACE_EVENT_FIELD(uint32, LogPoint)
	TRACE_EVENT_FIELD(uint64, Cycle)
	TRACE_EVENT_FIELD(uint8[], FormatArgs)
TRACE_EVENT_END()

////////////////////////////////////////////////////////////////////////////////
void LogMessageImpl(int32 Id, const uint8* ParamBuffer, int32 ParamSize)
{
	uint64 Timestamp = TimeGetTimestamp();

	TRACE_LOG(Logging, LogMessage, true)
		<< LogMessage.LogPoint(Id)
		<< LogMessage.Cycle(Timestamp)
		<< LogMessage.FormatArgs(ParamBuffer, ParamSize);
}

////////////////////////////////////////////////////////////////////////////////
int32 LogMessageNew(
	const std::string_view& Format,
	const std::string_view& File,
	int32 Line)
{
	static int32 volatile NextId = 1;
	int32 Id = AtomicAddRelaxed(&NextId, 1);

	int32 DataSize = 0;
	DataSize += int32(Format.size());
	DataSize += int32(File.size());

	TRACE_LOG(Logging, LogMessageSpec, true, DataSize)
		<< LogMessageSpec.LogPoint(Id)
		<< LogMessageSpec.Line(uint16(Line))
		<< LogMessageSpec.FileName(File.data(), int32(File.size()))
		<< LogMessageSpec.FormatString(Format.data(), int32(Format.size()));

	return Id;
}

} // namespace trace::detail

#endif // TRACE_IMPLEMENT



// {{{1 bookmark-header ////////////////////////////////////////////////////////

namespace trace::detail {

////////////////////////////////////////////////////////////////////////////////
void	BookmarkImpl(uint32 Id, const uint8* ParamBuffer, uint32 ParamSize);
uint32	BookmarkNew(const std::string_view& Format, const std::string_view& File, uint32 Line);

template <typename... Types>
void Bookmark(uint32 Id, Types&&... Args)
{
	ArgPacker Packer(std::forward<Types>(Args)...);
	BookmarkImpl(Id, Packer.GetData(), Packer.GetSize());
}

} // namespace trace::detail

////////////////////////////////////////////////////////////////////////////////
#define TRACE_BOOKMARK(format, ...) \
	if (LogChannel) { \
		using namespace std::literals; \
		static int32 bookmark_id; \
		if (bookmark_id == 0) \
			bookmark_id = trace::detail::BookmarkNew( \
				format##sv, \
				TRACE_PRIVATE_CONCAT(__FILE__, sv), \
				__LINE__); \
		trace::detail::Bookmark(bookmark_id, ##__VA_ARGS__); \
	} \
	do {} while (0)

// {{{1 bookmark-source ////////////////////////////////////////////////////////

#if TRACE_IMPLEMENT

namespace trace::detail {

////////////////////////////////////////////////////////////////////////////////
TRACE_EVENT_BEGIN(Misc, BookmarkSpec, NoSync|Important)
	TRACE_EVENT_FIELD(uint32, BookmarkPoint)
	TRACE_EVENT_FIELD(int32, Line)
	TRACE_EVENT_FIELD(trace::AnsiString, FormatString)
	TRACE_EVENT_FIELD(trace::AnsiString, FileName)
TRACE_EVENT_END()

TRACE_EVENT_BEGIN(Misc, Bookmark, NoSync)
	TRACE_EVENT_FIELD(uint64, Cycle)
	TRACE_EVENT_FIELD(uint32, BookmarkPoint)
	TRACE_EVENT_FIELD(uint8[], FormatArgs)
TRACE_EVENT_END()

////////////////////////////////////////////////////////////////////////////////
void BookmarkImpl(uint32 Id, const uint8* ParamBuffer, uint32 ParamSize)
{
	uint64 Timestamp = TimeGetTimestamp();

	TRACE_LOG(Misc, Bookmark, true)
		<< Bookmark.Cycle(Timestamp)
		<< Bookmark.BookmarkPoint(Id)
		<< Bookmark.FormatArgs(ParamBuffer, ParamSize);
}

////////////////////////////////////////////////////////////////////////////////
uint32 BookmarkNew(const std::string_view& Format, const std::string_view& File, uint32 Line)
{
	uint32 Id = uint32(uintptr_t(Format.data()) >> 2);

	int32 DataSize = 0;
	DataSize += int32(Format.size());
	DataSize += int32(File.size());

	TRACE_LOG(Misc, BookmarkSpec, true, DataSize)
		<< BookmarkSpec.BookmarkPoint(Id)
		<< BookmarkSpec.Line(uint16(Line))
		<< BookmarkSpec.FormatString(Format.data(), int32(Format.size()))
		<< BookmarkSpec.FileName(File.data(), int32(File.size()));

	return Id;
}

} // namespace trace::detail

#endif // TRACE_IMPLEMENT

// }}}

/* vim: set noet foldlevel=1 foldmethod=marker : */
