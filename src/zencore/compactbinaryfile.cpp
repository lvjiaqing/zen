// Copyright Epic Games, Inc. All Rights Reserved.

#include "zencore/compactbinaryfile.h"
#include "zencore/compactbinaryvalidation.h"

#include <zencore/filesystem.h>

namespace zen {

CbObjectFromFile
LoadCompactBinaryObject(const std::filesystem::path& FilePath)
{
	FileContents ObjectFile = ReadFile(FilePath);

	if (ObjectFile.ErrorCode)
	{
		throw std::system_error(ObjectFile.ErrorCode);
	}

	IoBuffer ObjectBuffer = ObjectFile.Flatten();

	if (CbValidateError Result = ValidateCompactBinary(ObjectBuffer, CbValidateMode::All); Result == CbValidateError::None)
	{
		CbObject	 Object	  = LoadCompactBinaryObject(ObjectBuffer);
		const IoHash WorkerId = IoHash::HashBuffer(ObjectBuffer);

		return {.Object = Object, .Hash = WorkerId};
	}

	return {.Hash = IoHash::Zero};
}

}  // namespace zen
