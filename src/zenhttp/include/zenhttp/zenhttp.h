// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include <zencore/zencore.h>

#ifndef ZEN_WITH_HTTPSYS
#	if ZEN_PLATFORM_WINDOWS
#		define ZEN_WITH_HTTPSYS 1
#	else
#		define ZEN_WITH_HTTPSYS 0
#	endif
#endif

#define ZENHTTP_API	 // Placeholder to allow DLL configs in the future

namespace zen {

ZENHTTP_API void zenhttp_forcelinktests();

}
