// Copyright Epic Games, Inc. All Rights Reserved.

#include "zenhttp/auth/authservice.h"

#include <zencore/compactbinarybuilder.h>
#include <zencore/string.h>
#include <zenhttp/auth/authmgr.h>

ZEN_THIRD_PARTY_INCLUDES_START
#include <json11.hpp>
ZEN_THIRD_PARTY_INCLUDES_END

namespace zen {

using namespace std::literals;

HttpAuthService::HttpAuthService(AuthMgr& AuthMgr) : m_AuthMgr(AuthMgr)
{
	m_Router.RegisterRoute(
		"oidc/refreshtoken",
		[this](HttpRouterRequest& RouterRequest) {
			HttpServerRequest& ServerRequest = RouterRequest.ServerRequest();

			const HttpContentType ContentType = ServerRequest.RequestContentType();

			if ((ContentType == HttpContentType::kUnknownContentType || ContentType == HttpContentType::kJSON) == false)
			{
				return ServerRequest.WriteResponse(HttpResponseCode::BadRequest);
			}

			const IoBuffer Body = ServerRequest.ReadPayload();

			std::string	 JsonText(reinterpret_cast<const char*>(Body.GetData()), Body.GetSize());
			std::string	 JsonError;
			json11::Json TokenInfo = json11::Json::parse(JsonText, JsonError);

			if (!JsonError.empty())
			{
				CbObjectWriter Response;
				Response << "Result"sv << false;
				Response << "Error"sv << JsonError;

				return ServerRequest.WriteResponse(HttpResponseCode::BadRequest, Response.Save());
			}

			const std::string RefreshToken = TokenInfo["RefreshToken"].string_value();
			std::string		  ProviderName = TokenInfo["ProviderName"].string_value();

			if (ProviderName.empty())
			{
				ProviderName = "Default"sv;
			}

			const bool Ok =
				m_AuthMgr.AddOpenIdToken(AuthMgr::AddOpenIdTokenParams{.ProviderName = ProviderName, .RefreshToken = RefreshToken});

			if (Ok)
			{
				ServerRequest.WriteResponse(Ok ? HttpResponseCode::OK : HttpResponseCode::BadRequest);
			}
			else
			{
				CbObjectWriter Response;
				Response << "Result"sv << false;
				Response << "Error"sv
						 << "Invalid token"sv;

				ServerRequest.WriteResponse(HttpResponseCode::BadRequest, Response.Save());
			}
		},
		HttpVerb::kPost);
}

HttpAuthService::~HttpAuthService()
{
}

const char*
HttpAuthService::BaseUri() const
{
	return "/auth/";
}

void
HttpAuthService::HandleRequest(zen::HttpServerRequest& Request)
{
	m_Router.HandleRequest(Request);
}

}  // namespace zen
