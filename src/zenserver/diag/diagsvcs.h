// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include <zencore/iobuffer.h>
#include <zenhttp/httpserver.h>

#include <filesystem>

//////////////////////////////////////////////////////////////////////////

namespace zen {

/** HTTP test endpoint

	This is intended to be used to exercise basic HTTP communication infrastructure
	which is useful for benchmarking performance of the server code and when evaluating
	network performance / diagnosing connectivity issues

  */
class HttpTestService : public HttpService
{
public:
	HttpTestService() {}
	~HttpTestService() = default;

	virtual const char* BaseUri() const override { return "/test/"; }

	virtual void HandleRequest(HttpServerRequest& Request) override
	{
		using namespace std::literals;

		auto Uri = Request.RelativeUri();

		if (Uri == "hello"sv)
		{
			Request.WriteResponse(HttpResponseCode::OK, HttpContentType::kText, u8"hello world!"sv);
		}
		else if (Uri == "1K"sv)
		{
			Request.WriteResponse(HttpResponseCode::OK, HttpContentType::kBinary, m_1k);
		}
		else if (Uri == "1M"sv)
		{
			Request.WriteResponse(HttpResponseCode::OK, HttpContentType::kBinary, m_1m);
		}
		else if (Uri == "1M_1k"sv)
		{
			std::vector<IoBuffer> Buffers;
			Buffers.reserve(1024);

			for (int i = 0; i < 1024; ++i)
			{
				Buffers.push_back(m_1k);
			}

			Request.WriteResponse(HttpResponseCode::OK, HttpContentType::kBinary, Buffers);
		}
		else if (Uri == "1G"sv)
		{
			std::vector<IoBuffer> Buffers;
			Buffers.reserve(1024);

			for (int i = 0; i < 1024; ++i)
			{
				Buffers.push_back(m_1m);
			}

			Request.WriteResponse(HttpResponseCode::OK, HttpContentType::kBinary, Buffers);
		}
		else if (Uri == "1G_1k"sv)
		{
			std::vector<IoBuffer> Buffers;
			Buffers.reserve(1024 * 1024);

			for (int i = 0; i < 1024 * 1024; ++i)
			{
				Buffers.push_back(m_1k);
			}

			Request.WriteResponse(HttpResponseCode::OK, HttpContentType::kBinary, Buffers);
		}
	}

private:
	IoBuffer m_1m{1024 * 1024};
	IoBuffer m_1k{m_1m, 0u, 1024};
};

struct HealthServiceInfo
{
	std::filesystem::path DataRoot;
	std::filesystem::path AbsLogPath;
	std::string			  HttpServerClass;
	std::string			  BuildVersion;
};

/** Health monitoring endpoint

	Thji
  */
class HttpHealthService : public HttpService
{
public:
	HttpHealthService();
	~HttpHealthService() = default;

	void SetHealthInfo(HealthServiceInfo&& Info);

	virtual const char* BaseUri() const override;
	virtual void		HandleRequest(HttpServerRequest& Request) override final;

private:
	HttpRequestRouter m_Router;
	RwLock			  m_InfoLock;
	HealthServiceInfo m_HealthInfo;
};

}  // namespace zen
