// Copyright Epic Games, Inc. All Rights Reserved.

#include <zencore/compactbinary.h>
#include <zencore/iohash.h>

#include <filesystem>

namespace zen {

struct CbObjectFromFile
{
	CbObject Object;
	IoHash	 Hash;
};

CbObjectFromFile LoadCompactBinaryObject(const std::filesystem::path& FilePath);

}  // namespace zen
