// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include <zenhttp/httpserver.h>

namespace zen {

Ref<HttpServer> CreateHttpAsioServer(bool ForceLoopback, unsigned int ThreadCount);

}  // namespace zen
