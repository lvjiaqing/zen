// Copyright Epic Games, Inc. All Rights Reserved.

#include "zencore/testing.h"
#include "zencore/logging.h"

#if ZEN_WITH_TESTS

#	include <doctest/doctest.h>

namespace zen::testing {

using namespace std::literals;

struct TestListener : public doctest::IReporter
{
	const std::string_view ColorYellow = "\033[0;33m"sv;
	const std::string_view ColorNone   = "\033[0m"sv;

	// constructor has to accept the ContextOptions by ref as a single argument
	TestListener(const doctest::ContextOptions&) {}

	void report_query(const doctest::QueryData& /*in*/) override {}

	void test_run_start() override {}

	void test_run_end(const doctest::TestRunStats& /*in*/) override {}

	void test_case_start(const doctest::TestCaseData& in) override
	{
		Current = &in;
		ZEN_CONSOLE("{}======== TEST_CASE: {:<50} ========{}", ColorYellow, Current->m_name, ColorNone);
	}

	// called when a test case is reentered because of unfinished subcases
	void test_case_reenter(const doctest::TestCaseData& /*in*/) override
	{
		ZEN_CONSOLE("{}-------------------------------------------------------------------------------{}", ColorYellow, ColorNone);
	}

	void test_case_end(const doctest::CurrentTestCaseStats& /*in*/) override { Current = nullptr; }

	void test_case_exception(const doctest::TestCaseException& /*in*/) override {}

	void subcase_start(const doctest::SubcaseSignature& in) override
	{
		ZEN_CONSOLE("{}-------- SUBCASE:   {:<50} --------{}",
					ColorYellow,
					fmt::format("{}/{}", Current->m_name, in.m_name.c_str()),
					ColorNone);
	}

	void subcase_end() override {}

	void log_assert(const doctest::AssertData& /*in*/) override {}

	void log_message(const doctest::MessageData& /*in*/) override {}

	void test_case_skipped(const doctest::TestCaseData& /*in*/) override {}

	const doctest::TestCaseData* Current = nullptr;
};

struct TestRunner::Impl
{
	Impl() { REGISTER_LISTENER("ZenTestListener", 1, TestListener); }
	doctest::Context Session;
};

TestRunner::TestRunner()
{
	m_Impl = std::make_unique<Impl>();
}

TestRunner::~TestRunner()
{
}

int
TestRunner::ApplyCommandLine(int argc, char const* const* argv)
{
	m_Impl->Session.applyCommandLine(argc, argv);

	for (int i = 1; i < argc; ++i)
	{
		if (argv[i] == "--debug"sv)
		{
			zen::logging::SetLogLevel(zen::logging::level::Debug);
		}
		else if (argv[i] == "--verbose"sv)
		{
			zen::logging::SetLogLevel(zen::logging::level::Trace);
		}
	}

	return 0;
}

int
TestRunner::Run()
{
	return m_Impl->Session.run();
}

}  // namespace zen::testing

#endif	// ZEN_WITH_TESTS
