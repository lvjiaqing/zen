# Github action runner setup

## Windows

A Hyper-V "Quick Create" of a `Windows 11 dev environment` can give you a head start when setting up an instance for testing purposes. The following
instructions should be sufficient to set up a completely clean Windows install.

* For Windows Server, some gymnastics are required to get `winget` installed
  * run `irm winget.pro | iex` in an Administrator Powershell window to automate this process (see https://github.com/asheroto/winget-install for other options)
* Install git using `winget install git.git` (alternate: https://git-scm.com/download/win)
* Install VS Pro using winget by following the relevant instructions in [README.md](/README.md) (note: ONLY Visual Studio)
* Enable scripting using `Set-ExecutionPolicy -ExecutionPolicy Unrestricted` in an admin PowerShell instance
  * In the same admin PowerShell instance, disable firewall with `netsh advfirewall set currentprofile state off` to allow tests to run without firewall popups (TODO: research better options here as this does not seem ideal)
* See https://docs.github.com/en/actions/hosting-your-own-runners/managing-self-hosted-runners/adding-self-hosted-runners for basic instructions on setting up the runner
* Remember to close the Terminal window after installing the above and open a new window to invoke `run.cmd`

## Linux

* Install docker (`sudo apt-get install docker.io`)
  * Make sure runner user is allowed to use docker (add user to docker group using `sudo usermod -aG docker ${USER}` note that you will need to log out before running the runner script as this change is not applied directly)
* Install development tools and utilities (`sudo apt install git build-essential curl zip unzip tar pkg-config`)
