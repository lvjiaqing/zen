// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "remoteprojectstore.h"

namespace zen {

struct FileRemoteStoreOptions : RemoteStoreOptions
{
	std::filesystem::path FolderPath;
	std::string			  Name;
	std::string			  OptionalBaseName;
	bool				  ForceDisableBlocks	= false;
	bool				  ForceEnableTempBlocks = false;
};

std::shared_ptr<RemoteProjectStore> CreateFileRemoteStore(const FileRemoteStoreOptions& Options);

}  // namespace zen
