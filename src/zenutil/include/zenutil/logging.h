// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include <zencore/logging.h>

#include <filesystem>
#include <memory>
#include <string>

namespace spdlog::sinks {
class sink;
}

namespace spdlog {
using sink_ptr = std::shared_ptr<sinks::sink>;
}

//////////////////////////////////////////////////////////////////////////
//
// Logging utilities
//
// These functions extend the basic logging functionality by setting up
// console and file logging, as well as colored output where available,
// for sharing across different executables
//

namespace zen {

struct LoggingOptions
{
	bool				  IsDebug		  = false;
	bool				  IsVerbose		  = false;
	bool				  IsTest		  = false;
	bool				  NoConsoleOutput = false;
	std::filesystem::path AbsLogFile;  // Absolute path to main log file
	std::string			  LogId;
};

void BeginInitializeLogging(const LoggingOptions& LoggingOptions);
void FinishInitializeLogging(const LoggingOptions& LoggingOptions);

void InitializeLogging(const LoggingOptions& LoggingOptions);
void ShutdownLogging();

spdlog::sink_ptr GetFileSink();

}  // namespace zen
