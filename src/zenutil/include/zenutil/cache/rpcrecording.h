// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include <zencore/compositebuffer.h>
#include <zencore/iobuffer.h>
#include <zencore/uid.h>

#include <compare>

namespace zen::cache {

struct RecordedRequestInfo
{
	ZenContentType ContentType;
	ZenContentType AcceptType;
	Oid			   SessionId;

	inline std::strong_ordering		 operator<=>(const RecordedRequestInfo& Rhs) const = default;
	static const RecordedRequestInfo NullRequest;
};

class IRpcRequestRecorder
{
public:
	virtual ~IRpcRequestRecorder() {}
	virtual void RecordRequest(const RecordedRequestInfo& RequestInfo, const IoBuffer& RequestBuffer) = 0;
};

class IRpcRequestReplayer
{
public:
	virtual ~IRpcRequestReplayer() {}
	virtual uint64_t			GetRequestCount() const								   = 0;
	virtual RecordedRequestInfo GetRequest(uint64_t RequestIndex, IoBuffer& OutBuffer) = 0;
};

std::unique_ptr<cache::IRpcRequestRecorder> MakeDiskRequestRecorder(const std::filesystem::path& BasePath);
std::unique_ptr<cache::IRpcRequestReplayer> MakeDiskRequestReplayer(const std::filesystem::path& BasePath, bool InMemory);

void rpcrecord_forcelink();

}  // namespace zen::cache
