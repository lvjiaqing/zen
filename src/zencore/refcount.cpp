// Copyright Epic Games, Inc. All Rights Reserved.

#include <zenbase/refcount.h>

#include <zencore/testing.h>

#include <functional>

namespace zen {

//////////////////////////////////////////////////////////////////////////
//
// Testing related code follows...
//

#if ZEN_WITH_TESTS

struct TestRefClass : public RefCounted
{
	~TestRefClass()
	{
		if (OnDestroy)
			OnDestroy();
	}

	using RefCounted::RefCount;

	std::function<void()> OnDestroy;
};

void
refcount_forcelink()
{
}

TEST_CASE("RefPtr")
{
	RefPtr<TestRefClass> Ref;
	Ref = new TestRefClass;

	bool IsDestroyed = false;
	Ref->OnDestroy	 = [&] { IsDestroyed = true; };

	CHECK(IsDestroyed == false);
	CHECK(Ref->RefCount() == 1);

	RefPtr<TestRefClass> Ref2;
	Ref2 = Ref;

	CHECK(IsDestroyed == false);
	CHECK(Ref->RefCount() == 2);

	RefPtr<TestRefClass> Ref3;
	Ref2 = Ref3;

	CHECK(IsDestroyed == false);
	CHECK(Ref->RefCount() == 1);
	Ref = Ref3;

	CHECK(IsDestroyed == true);
}

#endif

}  // namespace zen
