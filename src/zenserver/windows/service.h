// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

class WindowsService
{
public:
	WindowsService();
	~WindowsService();

	virtual int Run() = 0;

	int ServiceMain();

	static void Install();
	static void Delete();

	int SvcMain();
	static void __stdcall SvcCtrlHandler(unsigned long);
};
