# API status

Zen exposes a REST API which is currently intended to be used from Unreal Engine 
only. It is an internal implementation detail and is subject to change especially 
during the beta phase. Thus it is not recommended that you interface with this API 
directly.
