// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "zencore.h"

namespace zen {

struct Base64
{
	template<typename CharType>
	static uint32_t Encode(const uint8_t* Source, uint32_t Length, CharType* Dest);

	static inline constexpr int32_t GetEncodedDataSize(uint32_t Size) { return ((Size + 2) / 3) * 4; }
};

}  // namespace zen
