// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include <zencore/zencore.h>

#if ZEN_PLATFORM_WINDOWS
#	include <zencore/windows.h>

#	include <system_error>

namespace zen {

class WinIoThreadPool
{
public:
	WinIoThreadPool(int InThreadCount, int InMaxThreadCount);
	~WinIoThreadPool();

	void		  CreateIocp(HANDLE IoHandle, PTP_WIN32_IO_CALLBACK Callback, void* Context, std::error_code& ErrorCode);
	inline PTP_IO Iocp() const { return m_ThreadPoolIo; }

private:
	PTP_POOL			m_ThreadPool   = nullptr;
	PTP_CLEANUP_GROUP	m_CleanupGroup = nullptr;
	PTP_IO				m_ThreadPoolIo = nullptr;
	TP_CALLBACK_ENVIRON m_CallbackEnvironment;
};

}  // namespace zen
#endif
