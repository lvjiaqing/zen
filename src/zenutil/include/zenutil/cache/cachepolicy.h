// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include <zenbase/refcount.h>
#include <zencore/compactbinary.h>
#include <zencore/enumflags.h>
#include <zencore/string.h>
#include <zencore/uid.h>

#include <gsl/gsl-lite.hpp>
#include <span>
namespace zen::Private {
class ICacheRecordPolicyShared;
}
namespace zen {

class CbObjectView;
class CbWriter;

class OptionalCacheRecordPolicy;

enum class CachePolicy : uint32_t
{
	/** A value with no flags. Disables access to the cache unless combined with other flags. */
	None = 0,

	/** Allow a cache request to query local caches. */
	QueryLocal = 1 << 0,
	/** Allow a cache request to query remote caches. */
	QueryRemote = 1 << 1,
	/** Allow a cache request to query any caches. */
	Query = QueryLocal | QueryRemote,

	/** Allow cache requests to query and store records and values in local caches. */
	StoreLocal = 1 << 2,
	/** Allow cache records and values to be stored in remote caches. */
	StoreRemote = 1 << 3,
	/** Allow cache records and values to be stored in any caches. */
	Store = StoreLocal | StoreRemote,

	/** Allow cache requests to query and store records and values in local caches. */
	Local = QueryLocal | StoreLocal,
	/** Allow cache requests to query and store records and values in remote caches. */
	Remote = QueryRemote | StoreRemote,

	/** Allow cache requests to query and store records and values in any caches. */
	Default = Query | Store,

	/** Skip fetching the data for values. */
	SkipData = 1 << 4,

	/** Skip fetching the metadata for record requests. */
	SkipMeta = 1 << 5,

	/**
	 * Partial output will be provided with the error status when a required value is missing.
	 *
	 * This is meant for cases when the missing values can be individually recovered, or rebuilt,
	 * without rebuilding the whole record. The cache automatically adds this flag when there are
	 * other cache stores that it may be able to recover missing values from.
	 *
	 * Missing values will be returned in the records, but with only the hash and size.
	 *
	 * Applying this flag for a put of a record allows a partial record to be stored.
	 */
	PartialRecord = 1 << 6,

	/**
	 * Keep records in the cache for at least the duration of the session.
	 *
	 * This is a hint that the record may be accessed again in this session. This is mainly meant
	 * to be used when subsequent accesses will not tolerate a cache miss.
	 */
	KeepAlive = 1 << 7,
};

gsl_DEFINE_ENUM_BITMASK_OPERATORS(CachePolicy);
/** Append a non-empty text version of the policy to the builder. */
StringBuilderBase& operator<<(StringBuilderBase& Builder, CachePolicy Policy);
/** Parse non-empty text written by operator<< into a policy. */
CachePolicy ParseCachePolicy(std::string_view Text);
/** Return input converted into the equivalent policy that the upstream should use when forwarding a put or get to an upstream server. */
CachePolicy ConvertToUpstream(CachePolicy Policy);

inline CachePolicy
Union(CachePolicy A, CachePolicy B)
{
	constexpr CachePolicy InvertedFlags = CachePolicy::SkipData | CachePolicy::SkipMeta;
	return (A & ~(InvertedFlags)) | (B & ~(InvertedFlags)) | (A & B & InvertedFlags);
}

/** A value ID and the cache policy to use for that value. */
struct CacheValuePolicy
{
	Oid			Id;
	CachePolicy Policy = CachePolicy::Default;

	/** Flags that are valid on a value policy. */
	static constexpr CachePolicy PolicyMask = CachePolicy::Default | CachePolicy::SkipData;
};

/** Interface for the private implementation of the cache record policy. */
class Private::ICacheRecordPolicyShared : public RefCounted
{
public:
	virtual ~ICacheRecordPolicyShared()														 = default;
	virtual void							  AddValuePolicy(const CacheValuePolicy& Policy) = 0;
	virtual std::span<const CacheValuePolicy> GetValuePolicies() const						 = 0;
};

/**
 * Flags to control the behavior of cache record requests, with optional overrides by value.
 *
 * Examples:
 * - A base policy of None with value policy overrides of Default will fetch those values if they
 *   exist in the record, and skip data for any other values.
 * - A base policy of Default, with value policy overrides of (Query | SkipData), will skip those
 *   values, but still check if they exist, and will load any other values.
 */
class CacheRecordPolicy
{
public:
	/** Construct a cache record policy that uses the default policy. */
	CacheRecordPolicy() = default;

	/** Construct a cache record policy with a uniform policy for the record and every value. */
	inline CacheRecordPolicy(CachePolicy BasePolicy)
	: RecordPolicy(BasePolicy)
	, DefaultValuePolicy(BasePolicy & CacheValuePolicy::PolicyMask)
	{
	}

	/** Returns true if the record and every value use the same cache policy. */
	inline bool IsUniform() const { return !Shared; }

	/** Returns the cache policy to use for the record. */
	inline CachePolicy GetRecordPolicy() const { return RecordPolicy; }

	/** Returns the base cache policy that this was constructed from. */
	inline CachePolicy GetBasePolicy() const { return DefaultValuePolicy | (RecordPolicy & ~CacheValuePolicy::PolicyMask); }

	/** Returns the cache policy to use for the value. */
	CachePolicy GetValuePolicy(const Oid& Id) const;

	/** Returns the array of cache policy overrides for values, sorted by ID. */
	inline std::span<const CacheValuePolicy> GetValuePolicies() const
	{
		return Shared ? Shared->GetValuePolicies() : std::span<const CacheValuePolicy>();
	}

	/** Saves the cache record policy to a compact binary object. */
	void Save(CbWriter& Writer) const;

	/** Loads a cache record policy from an object. */
	static OptionalCacheRecordPolicy Load(CbObjectView Object);

	/** Return *this converted into the equivalent policy that the upstream should use when forwarding a put or get to an upstream server.
	 */
	CacheRecordPolicy ConvertToUpstream() const;

private:
	friend class CacheRecordPolicyBuilder;
	friend class OptionalCacheRecordPolicy;

	CachePolicy										RecordPolicy	   = CachePolicy::Default;
	CachePolicy										DefaultValuePolicy = CachePolicy::Default;
	RefPtr<const Private::ICacheRecordPolicyShared> Shared;
};

/** A cache record policy builder is used to construct a cache record policy. */
class CacheRecordPolicyBuilder
{
public:
	/** Construct a policy builder that uses the default policy as its base policy. */
	CacheRecordPolicyBuilder() = default;

	/** Construct a policy builder that uses the provided policy for the record and values with no override. */
	inline explicit CacheRecordPolicyBuilder(CachePolicy Policy) : BasePolicy(Policy) {}

	/** Adds a cache policy override for a value. */
	void		AddValuePolicy(const CacheValuePolicy& Value);
	inline void AddValuePolicy(const Oid& Id, CachePolicy Policy) { AddValuePolicy({Id, Policy}); }

	/** Build a cache record policy, which makes this builder subsequently unusable. */
	CacheRecordPolicy Build();

private:
	CachePolicy								  BasePolicy = CachePolicy::Default;
	RefPtr<Private::ICacheRecordPolicyShared> Shared;
};

/**
 * A cache record policy that can be null.
 *
 * @see CacheRecordPolicy
 */
class OptionalCacheRecordPolicy : private CacheRecordPolicy
{
public:
	inline OptionalCacheRecordPolicy() : CacheRecordPolicy(~CachePolicy::None) {}

	inline OptionalCacheRecordPolicy(CacheRecordPolicy&& InOutput) : CacheRecordPolicy(std::move(InOutput)) {}
	inline OptionalCacheRecordPolicy(const CacheRecordPolicy& InOutput) : CacheRecordPolicy(InOutput) {}
	inline OptionalCacheRecordPolicy& operator=(CacheRecordPolicy&& InOutput)
	{
		CacheRecordPolicy::operator=(std::move(InOutput));
		return *this;
	}
	inline OptionalCacheRecordPolicy& operator=(const CacheRecordPolicy& InOutput)
	{
		CacheRecordPolicy::operator=(InOutput);
		return *this;
	}

	/** Returns the cache record policy. The caller must check for null before using this accessor. */
	inline const CacheRecordPolicy& Get() const& { return *this; }
	inline CacheRecordPolicy		Get() && { return std::move(*this); }

	inline bool		IsNull() const { return RecordPolicy == ~CachePolicy::None; }
	inline bool		IsValid() const { return !IsNull(); }
	inline explicit operator bool() const { return !IsNull(); }

	inline void Reset() { *this = OptionalCacheRecordPolicy(); }
};

void cachepolicy_forcelink();

}  // namespace zen
