// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "../zen.h"

namespace zen {

class InfoCommand : public ZenCmdBase
{
public:
	InfoCommand();
	~InfoCommand();

	virtual int				  Run(const ZenCliOptions& GlobalOptions, int argc, char** argv) override;
	virtual cxxopts::Options& Options() override { return m_Options; }
	// virtual ZenCmdCategory&	  CommandCategory() const override { return g_UtilitiesCategory; }

private:
	cxxopts::Options m_Options{"info", "Show high level zen store information"};
	std::string		 m_HostName;
};

}  // namespace zen
