# Naming Conventions

The naming conventions for Zen are intended to remain close to the Unreal Engine coding style, with some minor exceptions wrt prefixes.

* Classes/Structs - `PascalCase`
* Functions - `PascalCase()`
* Class member variables - `m_PascalCase`
* Global variables - `g_PascalCase`
* Static variables - `s_PascalCase`
* Thread local variables - `t_PascalCase`

Those who are familiar with the UE coding standards will note that we do not require or encourage `F` prefixes on struct or classes, and we expect class members to have a `m_` member prefix.

# Code formatting

To ensure consistent formatting we rely on `clang-format` to automatically format source code. This leads to consistent formatting which should lead to fewer surprises and more straightforward merging.

Formatting is ensured by using [pre-commit](https://pre-commit.com/)

- [Ensure you have a Python install](https://www.python.org/downloads/) and that Python has been added to the PATH environment variable
- [Install pre-commit](https://pre-commit.com/#installation) so it is available in PATH
- Run pre-commit manually on staged files `pre-commit run`
- Run pre-commit manually on all files `pre-commit run --all-files`. There is also a `xmake precommit` shortcut which may be easier to remember
- Install git commit hooks `pre-commit install`, which will automatically run before every commit.
