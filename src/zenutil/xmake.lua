-- Copyright Epic Games, Inc. All Rights Reserved.

target('zenutil')
    set_kind("static")
    set_group("libs")
    add_headerfiles("**.h")
    add_files("**.cpp")
    add_includedirs("include", {public=true})
    add_deps("zencore")
    add_packages("vcpkg::robin-map", "vcpkg::spdlog")
