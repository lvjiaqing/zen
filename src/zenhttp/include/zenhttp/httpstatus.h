// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include <zencore/logging.h>
#include <zenhttp/httpserver.h>

#include <map>

namespace zen {

struct IHttpStatusProvider
{
	virtual void HandleStatusRequest(HttpServerRequest& Request) = 0;
};

class HttpStatusService : public HttpService
{
public:
	HttpStatusService();
	~HttpStatusService();

	virtual const char* BaseUri() const override;
	virtual void		HandleRequest(HttpServerRequest& Request) override;
	void				RegisterHandler(std::string_view Id, IHttpStatusProvider& Provider);
	void				UnregisterHandler(std::string_view Id, IHttpStatusProvider& Provider);

private:
	LoggerRef		  m_Log;
	HttpRequestRouter m_Router;

	RwLock										m_Lock;
	std::map<std::string, IHttpStatusProvider*> m_Providers;

	inline LoggerRef Log() { return m_Log; }
};

}  // namespace zen
