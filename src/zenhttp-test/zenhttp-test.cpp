// Copyright Epic Games, Inc. All Rights Reserved.

#include <zencore/filesystem.h>
#include <zencore/logging.h>
#include <zenhttp/zenhttp.h>

#if ZEN_USE_MIMALLOC
ZEN_THIRD_PARTY_INCLUDES_START
#	include <mimalloc-new-delete.h>
ZEN_THIRD_PARTY_INCLUDES_END
#endif

#if ZEN_WITH_TESTS
#	define ZEN_TEST_WITH_RUNNER 1
#	include <zencore/testing.h>
#endif

int
main([[maybe_unused]] int argc, [[maybe_unused]] char* argv[])
{
#if ZEN_USE_MIMALLOC
	mi_version();
#endif
#if ZEN_WITH_TESTS
	zen::zenhttp_forcelinktests();

	zen::logging::InitializeLogging();
	zen::MaximizeOpenFileCount();

	return ZEN_RUN_TESTS(argc, argv);
#else
	return 0;
#endif
}
