// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

/* clang-format off */

#include <zencore/zencore.h>

#if ZEN_WITH_TRACE

ZEN_THIRD_PARTY_INCLUDES_START
#if !defined(TRACE_IMPLEMENT)
#	define TRACE_IMPLEMENT 0
#endif
#include <trace.h>
#undef TRACE_IMPLEMENT
ZEN_THIRD_PARTY_INCLUDES_END

#define ZEN_TRACE_CPU(x) TRACE_CPU_SCOPE(x)
#define ZEN_TRACE_CPU_FLUSH(x) TRACE_CPU_SCOPE(x, trace::CpuScopeFlags::CpuFlush)

enum class TraceType
{
	File,
	Network,
	None
};

void TraceInit(std::string_view ProgramName);
void TraceShutdown();
bool IsTracing();
void TraceStart(std::string_view ProgramName, const char* HostOrPath, TraceType Type);
bool TraceStop();

#else

#define ZEN_TRACE_CPU(x)
#define ZEN_TRACE_CPU_FLUSH(x)

#endif	// ZEN_WITH_TRACE

/* clang-format on */
