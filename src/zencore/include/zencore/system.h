// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include <zencore/zencore.h>

#include <string>

namespace zen {

class CbWriter;

std::string		 GetMachineName();
std::string_view GetOperatingSystemName();
std::string_view GetCpuName();

struct SystemMetrics
{
	int		 CpuCount			   = 1;
	int		 CoreCount			   = 1;
	int		 LogicalProcessorCount = 1;
	uint64_t SystemMemoryMiB	   = 0;
	uint64_t AvailSystemMemoryMiB  = 0;
	uint64_t VirtualMemoryMiB	   = 0;
	uint64_t AvailVirtualMemoryMiB = 0;
	uint64_t PageFileMiB		   = 0;
	uint64_t AvailPageFileMiB	   = 0;
};

SystemMetrics GetSystemMetrics();

void		  SetCpuCountForReporting(int FakeCpuCount);
SystemMetrics GetSystemMetricsForReporting();

void Describe(const SystemMetrics& Metrics, CbWriter& Writer);

}  // namespace zen
