// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include <zencore/logging.h>

namespace zen {

struct ZenServerOptions;

void InitializeServerLogging(const ZenServerOptions& LogOptions);
void ShutdownServerLogging();

}  // namespace zen
