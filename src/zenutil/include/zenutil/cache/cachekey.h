// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include <zencore/iohash.h>
#include <zencore/string.h>
#include <zencore/uid.h>

#include <zenutil/cache/cachepolicy.h>

namespace zen {

struct CacheKey
{
	std::string Bucket;
	IoHash		Hash;

	static CacheKey Create(std::string_view Bucket, const IoHash& Hash) { return {.Bucket = ToLower(Bucket), .Hash = Hash}; }

	auto operator<=>(const CacheKey& that) const
	{
		if (auto b = caseSensitiveCompareStrings(Bucket, that.Bucket); b != std::strong_ordering::equal)
		{
			return b;
		}
		return Hash <=> that.Hash;
	}

	auto operator==(const CacheKey& that) const { return (*this <=> that) == std::strong_ordering::equal; }

	static const CacheKey Empty;
};

struct CacheChunkRequest
{
	CacheKey	Key;
	IoHash		ChunkId;
	Oid			ValueId;
	uint64_t	RawOffset = 0ull;
	uint64_t	RawSize	  = ~uint64_t(0);
	CachePolicy Policy	  = CachePolicy::Default;
};

struct CacheKeyRequest
{
	CacheKey		  Key;
	CacheRecordPolicy Policy;
};

struct CacheValueRequest
{
	CacheKey	Key;
	CachePolicy Policy = CachePolicy::Default;
};

inline bool
operator<(const CacheChunkRequest& A, const CacheChunkRequest& B)
{
	if (A.Key < B.Key)
	{
		return true;
	}
	if (B.Key < A.Key)
	{
		return false;
	}
	if (A.ChunkId < B.ChunkId)
	{
		return true;
	}
	if (B.ChunkId < A.ChunkId)
	{
		return false;
	}
	if (A.ValueId < B.ValueId)
	{
		return true;
	}
	if (B.ValueId < A.ValueId)
	{
		return false;
	}
	return A.RawOffset < B.RawOffset;
}

}  // namespace zen
