// Copyright Epic Games, Inc. All Rights Reserved.

#include "zenutil/zenutil.h"

#if ZEN_WITH_TESTS

#	include <zenutil/basicfile.h>
#	include <zenutil/cache/cacherequests.h>
#	include <zenutil/cache/rpcrecording.h>
#	include <zenutil/packageformat.h>

namespace zen {

void
zenutil_forcelinktests()
{
	basicfile_forcelink();
	cachepolicy_forcelink();
	cache::rpcrecord_forcelink();
	forcelink_packageformat();
	cacherequests_forcelink();
}

}  // namespace zen

#endif
