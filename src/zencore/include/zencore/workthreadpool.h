// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include <zencore/zencore.h>

#include <zenbase/refcount.h>

#include <exception>
#include <functional>
#include <future>

namespace zen {

//////////////////////////////////////////////////////////////////////////

struct IWork : public RefCounted
{
	virtual void Execute() = 0;

	inline std::exception_ptr GetException() { return m_Exception; }

private:
	std::exception_ptr m_Exception;

	friend class WorkerThreadPool;
};

//////////////////////////////////////////////////////////////////////////

class WorkerThreadPool
{
public:
	explicit WorkerThreadPool(int InThreadCount);
	WorkerThreadPool(int InThreadCount, std::string_view WorkerThreadBaseName);
	~WorkerThreadPool();

	void ScheduleWork(Ref<IWork> Work);
	void ScheduleWork(std::function<void()>&& Work);

	template<typename Func>
	auto EnqueueTask(std::packaged_task<Func> Task);

	[[nodiscard]] size_t PendingWorkItemCount() const;

private:
	struct Impl;
	struct ThreadStartInfo;

	std::unique_ptr<Impl> m_Impl;
};

//////////////////////////////////////////////////////////////////////////

template<typename Func>
auto
WorkerThreadPool::EnqueueTask(std::packaged_task<Func> Task)
{
	struct FutureWork : IWork
	{
		FutureWork(std::packaged_task<Func> Task) : m_Task{std::move(Task)} {}
		virtual void Execute() override { m_Task(); }

		std::packaged_task<Func> m_Task;
	};

	Ref<FutureWork> Work{new FutureWork(std::move(Task))};

	auto Future = Work->m_Task.get_future();
	ScheduleWork(std::move(Work));
	return Future;
}

void workthreadpool_forcelink();  // internal

}  // namespace zen
