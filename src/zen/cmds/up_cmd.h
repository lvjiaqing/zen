// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "../zen.h"

#include <filesystem>

namespace zen {

class UpCommand : public ZenCmdBase
{
public:
	UpCommand();
	~UpCommand();

	virtual int				  Run(const ZenCliOptions& GlobalOptions, int argc, char** argv) override;
	virtual cxxopts::Options& Options() override { return m_Options; }

private:
	cxxopts::Options	  m_Options{"up", "Bring up zen service"};
	uint16_t			  m_Port	 = 0;
	int					  m_OwnerPid = 0;
	std::filesystem::path m_ProgramBaseDir;
};

class AttachCommand : public ZenCmdBase
{
public:
	AttachCommand();
	~AttachCommand();

	virtual int				  Run(const ZenCliOptions& GlobalOptions, int argc, char** argv) override;
	virtual cxxopts::Options& Options() override { return m_Options; }

private:
	cxxopts::Options	  m_Options{"attach", "Add a sponsor process to a running zen service"};
	uint16_t			  m_Port;
	int					  m_OwnerPid;
	std::filesystem::path m_DataDir;
};

class DownCommand : public ZenCmdBase
{
public:
	DownCommand();
	~DownCommand();

	virtual int				  Run(const ZenCliOptions& GlobalOptions, int argc, char** argv) override;
	virtual cxxopts::Options& Options() override { return m_Options; }

private:
	cxxopts::Options	  m_Options{"down", "Bring down zen service"};
	uint16_t			  m_Port;
	bool				  m_ForceTerminate = false;
	std::filesystem::path m_ProgramBaseDir;
	std::filesystem::path m_DataDir;
};

}  // namespace zen
