#!/bin/bash

die() { echo ERROR: $1; exit 1; }

# Validate input
if [ -z $1 ]; then
	echo "usage: $0 <ue_toolchain_dir>"
	exit 1
fi

if ! [ -d $1 ]; then
	die "$1 is not a directory"
fi

if ! [ -e $1/bin/clang++ ]; then
	die "$1/bin/clang++ does not exist"
fi

export UE_TOOLCHAIN_DIR=$(realpath $1)
export CC="clang"
export CXX="clang++"
export LD="clang++"

export PATH="$(realpath $(dirname ${BASH_SOURCE[0]})):$PATH"

shift
exec $*
