// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include <zenhttp/httpserver.h>

namespace zen {

class AuthMgr;
class UpstreamCache;

class HttpUpstreamService final : public zen::HttpService
{
public:
	HttpUpstreamService(UpstreamCache& Upstream, AuthMgr& Mgr);
	virtual ~HttpUpstreamService();

	virtual const char* BaseUri() const override;
	virtual void		HandleRequest(zen::HttpServerRequest& Request) override;

private:
	UpstreamCache&	  m_Upstream;
	AuthMgr&		  m_AuthMgr;
	HttpRequestRouter m_Router;
};

}  // namespace zen
