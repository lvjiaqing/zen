// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "zenvfs.h"

#if ZEN_WITH_VFS

namespace zen {

bool IsProjFsAvailable();

}  // namespace zen
#endif
