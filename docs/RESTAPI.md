# REST API

## Test Service

Intended to be used for basic connectivity testing. Allows the client to fetch 
various kinds of payloads via well-known URIs

HTTP endpoint: `/test`

`/test/size/{size}` - verbs: (`GET`)

## Cache Service

HTTP endpoint: `/cache`

`/cache/`
