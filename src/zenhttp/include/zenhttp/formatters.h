// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include <zencore/compactbinary.h>
#include <zencore/compactbinaryvalidation.h>
#include <zencore/iobuffer.h>
#include <zencore/string.h>
#include <zenhttp/httpclient.h>

ZEN_THIRD_PARTY_INCLUDES_START
#include <cpr/cpr.h>
#include <fmt/format.h>
ZEN_THIRD_PARTY_INCLUDES_END

template<>
struct fmt::formatter<cpr::Response>
{
	constexpr auto parse(format_parse_context& Ctx) -> decltype(Ctx.begin()) { return Ctx.end(); }

	template<typename FormatContext>
	auto format(const cpr::Response& Response, FormatContext& Ctx) -> decltype(Ctx.out())
	{
		using namespace std::literals;

		if (Response.status_code == 200 || Response.status_code == 201)
		{
			return fmt::format_to(Ctx.out(),
								  "Url: {}, Status: {}, Bytes: {}/{} (Up/Down), Elapsed: {}s",
								  Response.url.str(),
								  Response.status_code,
								  Response.uploaded_bytes,
								  Response.downloaded_bytes,
								  Response.elapsed);
		}
		else
		{
			const auto			   It		   = Response.header.find("Content-Type");
			const std::string_view ContentType = It != Response.header.end() ? It->second : "<None>"sv;

			if (ContentType == "application/x-ue-cb"sv)
			{
				zen::IoBuffer					  Body(zen::IoBuffer::Wrap, Response.text.data(), Response.text.size());
				zen::CbObjectView				  Obj(Body.Data());
				zen::ExtendableStringBuilder<256> Sb;
				std::string_view				  Json = Obj.ToJson(Sb).ToView();

				return fmt::format_to(Ctx.out(),
									  "Url: {}, Status: {}, Bytes: {}/{} (Up/Down), Elapsed: {}s, Response: '{}', Reason: '{}'",
									  Response.url.str(),
									  Response.status_code,
									  Response.uploaded_bytes,
									  Response.downloaded_bytes,
									  Response.elapsed,
									  Json,
									  Response.reason);
			}
			else
			{
				return fmt::format_to(Ctx.out(),
									  "Url: {}, Status: {}, Bytes: {}/{} (Up/Down), Elapsed: {}s, Reponse: '{}', Reason: '{}'",
									  Response.url.str(),
									  Response.status_code,
									  Response.uploaded_bytes,
									  Response.downloaded_bytes,
									  Response.elapsed,
									  Response.text,
									  Response.reason);
			}
		}
	}
};

template<>
struct fmt::formatter<zen::HttpClient::Response>
{
	constexpr auto parse(format_parse_context& Ctx) -> decltype(Ctx.begin()) { return Ctx.end(); }

	template<typename FormatContext>
	auto format(const zen::HttpClient::Response& Response, FormatContext& Ctx) -> decltype(Ctx.out())
	{
		using namespace std::literals;

		if (Response.IsSuccess())
		{
			return fmt::format_to(Ctx.out(),
								  "OK: Status: {}, Bytes: {}/{} (Up/Down), Elapsed: {}s",
								  ToString(Response.StatusCode),
								  Response.UploadedBytes,
								  Response.DownloadedBytes,
								  Response.ElapsedSeconds);
		}
		else if (Response.Error)
		{
			return fmt::format_to(Ctx.out(),
								  "Failed: Elapsed: {}s, Error: ({}) '{}",
								  Response.ElapsedSeconds,
								  Response.Error.value().ErrorCode,
								  Response.Error.value().ErrorMessage);
		}
		else
		{
			return fmt::format_to(Ctx.out(),
								  "Failed: Bytes: {}/{} (Up/Down), Elapsed: {}s, Reason: '{}",
								  Response.UploadedBytes,
								  Response.DownloadedBytes,
								  Response.ElapsedSeconds,
								  Response.ErrorMessage(""sv));
		}
	}
};
