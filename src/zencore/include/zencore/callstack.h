// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include <zencore/zencore.h>

#include <zencore/string.h>

#include <string>
#include <vector>

namespace zen {

struct CallstackFrames
{
	uint32_t FrameCount;
	void**	 Frames;
};

CallstackFrames* CreateCallstack(uint32_t FrameCount, void** Frames) noexcept;
CallstackFrames* CloneCallstack(const CallstackFrames* Callstack) noexcept;
void			 FreeCallstack(CallstackFrames* Callstack) noexcept;

uint32_t				 GetCallstack(int FramesToSkip, int FramesToCapture, void* OutAddresses[]);
std::vector<std::string> GetFrameSymbols(uint32_t FrameCount, void** Frames);
inline std::vector<std::string>
GetFrameSymbols(const CallstackFrames* Callstack)
{
	return GetFrameSymbols(Callstack ? Callstack->FrameCount : 0, Callstack ? Callstack->Frames : nullptr);
}

void		FormatCallstack(const CallstackFrames* Callstack, StringBuilderBase& SB);
std::string CallstackToString(const CallstackFrames* Callstack);

void callstack_forcelink();	 // internal

}  // namespace zen
