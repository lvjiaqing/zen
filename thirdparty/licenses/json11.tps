<?xml version="1.0" encoding="utf-8"?>
<TpsData xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <Name>json11</Name>
  <Location>https://github.com/EpicGames/zen/tree/main/thirdparty/</Location>
  <Function>json11 is a tiny JSON library for C++11, providing JSON parsing and serialization.</Function>
  <Eula>https://github.com/dropbox/json11/blob/master/LICENSE.txt</Eula>
  <RedistributeTo>
    <EndUserGroup>Licensees</EndUserGroup>
    <EndUserGroup>Git</EndUserGroup>
    <EndUserGroup>P4</EndUserGroup>
  </RedistributeTo>
  <LicenseFolder>https://github.com/EpicGames/zen/tree/main/thirdparty/licenses</LicenseFolder>
</TpsData>