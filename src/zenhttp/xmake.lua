-- Copyright Epic Games, Inc. All Rights Reserved.

target('zenhttp')
    set_kind("static")
    set_group("libs")
    add_headerfiles("**.h")
    add_files("**.cpp")
    add_files("servers/httpsys.cpp", {unity_ignored=true})
    add_includedirs("include", {public=true})
    add_deps("zencore", "zenutil", "transport-sdk")
    add_packages(
        "vcpkg::asio",
        "vcpkg::cpr",
        "vcpkg::curl",  -- required by cpr
        "vcpkg::openssl", -- required by curl
        "vcpkg::zlib",  -- required by curl
        "vcpkg::gsl-lite",
        "vcpkg::http-parser"
    )
    add_options("httpsys")

    if is_plat("linux") then
        -- The 'vcpkg::openssl' package is two libraries; ssl and crypto, with
        -- ssl being dependent on symbols in crypto. When GCC-like linkers read
        -- object files from their command line, those object files only resolve
        -- symbols of objects previously encountered. Thus crypto must appear
        -- after ssl so it can fill out ssl's unresolved symbol table. Xmake's
        -- vcpkg support is basic and works by parsing .list files. Openssl's
        -- archives are listed alphabetically causing crypto to be _before_ ssl
        -- and resulting in link errors. The links are restated here to force
        -- xmake to use the correct order, and "syslinks" is used to force the
        -- arguments to the end of the line (otherwise they can appear before
        -- curl and cause more errors).
        add_syslinks("crypto")
        add_syslinks("dl")
    end
