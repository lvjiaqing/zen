// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "../zen.h"

namespace zen {

class WorkspaceCommand : public CacheStoreCommand
{
public:
	static constexpr char Name[]		= "workspace";
	static constexpr char Description[] = "Manage workspaces - create, remove, info";

	WorkspaceCommand();
	~WorkspaceCommand();

	virtual int				  Run(const ZenCliOptions& GlobalOptions, int argc, char** argv) override;
	virtual cxxopts::Options& Options() override { return m_Options; }

private:
	cxxopts::Options m_Options{Name, Description};
	std::string		 m_HostName;

	std::string m_Verb;	 // create, info, remove

	std::string m_Id;

	cxxopts::Options m_CreateOptions{"create", "Create a workspace"};
	std::string		 m_Path;

	cxxopts::Options m_InfoOptions{"info", "Info about a workspace"};

	cxxopts::Options m_RemoveOptions{"remove", "Remove a workspace"};

	cxxopts::Options* m_SubCommands[3] = {&m_CreateOptions, &m_InfoOptions, &m_RemoveOptions};
};

class WorkspaceShareCommand : public CacheStoreCommand
{
public:
	static constexpr char Name[] = "workspace-share";
	static constexpr char Description[] =
		"Manage workspace shared folders in a workspace - create, remove, info, files, entries, get, batch";

	WorkspaceShareCommand();
	~WorkspaceShareCommand();
	virtual int				  Run(const ZenCliOptions& GlobalOptions, int argc, char** argv) override;
	virtual cxxopts::Options& Options() override { return m_Options; }

private:
	cxxopts::Options m_Options{Name, Description};
	std::string		 m_HostName;
	std::string		 m_WorkspaceId;
	std::string		 m_WorkspaceRoot;
	std::string		 m_Verb;  // create, info, remove
	std::string		 m_ShareId;
	std::string		 m_Alias;

	cxxopts::Options m_CreateOptions{"create", "Create a workspace share"};
	std::string		 m_SharePath;

	bool m_Refresh = false;

	cxxopts::Options m_InfoOptions{"info", "Info about a workspace share"};

	cxxopts::Options m_RemoveOptions{"remove", "Remove a workspace share"};

	std::string m_FieldFilter;

	cxxopts::Options m_FilesOptions{"files", "List files in a workspace share"};

	std::string m_ChunkId;

	cxxopts::Options m_EntriesOptions{"entries", "List entries in a workspace shared folder"};

	cxxopts::Options m_GetChunkOptions{"get", "List entries in a workspace shared folder"};
	uint64_t		 m_Offset = 0;
	uint64_t		 m_Size	  = ~uint64_t(0);

	cxxopts::Options		 m_GetChunkBatchOptions{"batch", "Get a batch of chunks from a workspace shared folder"};
	std::vector<std::string> m_ChunkIds;

	cxxopts::Options* m_SubCommands[7] = {&m_CreateOptions,
										  &m_InfoOptions,
										  &m_RemoveOptions,
										  &m_FilesOptions,
										  &m_EntriesOptions,
										  &m_GetChunkOptions,
										  &m_GetChunkBatchOptions};
};

}  // namespace zen
