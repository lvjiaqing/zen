// Copyright Epic Games, Inc. All Rights Reserved.

#include "zenvfs/vfs.h"
#include "vfsprovider.h"

#include <zencore/iohash.h>
#include <zencore/scopeguard.h>
#include <zencore/string.h>
#include <zencore/thread.h>

#if ZEN_WITH_VFS

namespace zen {

VfsHost::VfsHost(std::string_view VfsRootPath) : m_Provider(new VfsProvider(VfsRootPath))
{
}

VfsHost::~VfsHost()
{
	delete m_Provider;
}

void
VfsHost::Initialize()
{
	m_Provider->Initialize();
}

void
VfsHost::Run()
{
	m_Provider->Run();
}

void
VfsHost::RequestStop()
{
	m_Provider->RequestStop();
}

void
VfsHost::Cleanup()
{
	m_Provider->Cleanup();
}

void
VfsHost::AddMount(std::string_view Mountpoint, Ref<VfsTreeDataSource>&& DataSource)
{
	m_Provider->AddMount(Mountpoint, std::move(DataSource));
}

}  // namespace zen

#endif
