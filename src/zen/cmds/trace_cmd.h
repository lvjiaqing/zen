// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "../zen.h"

namespace zen {

/** Scrub storage
 */
class TraceCommand : public ZenCmdBase
{
public:
	TraceCommand();
	~TraceCommand();

	virtual int				  Run(const ZenCliOptions& GlobalOptions, int argc, char** argv) override;
	virtual cxxopts::Options& Options() override { return m_Options; }

private:
	cxxopts::Options m_Options{"trace", "Control zen realtime tracing"};
	std::string		 m_HostName;
	bool			 m_Stop;
	std::string		 m_TraceHost;
	std::string		 m_TraceFile;
};

}  // namespace zen
