-- Copyright Epic Games, Inc. All Rights Reserved.

target("zenserver-test")
    set_kind("binary")
    set_group("tests")
    add_headerfiles("**.h")
    add_files("*.cpp")
    add_files("zenserver-test.cpp", {unity_ignored  = true })
    add_deps("zencore", "zenutil", "zenhttp")
    add_deps("zenserver", {inherit=false})
    add_packages("vcpkg::cpr", "vcpkg::http-parser", "vcpkg::mimalloc")

    if is_plat("macosx") then
        add_ldflags("-framework CoreFoundation")
        add_ldflags("-framework Security")
        add_ldflags("-framework SystemConfiguration")
    end
