// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "zencore.h"

#include <cstdint>

namespace zen {

inline uint16_t
ByteSwap(uint16_t x)
{
#if ZEN_COMPILER_MSC
	return _byteswap_ushort(x);
#else
	return __builtin_bswap16(x);
#endif
}

inline uint32_t
ByteSwap(uint32_t x)
{
#if ZEN_COMPILER_MSC
	return _byteswap_ulong(x);
#else
	return __builtin_bswap32(x);
#endif
}

inline uint64_t
ByteSwap(uint64_t x)
{
#if ZEN_COMPILER_MSC
	return _byteswap_uint64(x);
#else
	return __builtin_bswap64(x);
#endif
}

inline uint16_t
FromNetworkOrder(uint16_t x)
{
	return ByteSwap(x);
}

inline uint32_t
FromNetworkOrder(uint32_t x)
{
	return ByteSwap(x);
}

inline uint64_t
FromNetworkOrder(uint64_t x)
{
	return ByteSwap(x);
}

inline uint16_t
FromNetworkOrder(int16_t x)
{
	return ByteSwap(uint16_t(x));
}

inline uint32_t
FromNetworkOrder(int32_t x)
{
	return ByteSwap(uint32_t(x));
}

inline uint64_t
FromNetworkOrder(int64_t x)
{
	return ByteSwap(uint64_t(x));
}

inline uint16_t
ToNetworkOrder(uint16_t x)
{
	return ByteSwap(x);
}

inline uint32_t
ToNetworkOrder(uint32_t x)
{
	return ByteSwap(x);
}

inline uint64_t
ToNetworkOrder(uint64_t x)
{
	return ByteSwap(x);
}

inline uint16_t
ToNetworkOrder(int16_t x)
{
	return ByteSwap(uint16_t(x));
}

inline uint32_t
ToNetworkOrder(int32_t x)
{
	return ByteSwap(uint32_t(x));
}

inline uint64_t
ToNetworkOrder(int64_t x)
{
	return ByteSwap(uint64_t(x));
}

}  // namespace zen
