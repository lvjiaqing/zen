// Copyright Epic Games, Inc. All Rights Reserved.

#include <zencore/mpscqueue.h>

#include <zencore/testing.h>
#include <string>

namespace zen {

#if ZEN_WITH_TESTS && 0
TEST_CASE("mpsc")
{
	MpscQueue<std::string> Queue;
	Queue.Enqueue("hello");
	std::optional<std::string> Value = Queue.Dequeue();
	CHECK_EQ(Value, "hello");
}
#endif

void
mpscqueue_forcelink()
{
}

}  // namespace zen