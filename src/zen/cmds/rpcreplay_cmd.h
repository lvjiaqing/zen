// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "../zen.h"

namespace zen {

class RpcStartRecordingCommand : public CacheStoreCommand
{
public:
	RpcStartRecordingCommand();
	~RpcStartRecordingCommand();

	virtual int				  Run(const ZenCliOptions& GlobalOptions, int argc, char** argv) override;
	virtual cxxopts::Options& Options() override { return m_Options; }

private:
	cxxopts::Options m_Options{"rpc-record-start", "Starts recording of cache rpc requests on a host"};
	std::string		 m_HostName;
	std::string		 m_RecordingPath;
};

class RpcStopRecordingCommand : public CacheStoreCommand
{
public:
	RpcStopRecordingCommand();
	~RpcStopRecordingCommand();

	virtual int				  Run(const ZenCliOptions& GlobalOptions, int argc, char** argv) override;
	virtual cxxopts::Options& Options() override { return m_Options; }

private:
	cxxopts::Options m_Options{"rpc-record-stop", "Stops recording of cache rpc requests on a host"};
	std::string		 m_HostName;
};

class RpcReplayCommand : public CacheStoreCommand
{
public:
	RpcReplayCommand();
	~RpcReplayCommand();

	virtual int				  Run(const ZenCliOptions& GlobalOptions, int argc, char** argv) override;
	virtual cxxopts::Options& Options() override { return m_Options; }

private:
	cxxopts::Options m_Options{"rpc-record-replay", "Replays a previously recorded session of cache rpc requests to a target host"};
	std::string		 m_HostName;
	std::string		 m_RecordingPath;
	bool			 m_OnHost					  = false;
	bool			 m_ShowMethodStats			  = false;
	int				 m_ProcessCount				  = 1;
	int				 m_ThreadCount				  = 0;
	uint64_t		 m_Offset					  = 0;
	uint64_t		 m_Stride					  = 1;
	bool			 m_ForceAllowLocalRefs		  = false;
	bool			 m_DisableLocalRefs			  = false;
	bool			 m_ForceAllowLocalHandleRef	  = false;
	bool			 m_DisableLocalHandleRefs	  = false;
	bool			 m_ForceAllowPartialLocalRefs = false;
	bool			 m_DisablePartialLocalRefs	  = false;
	bool			 m_DryRun					  = false;
};

}  // namespace zen
