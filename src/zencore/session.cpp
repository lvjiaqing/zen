// Copyright Epic Games, Inc. All Rights Reserved.

#include "zencore/session.h"

#include <zencore/uid.h>

#include <mutex>

namespace zen {

static Oid			  GlobalSessionId;
static Oid::String_t  GlobalSessionString;
static std::once_flag SessionInitFlag;

Oid
GetSessionId()
{
	std::call_once(SessionInitFlag, [&] {
		GlobalSessionId.Generate();
		GlobalSessionId.ToString(GlobalSessionString);
	});

	return GlobalSessionId;
}

std::string_view
GetSessionIdString()
{
	// Ensure we actually have a generated session identifier
	std::ignore = GetSessionId();

	return std::string_view(GlobalSessionString, Oid::StringLength);
}

}  // namespace zen
