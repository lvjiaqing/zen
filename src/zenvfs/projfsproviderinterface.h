// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include <zenvfs/vfs.h>
#include <zenvfs/zenvfs.h>

#if ZEN_WITH_VFS && ZEN_PLATFORM_WINDOWS

ZEN_THIRD_PARTY_INCLUDES_START
#	include <zencore/windows.h>
#	include <projectedfslib.h>
ZEN_THIRD_PARTY_INCLUDES_END

namespace zen {

//////////////////////////////////////////////////////////////////////////

struct ProjFsProviderInterface
{
	virtual ~ProjFsProviderInterface() = default;

	// [Mandatory] Inform the provider a directory enumeration is starting.
	// It corresponds to PRJ_START_DIRECTORY_ENUMERATION_CB in projectedfslib.h
	virtual HRESULT StartDirEnum(_In_ const PRJ_CALLBACK_DATA* CallbackData, _In_ const GUID* EnumerationId) = 0;

	// [Mandatory] Inform the provider a directory enumeration is over.
	// It corresponds to PRJ_END_DIRECTORY_ENUMERATION_CB in projectedfslib.h
	virtual HRESULT EndDirEnum(_In_ const PRJ_CALLBACK_DATA* CallbackData, _In_ const GUID* EnumerationId) = 0;

	// [Mandatory] Request directory enumeration information from the provider.
	// It corresponds to PRJ_GET_DIRECTORY_ENUMERATION_CB in projectedfslib.h
	virtual HRESULT GetDirEnum(_In_ const PRJ_CALLBACK_DATA*	CallbackData,
							   _In_ const GUID*					EnumerationId,
							   _In_opt_ PCWSTR					SearchExpression,
							   _In_ PRJ_DIR_ENTRY_BUFFER_HANDLE DirEntryBufferHandle) = 0;

	// [Mandatory] Request meta data information for a file or directory.
	// It corresponds to PRJ_GET_PLACEHOLDER_INFO_CB in projectedfslib.h
	virtual HRESULT GetPlaceholderInfo(_In_ const PRJ_CALLBACK_DATA* CallbackData) = 0;

	// [Mandatory] Request the contents of a file's primary data stream.
	// It corresponds to PRJ_GET_FILE_DATA_CB in projectedfslib.h
	virtual HRESULT GetFileData(_In_ const PRJ_CALLBACK_DATA* CallbackData, _In_ UINT64 ByteOffset, _In_ UINT32 Length) = 0;

	// [Optional] Deliver notifications to the provider that it has specified
	// it wishes to receive. It corresponds to PRJ_NOTIFICATION_CB in projectedfslib.h
	virtual HRESULT Notify(_In_ const PRJ_CALLBACK_DATA*		CallbackData,
						   _In_ BOOLEAN							IsDirectory,
						   _In_ PRJ_NOTIFICATION				NotificationType,
						   _In_opt_ PCWSTR						DestinationFileName,
						   _Inout_ PRJ_NOTIFICATION_PARAMETERS* NotificationParameters);

	/** This callback is optional. If the provider does not supply an implementation of this callback, ProjFS will invoke the provider's
		directory enumeration callbacks to determine the existence of a file path in the provider's store.

		The provider should use PrjFileNameCompare as the comparison routine when searching its backing store for the specified file.
	 */
	virtual HRESULT QueryFileName(_In_ const PRJ_CALLBACK_DATA* CallbackData);

	virtual void CancelCommand(_In_ const PRJ_CALLBACK_DATA* CallbackData);

	// Setup

	virtual void Run() = 0;

	//////////////////////////////////////////////////////////////////////////
	// Forwarding functions
	//

	static HRESULT StartDirEnumCallback_C(_In_ const PRJ_CALLBACK_DATA* CallbackData, _In_ const GUID* EnumerationId);
	static HRESULT EndDirEnumCallback_C(_In_ const PRJ_CALLBACK_DATA* CallbackData, _In_ const GUID* EnumerationId);
	static HRESULT GetDirEnumCallback_C(_In_ const PRJ_CALLBACK_DATA*	 CallbackData,
										_In_ const GUID*				 EnumerationId,
										_In_opt_ PCWSTR					 SearchExpression,
										_In_ PRJ_DIR_ENTRY_BUFFER_HANDLE DirEntryBufferHandle);
	static HRESULT GetPlaceholderInfoCallback_C(_In_ const PRJ_CALLBACK_DATA* CallbackData);
	static HRESULT GetFileDataCallback_C(_In_ const PRJ_CALLBACK_DATA* CallbackData, _In_ UINT64 ByteOffset, _In_ UINT32 Length);
	static HRESULT NotificationCallback_C(_In_ const PRJ_CALLBACK_DATA*		   CallbackData,
										  _In_ BOOLEAN						   IsDirectory,
										  _In_ PRJ_NOTIFICATION				   NotificationType,
										  _In_opt_ PCWSTR					   DestinationFileName,
										  _Inout_ PRJ_NOTIFICATION_PARAMETERS* NotificationParameters);
	static HRESULT QueryFileName_C(_In_ const PRJ_CALLBACK_DATA* CallbackData);
	static void	   CancelCommand_C(_In_ const PRJ_CALLBACK_DATA* CallbackData);
};

}  // namespace zen
#endif
