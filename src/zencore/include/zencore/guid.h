// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include <zencore/hashutils.h>
#include <zencore/zencore.h>

#include <compare>
#include <string_view>

namespace zen {

class StringBuilderBase;

struct Guid
{
	uint32_t A, B, C, D;

	static const int StringLength = 36;
	typedef char	 String_t[StringLength + 1];

	StringBuilderBase& ToString(StringBuilderBase& OutString) const;
	static Guid		   FromString(std::string_view InString);

	inline bool operator==(const Guid& Rhs) const  = default;
	inline auto operator<=>(const Guid& Rhs) const = default;
};

}  // namespace zen

template<>
struct std::hash<zen::Guid>
{
	std::size_t operator()(const zen::Guid& In) const noexcept { return zen::CombineHashes(In.A, In.B, In.C, In.D); }
};
