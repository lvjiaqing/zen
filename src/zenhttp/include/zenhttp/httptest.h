// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include <zencore/logging.h>
#include <zencore/stats.h>
#include <zenhttp/httpserver.h>

#include <atomic>
#include <unordered_map>

#if ZEN_WITH_TESTS

namespace zen {

/**
 * Test service to facilitate testing the HTTP framework and client interactions
 */
class HttpTestingService : public HttpService
{
public:
	HttpTestingService();
	~HttpTestingService();

	virtual const char*				 BaseUri() const override;
	virtual void					 HandleRequest(HttpServerRequest& Request) override;
	virtual Ref<IHttpPackageHandler> HandlePackageRequest(HttpServerRequest& HttpServiceRequest) override;

	class PackageHandler : public IHttpPackageHandler
	{
	public:
		PackageHandler(HttpTestingService& Svc, uint32_t RequestId);
		~PackageHandler();

		virtual void	 FilterOffer(std::vector<IoHash>& OfferCids) override;
		virtual void	 OnRequestBegin() override;
		virtual IoBuffer CreateTarget(const IoHash& Cid, uint64_t StorageSize) override;
		virtual void	 OnRequestComplete() override;

	private:
		HttpTestingService& m_Svc;
		uint32_t			m_RequestId;
	};

private:
	HttpRequestRouter		 m_Router;
	std::atomic<uint32_t>	 m_Counter{0};
	metrics::OperationTiming m_TimingStats;

	RwLock											  m_RwLock;
	std::unordered_map<uint32_t, Ref<PackageHandler>> m_HandlerMap;
};

}  // namespace zen

#endif
