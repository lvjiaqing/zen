-- Copyright Epic Games, Inc. All Rights Reserved.

target("zentest-appstub")
    set_kind("binary")
    set_group("tests")
    add_headerfiles("**.h")
    add_files("*.cpp")

    if is_os("linux") then
        add_syslinks("pthread")
    end

    if is_plat("macosx") then
        add_ldflags("-framework CoreFoundation")
        add_ldflags("-framework Security")
        add_ldflags("-framework SystemConfiguration")
    end
