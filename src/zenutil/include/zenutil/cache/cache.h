// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include <zenutil/cache/cachekey.h>
#include <zenutil/cache/cachepolicy.h>

ZEN_THIRD_PARTY_INCLUDES_START
#include <fmt/format.h>
ZEN_THIRD_PARTY_INCLUDES_END

namespace zen {

struct CacheRequestContext
{
	Oid		 SessionId{Oid::Zero};
	uint32_t RequestId{0};
};

}  // namespace zen

template<>
struct fmt::formatter<zen::CacheRequestContext> : formatter<string_view>
{
	template<typename FormatContext>
	auto format(const zen::CacheRequestContext& Context, FormatContext& ctx)
	{
		zen::ExtendableStringBuilder<64> String;
		Context.SessionId.ToString(String);
		String << ".";
		String << Context.RequestId;
		return formatter<string_view>::format(String.ToView(), ctx);
	}
};
