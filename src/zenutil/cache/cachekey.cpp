// Copyright Epic Games, Inc. All Rights Reserved.

#include <zenutil/cache/cachekey.h>

namespace zen {

const CacheKey CacheKey::Empty = CacheKey{.Bucket = std::string(), .Hash = IoHash()};

}  // namespace zen
