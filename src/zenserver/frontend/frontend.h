// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include <zenhttp/httpserver.h>
#include "zipfs.h"

#include <filesystem>

namespace zen {

class HttpFrontendService final : public zen::HttpService
{
public:
	HttpFrontendService(std::filesystem::path Directory);
	virtual ~HttpFrontendService();
	virtual const char* BaseUri() const override;
	virtual void		HandleRequest(zen::HttpServerRequest& Request) override;

private:
	ZipFs				  m_ZipFs;
	std::filesystem::path m_Directory;
};

}  // namespace zen
