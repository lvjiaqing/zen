// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include <zencore/crypto.h>
#include <zencore/iobuffer.h>
#include <zencore/string.h>

#include <chrono>
#include <filesystem>
#include <memory>

namespace zen {

struct AuthConfig
{
	std::filesystem::path RootDirectory;
	std::chrono::seconds  UpdateInterval{30};
	AesKey256Bit		  EncryptionKey;
	AesIV128Bit			  EncryptionIV;
};

class AuthMgr
{
public:
	virtual ~AuthMgr() = default;

	struct AddOpenIdProviderParams
	{
		std::string_view Name;
		std::string_view Url;
		std::string_view ClientId;
	};

	virtual void AddOpenIdProvider(const AddOpenIdProviderParams& Params) = 0;

	struct AddOpenIdTokenParams
	{
		std::string_view ProviderName;
		std::string_view RefreshToken;
	};

	virtual bool AddOpenIdToken(const AddOpenIdTokenParams& Params) = 0;

	struct OpenIdAccessToken
	{
		std::string							  AccessToken;
		std::chrono::system_clock::time_point ExpireTime{};
	};

	virtual OpenIdAccessToken GetOpenIdAccessToken(std::string_view ProviderName) = 0;

	static std::unique_ptr<AuthMgr> Create(const AuthConfig& Config);
};

}  // namespace zen
