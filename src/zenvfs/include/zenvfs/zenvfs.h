// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include <zencore/zencore.h>

#ifndef ZEN_WITH_VFS
#	define ZEN_WITH_VFS ZEN_PLATFORM_WINDOWS
#endif
