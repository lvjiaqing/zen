// Copyright Epic Games, Inc. All Rights Reserved.

#include "zennet/zennet.h"

#include <zencore/testing.h>

#include <zennet/statsdclient.h>

namespace zen {

void
zennet_forcelinktests()
{
#if ZEN_WITH_TESTS
	statsd_forcelink();
#endif
}

}  // namespace zen
